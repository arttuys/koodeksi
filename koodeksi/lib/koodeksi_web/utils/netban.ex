defmodule KoodeksiWeb.Utils.Netban do
  @moduledoc """
    This module takes care of so-called "network bans"; this is to enable elegant handling of total site bans at Phoenix level.

    What this effectively does, it grabs most UI and JSON requests, refusing to serve them and instead signaling that there is a ban in force.
  """

  def check_ip(ip) do
    Application.get_env(:koodeksi, :static_ip_func, fn _ -> true end).(ip) == true
  end

  defmodule UIBan do
    import Plug.Conn

    @moduledoc """
      UI level ban - redirect to an error page if you attempt to access anything else than it
    """
    @ban_page "/errors/network_ban"

    def init(default), do: default

    def call(conn, _) do
      remote_addr = conn.remote_ip # Retrieve the remote IP address
      if (!KoodeksiWeb.Utils.Netban.check_ip(remote_addr) && conn.request_path != @ban_page) do
        Phoenix.Controller.redirect(conn, to: @ban_page)
        |> halt
      else
        conn
      end
    end
  end

  defmodule JSONBan do
    import Plug.Conn

    @moduledoc """
      JSON level ban - respond with a plain "access denied"
    """
    def init(default), do: default

    def call(conn, _) do
      remote_addr = conn.remote_ip # Retrieve the remote IP address
      if (!KoodeksiWeb.Utils.Netban.check_ip(remote_addr)) do
        Phoenix.Controller.text(conn, "Access denied")
        |> halt
      else
        conn
      end
    end
  end
end