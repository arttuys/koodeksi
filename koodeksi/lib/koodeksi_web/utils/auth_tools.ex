defmodule KoodeksiWeb.Utils.AuthTools do
  import Plug.Conn

  @moduledoc """
    This module defines so-called "auth tools", or the shared interface on setting and verifying the authentication cookies as sent by the clients.

    This module does not concern itself with the actual checks; those are called directly by the controller function, and then, in response to the results of
    those calls, it may or may not call functions in this module to register the authorized status.
  """

  @doc "Logs out an user; this deletes the UID from both the assigns and the cookie"
  def log_out_user(conn) do
    conn
    |> assign(:uid, nil)
    |> assign(:security_key_invalid, nil)
    |> configure_session([:renew])
    |> put_session(:uid, nil)
    |> put_session(:security_key, nil)
  end

  @doc """
  Logs an user in, or if already logged in, switches apparent identity. Takes two parameters
    - UID, the user identifier
    - Security key - this will be stored along with the session. Its intention is to ensure that even when no server-side session storage is used, there is a way for the server to revoke a session
      The security key, therefore, must be something that does not depend on the client at all.

      If the security key is nil or not a valid string, an exception will be raised; it is assumed that the caller will see that the security key should exist in all normal situations
"""
  def log_in_user(conn, uid, security_key) do
    if (!is_integer(uid)), do: raise "UID must be an integer!"
    if (security_key == nil || (!String.valid?(security_key))), do: raise "Security key must exist, and it must be a valid string!"
    conn
    |> assign(:uid, uid)
    |> assign(:security_key_invalid, false)
    |> configure_session([:renew])
    |> put_session(:uid, uid)
    |> put_session(:security_key, security_key)
  end

  @doc """
    Checks the user session, and sets the assigns accordingly.

    If both the UID and security key exist in the session, the security key will be verified by an optionally provided function (by default, any security key will be accepted!)
    The function in question should accept 2 parameters; the first one being the UID, and the second one being the security key for given. The result should be a boolean, which signifies if the security key was valid or not.

    If the security key is invalid, the UID field will be nil. This is to reduce corner cases: if a non-nil UID field is found, one can assume that there is a logged in, factually existing user, that has been cleared and can be assumed to have a valid session.
    If the datatypes for the UID and the security key do not match, they will trigger an exception to alert of possible tampering or internal error
"""
  def user_auth_session_to_assigns(conn, security_key_checker_func \\ (fn (_,_) -> true end)) do
    uid = get_session(conn, :uid)
    key = get_session(conn, :security_key)
    # If both the UID and key are not nil, this could be a valid user. Also require that there is no network ban in force; if there is, treat the user as a nobody even if they had a valid cookie. They cannot execute actions anyway, so it is not useful to check for session validity in depth
    if (uid != nil && key != nil && KoodeksiWeb.Utils.Netban.check_ip(conn.remote_ip)) do
      if (!is_integer(uid)), do: raise "UID must be an integer; user cookie data is invalid" # In pathological situations, this could happen (considering we are reading user data). Better trigger an exception here than somewhere else down the line.
      if (!String.valid?(key)), do: raise "Security key is not a string; user cookie data is invalid"
      key_invalid = security_key_checker_func.(uid, key) != true
      conn
      |> assign(:security_key_invalid, key_invalid)
      |> assign(:uid, nil) # Default value, this will be overwritten if the key is valid
      |> assign((if (key_invalid), do: :proposed_uid, else: :uid), uid)
    else
      conn
      |> assign(:uid, nil)
      |> assign(:security_key_invalid, nil)
    end
  end

end