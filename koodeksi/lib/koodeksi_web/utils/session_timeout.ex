defmodule KoodeksiWeb.Utils.SessionTimeout do
  @moduledoc """
    This plug adds a session timeout functionality to all sessions (except if this is especially prevented by a special :remember_me field), and is independent of all other plugs.
  """

  import Plug.Conn

  def init(opts \\ []) do
    Keyword.merge([timeout_over_seconds: 7200], opts) # Inject a sane default value, if none is provided
  end

  @doc """
    The main function as expected from all plugs; this function will be called with each connection
"""
  def call(conn, opts) do
    timeouts_at = get_session(conn, :session_timeout_unixtime) # Retrieve the amount of seconds till timeout
    nonexpiring = (get_session(conn, :remember_me) == true) # Check if this session is nonexpiring

    if (timeouts_at != nil && unix_time() > timeouts_at && nonexpiring == false) do
      expire_session(conn)
    else
      put_session(conn, :session_timeout_unixtime, timeout_seconds_now(opts[:timeout_over_seconds]))
    end
  end

  defp expire_session(conn) do
    conn
    |> clear_session() # Clean all contents of a session
    |> configure_session([:renew]) # Renew the ID
    |> put_session(:session_expired, true) # Signal that this session expired; this may mean a special message is shown, or some other operation done.
  end

  defp unix_time() do
    # This is the easiest way is to get a easily comparable UNIX time, even though we need not to explicitly care about time zones, etc
    DateTime.utc_now() |> DateTime.to_unix()
  end

  defp timeout_seconds_now(seconds) do
    # Return a time that will timeout in exactly configured number of seconds from calling.
    unix_time() + seconds
  end
end