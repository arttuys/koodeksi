defmodule KoodeksiWeb.JSONController do
  @moduledoc """
    This controller specifically manages the JSON API endpoints
  """

  use KoodeksiWeb, :controller

  # Simple JSON hash with an "ok" field
  defp simple_ok(conn, res) do
    render_json_block(conn, %{"ok": res})
  end

  # Simple JSON hash with an error
  defp simple_err(conn, res) do
    render_json_block(conn, %{"error": res})
  end

  # Render a raw JSON block
  defp render_json_block(conn, res) do
    render(conn, "simple-result.json", %{result: res})
  end

  @doc """
    Attempts to sign up a new user. If successful, returns {:ok, "success"}, otherwise {:error, error_messages}. Success, more precisely, implies that the user with the specified details has been successfully created and can be logged into.
    Currently no further information is returned, although later there may be a need, for example, email validation.

    This is a relatively thin wrapper over the respective `signup_safe/2` function found in `Koodeksi.Operations.User`, and therefore for more precise behavioral details, one should look there.

"""
  def signup(conn, params) do
    # First, get rid of all unrelated keys, and only select those that are relevant to our functions. As we convert the keys to atoms soon, we want to limit the range of valid keys
    selected_data = params
                    |> Map.take(["username", "password", "confirm_password", "email", "confirm_email", "disclaimer_agree", "inactive", "admin"])
                    |> Enum.map(fn({key, val}) ->
                          if (Enum.member?(["disclaimer_agree", "inactive", "admin"], key)) do
                            {key, Koodeksi.Operations.Common.safe_boolean_transform(val)} # Transform these to booleans
                          else
                            {key, val} # As is
                          end
                        end)
    atomized_params = selected_data |> Enum.reduce(%{}, fn ({key, val}, acc) -> Map.put(acc, String.to_atom(key), val) end) # Convert to strings
    # Before going towards the signup, check that two passwords match, and emails also match
    cond do
    (atomized_params[:password] != atomized_params[:confirm_password]) -> simple_err(conn, "Passwords do not match")
    (atomized_params[:email] != atomized_params[:confirm_email]) -> simple_err(conn, "Emails do not match")
    true -> fn() ->
      # We have now verified that there are no conflicts within input. Let's now transfer control to the signup function closer to the database layer. The function will return a standard OK/error structure which will be then converted by Poison/other JSON encoder in use to the standard form
      res = Koodeksi.Operations.User.signup_safe(atomized_params, Koodeksi.Operations.User.is_uid_admin(conn.assigns[:uid]) == true)
      case res do
        {:ok, _} -> simple_ok(conn, "success")
        {:error, err} -> simple_err(conn, err)
        end
      end.()
    end
  end

  @doc """
    Attempts to log an user in. This takes two values, an username and a password. If they match and the account is allowed to log on (active), a session will be established for the user, and the UID returned
    Otherwise an error will be returned.
"""
  def login(conn, %{"username" => username, "password" => password}) when (is_bitstring(username) and is_bitstring(password)) do
    # First, call into the Koodeksi user operations, and check if the user is valid. Do not allow inactive users to log on.
    res = Koodeksi.Operations.User.check_credentials(username, password, false)
    (case res do
      {:ok, uid} -> (fn ->
                        log_in_user(conn, uid, Koodeksi.Operations.User.security_key_for_uid(uid)) # OK, those credentials were acceptable. Log in the user by establishing a session, and send an UID as a return
                        |> simple_ok(Koodeksi.Operations.User.generate_user_json_blob(uid)) # Also send in the user blob, to replace the old one with.
                     end)
      {:error, msg} -> (fn -> simple_err(conn, msg) end) # Unacceptable, return an error message without altering the session data
    end).()
  end

  def login(conn, _) do
    simple_err(conn, "Incomplete credentials supplied to request")
  end

  @doc """
    Attempts to create a new directory; this is a simple passthrough to the relevant Koodeksi Operations-function.
    As specified in its documentation, validation and permission checks take place there.
"""
  def op_new_dir(conn, %{"root_dir_id" => root_dir_id, "title" => title, "path_slug" => path_slug}) when (is_bitstring(root_dir_id) and is_bitstring(title) and is_bitstring(path_slug)) do
    render_json_block(conn, Koodeksi.Operations.Directory.new_directory_json(Koodeksi.Operations.Common.safe_str_to_int(root_dir_id), conn.assigns[:uid], title, path_slug))
  end

  def op_new_dir(conn, _) do
    simple_err(conn, "Incomplete parameters supplied")
  end

  def op_new_doc(conn, %{"root_dir_id" => root_dir_id, "title" => title, "path_slug" => path_slug}) when (is_bitstring(root_dir_id) and is_bitstring(title) and is_bitstring(path_slug)) do
    render_json_block(conn, Koodeksi.Operations.Document.new_document_json(Koodeksi.Operations.Common.safe_str_to_int(root_dir_id), conn.assigns[:uid], title, path_slug))
  end

  def op_new_doc(conn, _) do
    simple_err(conn, "Incomplete parameters supplied")
  end

  @doc """
    Triggers a logout, which in its current implementation only destroys the cookie containing the authentication token, but it could later on mean, say, destruction of a key-value pair in a centralized structure.
"""
  def logout(conn, _) do
    conn
    |> log_out_user()
    |> simple_ok("success")
  end


  @doc """
    Returns an user page preload; this is different from an user JSON blob, as this generally contains more data, whileas an user JSON blob only contains the bare necessities to properly address the user and determine if he or she has global admin status
"""
  def get_user_page_preload(conn, %{"username" => username}) do
    render_json_block(conn, Koodeksi.Operations.User.generate_user_page_json_preload_by_username(username, conn.assigns[:uid]))
  end

  def get_user_page_preload(conn, _), do: render_json_block(conn, %{"result" => "notfound"})

  @doc """
    Returns an user JSON blob as perceived by the server;
"""
  def get_user_blob(conn, _) do
    render_json_block(conn, %{user: Koodeksi.Operations.User.generate_user_json_blob(conn.assigns[:uid])}) # This is very straightforward; return an user blob, as defined by the apparent UID in a session
  end

  @doc """
    Resolves a given path, and returns a trampoline preload for it, in the scope of the user who is doing the request and using the global root directory ID
"""
  def resolve_path(conn, %{"path" => path}) when (is_bitstring(path)) do
    render_json_block(conn, Koodeksi.Operations.DirDocument.generate_trampoline_preload(path, conn.assigns[:uid], Application.get_env(:koodeksi, :root_dir_id)))
  end

  def resolve_path(conn, _) do
    render_json_block(conn, %{"result": "error"}) # Match the error value from trampoline preloader
  end

  def document_gateway(conn, %{"doc_id" => doc_id, "transaction_data" => data}) when is_bitstring(doc_id) and is_bitstring(data) do
    # Attempt to decode the JSON
    decoded_json = Poison.decode(data)
    case decoded_json do
      {:ok, rest} -> render_json_block(conn, Koodeksi.Operations.Document.document_gateway(Koodeksi.Operations.Common.safe_str_to_int(doc_id), conn.assigns[:uid], rest)) # Nothing implemented yet
      _ -> render_json_block(conn, %{"result": "error"})
    end
  end

  def document_gateway(conn, _) do
    render_json_block(conn, %{"result": "error"}) # Match the error value from trampoline preloader
  end
end