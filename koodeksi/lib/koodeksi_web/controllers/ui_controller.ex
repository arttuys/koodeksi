defmodule KoodeksiWeb.UIController do
  use KoodeksiWeb, :controller

  # A default rendering function; this does not inject anything to the file
  def empty(conn, _params) do
    #IO.puts(inspect conn)
    render(conn, "empty.html", title: "")
  end

  # A directory/document view; this parses the address, and decides if one should send either a document or a directory preloading structure.
  def dir_doc_view(conn, params) do
    # TODO
    render(conn, "simple_preload.html", title: "", to_preload: Koodeksi.Operations.DirDocument.generate_trampoline_preload(Enum.join(params["path"], "/"), conn.assigns[:uid], Application.get_env(:koodeksi, :root_dir_id)))
  end
end
