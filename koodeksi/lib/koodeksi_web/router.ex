defmodule KoodeksiWeb.Router do
  use KoodeksiWeb, :router

  @doc """
    This function plug retrieves an auth session using the AuthTools library
"""
  def auth_session_retrieval(conn, _) do
    conn |> KoodeksiWeb.Utils.AuthTools.user_auth_session_to_assigns(&Koodeksi.Operations.User.verify_security_key_for_uid/2)
  end

  # Define known error types; routes for them will be generated further down the file
  @known_error_types ["notfound", "internalerr", "unauthorized", "service_unavailable", "network_ban"]

  pipeline :browser do
    plug KoodeksiWeb.Utils.Netban.UIBan
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug KoodeksiWeb.Utils.SessionTimeout, timeout_after_seconds: 3600
    plug :auth_session_retrieval
  end

  # Standard API stack, require CSRF on all non-GET requests
  pipeline :api do
    plug KoodeksiWeb.Utils.Netban.JSONBan
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
    plug KoodeksiWeb.Utils.SessionTimeout, timeout_after_seconds: 3600
    plug :auth_session_retrieval
  end

  scope "/", KoodeksiWeb do
    pipe_through :browser # Use the default browser stack

    get "/", UIController, :empty
    get "/signin", UIController, :empty # Sign-in does not so far require any special preloading
    get "/signout", UIController, :empty # Nor does a sign-out require such
    get "/signup", UIController, :empty # Nor a sign-up
    Enum.each(@known_error_types, fn(x) ->
      get ("/errors/" <> x), UIController, :empty # All errors lead to a blank UI state, as they do not have anything beyond ordinary to preload
    end)

    # Define the dir/document view; this is a special case
    get "/content/*path", UIController, :dir_doc_view

    # For testing purposes now, define a quiet path
    get "/user/*username", UIController, :empty
  end

  scope "/json", KoodeksiWeb do
    pipe_through :api

    # Authentication related paths
    get "/auth/user_blob", JSONController, :get_user_blob
    post "/auth/login", JSONController, :login
    post "/auth/logout", JSONController, :logout
    post "/auth/signup", JSONController, :signup

    # Directory/document/user page paths resolver
    get "/data/resolve_path", JSONController, :resolve_path

    # Directory/document creation shorthands
    post "/operations/new_directory", JSONController, :op_new_dir
    post "/operations/new_document", JSONController, :op_new_doc

    # User page preload
    get "/data/user_preload", JSONController, :get_user_page_preload
    # Document gateway
    post "/document/gateway", JSONController, :document_gateway
  end

end
