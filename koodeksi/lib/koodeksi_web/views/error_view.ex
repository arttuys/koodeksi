defmodule KoodeksiWeb.ErrorView do
  use KoodeksiWeb, :view

  defp redirect_page(destination) do
    {:safe, escaped_dest} = Phoenix.HTML.html_escape(destination)

    Phoenix.HTML.raw(
    """
    <!DOCTYPE html>
    <title>Redirecting...</title>
    <meta http-equiv="refresh" content="0;url=#{escaped_dest}"/>
    """
    )
  end

  def render("404.html", _assigns) do
    redirect_page("/errors/notfound")
  end

  def render("500.html", _assigns) do
    redirect_page("/errors/internalerr")
  end

  # In case no render clause matches or no
  # template is found, let's render it as 500
  def template_not_found(_template, assigns) do
    render "500.html", assigns
  end
end
