defmodule KoodeksiWeb.JSONView do

  @doc """
    Simple structure rendering; render the provided structure as-is
    """
  def render("simple-result.json", %{result: blob}) do
    blob
  end
end