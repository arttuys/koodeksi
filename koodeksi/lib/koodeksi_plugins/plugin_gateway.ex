defmodule KoodeksiPlugins.PluginGateway do
  alias Koodeksi.Repo
  import Ecto.Query

  @moduledoc """
    This module defines a so-called "plugin gateway". For the backend, plugins are specialized components that:
        - Manage the processing logic for document blocks; translating clientside JSON <=> database.
            - Provide view data for an user (authentication will be handled by web components)
            - Act on provided edits (if applicable)
        - Manage plugin options, in response to transactions
            - Plugin options may be used for shared properties between different plugins, or options which do not depend on a single block.
            - Clientside pages may also use them for showing auxillary page content, e.g table of contents. This has been standardized in specifications

    Plugins are effectively stateless; they do not hold state between calls, and cannot take actions out of their own initiative. Any calls to plugins
    originate from some other serverside event, typically a page request.
"""
  use GenServer

  @doc """
    By default, start the plugin gateway with default provided plugin options
"""
  def start_link() do
    start_link(Application.get_env(:koodeksi, KoodeksiPlugins.PluginGateway))
  end

  def start_link(default) do
    IO.puts "Starting plugin gateway, with: #{inspect default}"
    GenServer.start_link(__MODULE__, default, name: KoodeksiPlugins.PluginGateway)
  end

  @doc """
    Returns a list of plugins available for pages
"""
  def list_of_plugins() do
    GenServer.call(KoodeksiPlugins.PluginGateway, :list_of_plugins)
  end

  @doc """
    Construct an empty DPO map for the given list of plugins.
"""
  def empty_dpos(list_of_plugins) do
    GenServer.call(KoodeksiPlugins.PluginGateway, {:empty_dpos, list_of_plugins})
  end

  @doc """
    Executes a generic overlay-applying transaction. It is expected that the transaction parameters contain at least the "type" parameter.
"""
  def overlay_transaction(doc_id, user_id, transaction_params) do
    GenServer.call(KoodeksiPlugins.PluginGateway, {:execute_transaction,  doc_id, user_id, transaction_params})
  end

  ###################################################################

  defp plugin_map_from_state(state) do
    Keyword.fetch!(state, :plugin_definitions)
  end

  def handle_call(:list_of_plugins, _from, state) do
    {:reply, plugin_map_from_state(state), state}
  end


  def handle_call({:empty_dpos, list_of_plugins}, _from, state) do
    # Pull in a list of available plugins
    avail_plugins = plugin_map_from_state(state)

    # Intersect the requested baseline DPOs with the list of available plugins
    req_dpos = Enum.filter(list_of_plugins, fn x -> Map.has_key?(avail_plugins, x)  end)

    # Return appropriately keyed empty DPO structures
    res = req_dpos |> Enum.reduce(%{}, fn key, map -> (map |> Map.put(key, avail_plugins[key].empty_dpo())) end)
    {:reply, res, state} # Do not alter state yet
  end

  ###################################################################


  @doc """
      Handles a general transaction. It is expected that a document ID at least is provided, otherwise this routine may cause an exception, leading to the plugin
      gateway restarting and cutting all connections that are in progress at that time
  """
  def handle_call({:execute_transaction, doc_id, user_id, params}, _from, state) do
    res = KoodeksiPlugins.Overlay.empty_overlay()
    # It has been verified that the transaction type is provided
    transaction_type = params["type"]
    plugins_available = plugin_map_from_state(state)

    res = multiplex_transaction_type(res, transaction_type, doc_id, user_id, params, plugins_available)

    {:reply, %{"result" => "success", "overlay" => KoodeksiPlugins.Overlay.to_json(res)}, state}
  end

  def multiplex_transaction_type(base_overlay, type, doc_id, user_id, all_params, plugins_available) do
    # First, prepare by determining privileges
    {not_error, can_view, can_edit} = Koodeksi.Operations.Document.document_privilege_check(nil, doc_id, user_id)

    cond do
      # All view-priv requiring transactions under this
      !not_error -> KoodeksiPlugins.Overlay.set_general_error(base_overlay, "Something went wrong, please try again in a brief while")
      !can_view -> KoodeksiPlugins.Overlay.set_general_error(base_overlay, "Permission denied")
      type == "load-block" -> load_block(base_overlay, doc_id, user_id, all_params, plugins_available, can_edit)
      # All view-priv requiring transactions under this
      !can_edit -> KoodeksiPlugins.Overlay.set_general_error(base_overlay, "Permission denied")
      true -> KoodeksiPlugins.Overlay.set_general_error(base_overlay, "Undefined transaction")
    end
  end

  @doc """
    Loads a block, and generates a overlay which adds this block to the view
"""
  def load_block(base_overlay, doc_id, user_id, %{"block_id" => block_id}, plugins_available, is_editor_user) do
    # Retrieve block data
    safe_num = Koodeksi.Operations.Common.safe_str_to_int(block_id)

    block_data = if (safe_num == nil), do: nil, else: Repo.one(from b in Koodeksi.DB.Block, where: b.id == ^block_id and b.document_id == ^doc_id)

    # Check the plugin name
    resolved_plugin = if (block_data == nil), do: nil, else: Map.get(plugins_available, block_data.plugin, nil)

    plugin_response = if (resolved_plugin == nil), do: nil, else: fn ->
      if (!is_editor_user) do
        resolved_plugin.load_block_view(user_id, doc_id, block_data.id, block_data.contents)
      else
        resolved_plugin.load_block_edit(user_id, doc_id, block_data.id, block_data.contents)
      end
    end.()

    if (plugin_response != nil) do
      IO.puts(inspect plugin_response)
    end

    final_res = if plugin_response == nil do
      (base_overlay |> KoodeksiPlugins.Overlay.set_general_error("Something went wrong, please try again after a bit"))
    else
     case plugin_response do
      {:ok, view, edit} -> KoodeksiPlugins.Overlay.compose_loaded_block(base_overlay, block_data.id, block_data.order, block_data.plugin, view, edit)
      {:ok, view} -> KoodeksiPlugins.Overlay.compose_loaded_block(base_overlay, block_data.id, block_data.order, block_data.plugin, view)
      _ -> base_overlay |> KoodeksiPlugins.Overlay.set_general_error("Something went wrong, please try again after a bit")
      end
    end

    final_res
  end

  def load_block(ovl, _, _, _, _) do
    ovl |> KoodeksiPlugins.Overlay.set_general_error("Incomplete data provided for loading")
  end




  # Callbacks, as copied from the default Elixir template

  #def handle_cast({:push, item}, state) do
  #  {:noreply, [item | state]}
  #end
end