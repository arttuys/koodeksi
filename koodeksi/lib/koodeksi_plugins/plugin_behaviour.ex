defmodule KoodeksiPlugins.PluginBehaviour do
  @moduledoc """
    This module file declares a "behaviour" for plugins; this is quite akin to an interface or a trait in other languages.

    Essentially, this behaviour specifies some common transaction types; the gateway itself will build appropriate overlay structures for the more common scenarios,
    to ensure handling them serverside will be relatively painless and easy
  """

  @type document_id :: Integer.t
  @type block_id :: Integer.t
  @type user_id :: Integer.t

  @type block_data :: Binary.t
  @type view_data :: Map.t
  @type edit_data :: Map.t
  @type dpo_map :: Map.t

  @doc """
    Defines a new block; provided are the user and document IDs, and a map of edit data, and map of appropriate document plugin options for this plugin.
    Plugin need not to check if the user has a permission to create a new block, it will be verified before this function is called

    If this is acceptable, one should return the contents of the new saved block, possibly altered plugin options, and an indication if a post-processing DPO update call is required; or if not, an error
"""
  @callback new_block(user_id, document_id, edit_data, dpo_map) :: {:ok, block_data, dpo_map, Boolean.t} | {:error, String.t}

  @doc """
    Defines a block update; this is essentially editing block's edit data, and updating it to something new. Upon return, one should provide appropriate new block data if the transaction was valid,
    as well as a new DPO map and indication if a rebuild is necessary
"""
  @callback update_block(user_id, document_id, block_id, edit_data, edit_data, dpo_map) :: {:ok, block_data, dpo_map, Boolean.t} | {:error, String.t}
  @doc """
    It can happen that blocks get deleted. The owning plugin is informed of this, as it may want to execute operations for it, varying from DPO cleanup to perhaps altering other structures as well
    User, document, block ID, DPO map given, expected to return an altered map and possibly a transaction, plus an indication if a DPO rebuild is required.

    Do note that this does not prevent a block from being removed! If this function fails, it will be logged, but the block will be regardless deleted
"""
  @callback block_deleted(user_id, document_id, block_id, dpo_map) :: {:ok, dpo_map, Ecto.Multi.t, Boolean.t} | nil


  @doc """
    It may be occasionally necessary to execute a full update of document-plugin options for a given document. This can happen, for example, when creating a new document. This is called when such an operation is necessary; essentially, the plugin should reconstruct a valid DPO
    Parameters are:
"""
  @callback clean_dpo(document_id, dpo_map) :: {:ok, dpo_map} | {:error, String.t}

  @doc """
    Each plugin should have a valid empty DPO that can be added to a document. It must not be dependent on document IDs et al; this function is used to query for a such structure
"""
  @callback empty_dpo() :: Map.t

  @doc """
    Load a block for viewing; provided are user, document and block IDs, with the binary data found from the block; if this succeeds, return a map.
    User permissions are checked, and this will not be called unless the user has an adequate view permission
    This map's contents will be saved as 'view_XXXX' blocks on the clientside
  """
  @callback load_block_view(user_id, document_id, block_id, block_data) :: {:ok, view_data} | {:error, String.t}

  @doc """
    Load a block for editing; provided are user, document and block IDs, with the binary data found from the block; if this succeeds, return a map of view and a map of edit data.
    User permissions are checked, and this will not be called unless the user has an adequate edit permission
    This map's contents will be saved as 'view_XXXX' and 'edit_XXXX' blocks on clientside
  """
  @callback load_block_edit(user_id, document_id, block_id, block_data) :: {:ok, view_data, edit_data} | {:error, String.t}

  @doc """
    Above transactions are the only "special" transactions defined; any other transactions require using this manual method, where you get essentially:
      - User, document, block IDs
      - Transaction type as a string
      - Map of auxillary content that has been included

    You are expected to either:
      - Return an overlay, which alters the view/edit manually, or does something else
      - Possibly provide an Ecto.Multi transaction; if this transaction doesn't go through, overlay won't be sent to the client
    or
      - Return an error message
"""
  @callback custom_transaction(Integer.t, Integer.t, Integer.t, String.t, Map.t) :: {:ok, KoodeksiPlugins.Overlay.t, Ecto.Multi.t} | {:error, String.t}
end