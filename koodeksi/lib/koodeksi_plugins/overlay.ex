defmodule KoodeksiPlugins.Overlay do
  @moduledoc """
    Defines an "overlay" - or, an alteration to the apparent clientside view. To enforce standard formatting, a structure will be used for this.
  """
  defstruct added: %{}, removed: [], updated: %{}, refresh_time: nil, full_resets: [], plugin_opts_updates: %{}, general_error: nil

  def empty_overlay() do
    %KoodeksiPlugins.Overlay{}
  end

  def set_refresh_time(struct, time) do
    if (!is_integer(time) || time < 1), do: raise "Provided refresh time must be an nonzero, positive integer!"
    struct |> Map.put(:refresh_time, time)
  end

  def set_plugin_opts(struct, opts) do
    struct |> Map.put(:plugin_opts_updates, opts)
  end

  def set_general_error(struct, err) do
    struct |> Map.put(:general_error, err)
  end

  @doc """
    Composes a loaded block; effectively, adds it as a new block with provided details. A convenience function for the plugin gateway,
    it follows the specification given in the clientside view root
"""
  def compose_loaded_block(struct, block_id, order, plugin_name, view_data, edit_data \\ %{}) do
    base_map = %{"loaded" => true, "order" => order, "plugin" => plugin_name, "errors" => []}

    composed_map = Enum.reduce(Map.keys(view_data), base_map, fn x, acc -> Map.put(acc, "view_" <> x, view_data[x]) end)
    IO.puts (inspect composed_map)
    recomposed_map = Enum.reduce(Map.keys(edit_data), composed_map, fn x, acc -> Map.put(acc, "edit_" <> x, edit_data[x]) end)

    struct |> add_block(block_id, recomposed_map)
  end

  @doc """
    View root "added"
"""
  def add_block(struct, block_id, block) do
    block_name = Integer.to_string(block_id)
    # Enforce consistency; additions/replacements override everything else
    struct |> Map.update!(:added, fn x -> x |> Map.put(block_name, block) end)
    |> Map.update!(:removed, fn x -> Enum.reject(x, fn y -> y == block_name end) end)
    |> Map.update!(:full_resets, fn x -> Enum.reject(x, fn y -> y == block_name end) end)
    |> Map.update!(:updated, fn x -> Map.drop(x, [block_name]) end)
  end

  @doc """
      View root "updated"
  """
  def update_block(struct, block_id, block) do
    block_name = Integer.to_string(block_id)
    # Enforce consistency; updates override additions or removals
    struct |> Map.update!(:updated, fn x -> x |> Map.put(block_name, block) end)
    |> Map.update!(:removed, fn x -> Enum.reject(x, fn y -> y == block_name end) end)
    |> Map.update!(:full_resets, fn x -> Enum.reject(x, fn y -> y == block_name end) end)
    |> Map.update!(:added, fn x -> Map.drop(x, [block_name]) end)
  end
  @doc """
      View root "removed"
  """
  def remove_block(struct, block_id) do
    block_name = if is_integer(block_id), do: Integer.to_string(block_id), else: block_id
    # Enforce consistency; if removed, no other action can take place
    struct |> Map.update!(:removed, fn x -> x |> Enum.concat( [block_name]) end)
    |> Map.update!(:updated, fn x -> Map.drop(x, [block_name]) end)
    |> Map.update!(:full_resets, fn x -> Enum.reject(x, fn y -> y == block_name end) end)
    |> Map.update!(:added, fn x -> Map.drop(x, [block_name]) end)
  end

  @doc """
      View root "full_resets"
  """
  def reset_block(struct, block_id) do
    block_name = if is_integer(block_id), do: Integer.to_string(block_id), else: block_id
    # Enforce consistency; no other action can take place with a reset
    struct |> Map.update!(:full_resets, fn x -> x |> Enum.concat( [block_name]) end)
    |> Map.update!(:updated, fn x -> Map.drop(x, [block_name]) end)
    |> Map.update!(:removed, fn x -> Enum.reject(x, fn y -> y == block_name end) end)
    |> Map.update!(:added, fn x -> Map.drop(x, [block_name]) end)
  end

  ############

  @doc """
      JSON conversion from a struct; this removes all nil keys
  """
  def to_json(%KoodeksiPlugins.Overlay{:added => added, :removed => removed, :updated => updated, :full_resets => full_resets, :refresh_time => refresh_time, :plugin_opts_updates => pou, :general_error => err}) do
    base_map = %{"added" => added, "removed" => removed, "updated" => updated, "full_resets" => full_resets, "refresh_time" => refresh_time, "plugin_opts_updates" => pou, "general_error" => err}

    zero_keys = Map.keys(base_map) |> Enum.filter(fn key -> Map.get(base_map, key) == nil || Map.get(base_map, key) == %{} || Map.get(base_map, key) == []  end)
    #IO.puts(inspect zero_keys)
    #IO.puts(inspect base_map)
    #base_map
    Enum.reduce(zero_keys, base_map, fn (key, map) -> Map.drop(map, [key]) end)
  end
end
