defmodule KoodeksiPlugins.Plugins.HTML do
  @moduledoc """
    The serverside implementation of a baseline HTML plugin. Its job isn't to do much more than validate the technical validity of contents
    sent from the clientside, store it, and return it on recall.
"""

  @behaviour KoodeksiPlugins.PluginBehaviour


  defp map_from_string(str) do
    # Simple helper function to decode from a string
    Poison.decode(str)
  end

  @doc """
    Creates a new block; this basically parses Markdown and makes a HTML subsection out of it
"""
  def new_block(user_id, document_id, edit_data, dpo_map) do
    cond do
      # Check that we have valid edit data
      edit_data == nil -> {:error, "Missing or invalid input data"}
      !Enum.all?(["markdown"], fn x -> Map.has_key?(edit_data, x) && is_bitstring(edit_data[x]) end) -> {:error, "Missing or invalid input data"}
      true -> fn ->
        # Attempt to parse into HTML
        html_res = Earmark.as_html(edit_data["markdown"], %Earmark.Options{gfm: true})
        case html_res do
          {:ok, html_doc, _} -> fn ->
            # We now have valid Markdown and HTML content
            result_json = Poison.encode!(%{"markdown" => edit_data["markdown"], "html" => html_res})
            {:ok, result_json, dpo_map, true} # We need to refactor our DPOs soon after this to properly reflect the TOC
          end.()
          {:error, html_doc, errors} -> {:error, "Markdown invalid: #{inspect errors}"}
        end
      end.()
    end
  end

  @doc """
    Updates a block; this is in many ways essentially the same as creating a new block. In this case, we'll delegate it to our new_block/4 function, as it is quite simple what we do
"""
  def update_block(user_id, document_id, _block_id, _old_edit_data, edit_data, dpo_map) do
    new_block(user_id, document_id, edit_data, dpo_map)
  end

  def clean_dpo(document_id, dpo_map) do
    {:ok, dpo_map} # TODO: Implement actual rebuilding for TOC
  end

  def block_deleted(_, doc_id, _, dpo_map) do
    # This is simple enough
    {:ok, dpo_map, nil, true}
  end

  def empty_dpo() do
    # We do not have a special initial state we'd like to apply. Return an empty map
    %{}
  end

  def load_block_view(user_id, document_id, block_id, block_data) do
    map = Poison.decode(block_data)
    case map do
      {:ok, map}-> {:ok, %{"html" => map["html"]}}
       _-> {:error, "Unable to decode saved data"}
    end
  end

  def load_block_edit(user_id, document_id, block_id, block_data) do
    map = Poison.decode(block_data)
    case map do
      {:ok, map}-> {:ok, %{"html" => map["html"]}, %{"markdown" => map["markdown"]}}
      _-> {:error, "Unable to decode saved data"}
    end
  end

  def custom_transaction(_, _, _, _, _), do: {:error, "Custom transactions are not supported for HTML plugin"}
end