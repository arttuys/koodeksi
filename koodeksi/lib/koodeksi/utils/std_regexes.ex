defmodule Koodeksi.Utils.StdRegexes do
  @moduledoc """
    This module defines a few common Regex expressions that are required in several places
"""

  @sane_title ~r/\A(\pL|\pN)+((\pL|\pN|\pP| )*(\pL|\pN)+)*\z/ui

  @doc """
    Returns a regex that will match iff the string presented could be considered a sane title - containing only characters, digits and certain symbols
"""
  def sane_title, do: @sane_title

  @sane_slug ~r/\A[a-zA-Z0-9_\-]+\z/

  @doc """
    Returns a regex that will match iff the string presented is a sane slug; that is, a string that could be a reasonable part of an URL path. This is much more stringent than a sane title check.
    Also note that this is actually a subset of valid URL characters, as, for instance, '$' is reserved for documents

    Same rule is also used for plugin names, as they need to be in URLs as well
"""
  def sane_slug, do: @sane_slug

  @sane_path_url ~r/\A([a-zA-Z0-9_\-]+\/)*(([a-zA-Z0-9_\-]+)?\$[a-zA-Z0-9_\-]+)\z/

  @doc """
    Returns a regex that will match iff the string presented is a sane path url; a sane path consists of one or more valid slugs, last part containing a $, possibly prefixed with a specific identifier if required,
"""
  def sane_path_url, do: @sane_path_url

  @doc """
    Returns a regex that matches some names that while otherwise valid, may not be desirable to be allowed for a user due to the possible confusion generated otherwise.
"""
  @reserved_names ~r/\A(null|undefined|document|directory|block|log|user)\z/i

  def reserved_names, do: @reserved_names
end