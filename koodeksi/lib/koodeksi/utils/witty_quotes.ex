defmodule Koodeksi.Utils.WittyQuotes do
@moduledoc """
  A simple module that dispenses witty quotes, one at a time.

  It is used for the loading blurb; also, due to Elixir being relatively new as a language, this is used to get started with testing.
"""

@quotelist ["Did you know that Koodeksi consists of Elixir magic, and is open source? Take a peek!",
            "Ever wondered how the UI reacts to your actions? Download a Vue.js dev extension for your browser today!",
            "Everyone needs type holes!",
            "Have you yet tried functional programming? It is healthy to twist your mind upside down at least once!",
            "Shall we crush some glitches today?",
            "Remember, JavaScript ≠ Java",
            "Comment in time saves nine", 
            "What masterpiece shall we paint today in Joy of Coding?",
            "Q/A is important to avoid things catching on fire",
            "Code does exactly what you tell it to do, no more, no less.",
            "Proportionality is important in everything you do; avoid slacking, but do not drown yourself in inconsequential work either",
            "Programming with N friends is N+1 times as fun!",
            "Summoning the code from the infinite depths of the database",
            "READY, SET, GO!",
            "Refactor often and refactor everything!",
            "Ooh, a visitor! Welcome!",
            "Aitoa suomalaista käsityötä!",
            "\“Don’t worry if it doesn’t work right. If everything did, you’d be out of a job.\” - Mosher's Law of Software Engineering",
            "\"Measuring programming progress by lines of code is like measuring aircraft building progress by weight.\" - Bill Gates",
            "Are you suggesting databases MIGRATE??",
            "\"Everyone stand back, I know regular expressions.\" - XKCD 208",
            "No one can learn for you, you must learn and master the trade with your own effort. Let's make it fun!",
            "Always remember to keep a notepad near you, as brilliant ideas may strike without a warning!",
            "Spooky scary spam emails!",
            "You found me!",
            "Misbehavior can result in a ban! Behave nicely, there are other people here.",
            "Be calm and gentle, slamming F5 may result in a throttle or a temporary network ban for you.",
            "\"sudo\" is a dangerous weapon. Use it wisely.",
            "Unexplainable things are a part of this world. If it doesn't work today, come back later and try again.",
            "Paparazzis prefer trenchcoats and dark sunglasses. Be aware if you see any, they may want a shot of you for your coding fame!",
            "Comment constructively! Everyone will enjoy it better!",
            "Halt! Who goes there, friend or foe?",
            "Do not lose your cookies! They persist your identity.. unless, of course, you do not wish to be identified",
            "Be careful, C has a lot of undefined behavior. That means, the compiler can do anything it chooses in such a scenario",
            "Do not settle for inferior, outdated tools; be sure to look out for any new improvements!",
            "Big Data!",
            "Do not become a confused deputy; check your orders!",
            "Path traversals can reveal secrets. Be careful!",
            "https://xkcd.com/1658/",
            "End result is not as important as the journey there!"
          ]

  @doc """
  Dispenses a random String quote from a list. All strings dispensed are safe to directly inline with HTML code, as they are escaped
"""
  def random_quote() do
    Phoenix.HTML.html_escape(Enum.random(@quotelist)) # This returns a tuple, which indicates that this string is safe
  end

  @doc """
  Returns an approximate reading time one would need the quote to be visible for it to be easily readable.
  It can be intepreted as required; if loading took very long, this time can be ignored entirely, or if loading was effectively instaneous, this whole time can be waited as needed.

  This is calculated under these assumptions: the desired reading speed will be 150 WPM, and as the average length of an English word is 5 letters, this comes to 1000 letters per minute or approximately 12.5 per second.
  Inversely, this means that each letter is to be allocated roughly 0.08 seconds.

  In addition to this approximate time, 3 seconds will be added to allow the attention to be focused into the text
"""
  def reading_time(str) do
    round(3 + (0.08 * String.length(str)))
  end
end
