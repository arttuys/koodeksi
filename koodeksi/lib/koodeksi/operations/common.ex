defmodule Koodeksi.Operations.Common do
  alias Koodeksi.Repo
  alias Koodeksi.DB.Log
  # Define a log origin as an environment variable
  @log_origin Application.get_env(:koodeksi, :server_name)

  @moduledoc """
    This file defines common shared properties and functionality for operations, e.g database transaction helpers
  """

  @doc """
    Logs the simplest type of an event, a simple message with no references
"""
  def log_simple_text(options \\ []) do
    defaults = [sensitive: false]
    %{text: text, sensitive: sensitive} = Keyword.merge(defaults, options) |> Enum.into(%{})
    Repo.insert(Log.changeset(%Koodeksi.DB.Log{}, %{:log_data => %{"message" => text}, :sensitive => sensitive, :originating_server => @log_origin}))
  end

  @doc """
    Logs a simple user action of some nature, with an optional ID.
"""
  def log_user_action(options \\ []) do
    defaults = [id: nil, sensitive: false]
    %{id: id, text: text, sensitive: sensitive} = Keyword.merge(defaults, options) |> Enum.into(%{})
    Repo.insert!(Log.changeset(%Koodeksi.DB.Log{}, %{:initiating_user_id => id, :log_data => %{"message" => text}, :sensitive => sensitive, :originating_server => @log_origin}))
  end

  @doc """
      Logs a directory-user action of some nature
  """
  def log_dir_user_action(options \\ []) do
    defaults = [user_id: nil, directory_id: nil, sensitive: false]
    %{user_id: id, directory_id: directory, text: text, sensitive: sensitive} = Keyword.merge(defaults, options) |> Enum.into(%{})
    Repo.insert!(Log.changeset(%Koodeksi.DB.Log{}, %{:initiating_user_id => id, :directory_id => directory, :log_data => %{"message" => text}, :sensitive => sensitive, :originating_server => @log_origin}))
  end

  def changeset_errors_to_list(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, _} -> msg
    end)
    |> Map.to_list
    |> Enum.map(fn {key, val} ->
      "#{to_string(key) |> String.capitalize}: #{val |> Enum.reduce(&("#{&2}, #{&1}"))}"
    end)
  end

  @doc """
    Defines a safe boolean transformation; in JSON, booleans may be translated to strings, which is nonideal for for native Elixir code. This snippet transforms such boolean values safely to their native representations, or to nil if it cannot be done
"""
  def safe_boolean_transform("true"), do: true

  def safe_boolean_transform("false"), do: false

  def safe_boolean_transform(true), do: true

  def safe_boolean_transform(false), do: false

  def safe_boolean_transform(_), do: nil

  @doc """
    Defines a safe string-to-integer transform; all integers are passed as is, strings which resolve to valid integers are returned as such, and all others are nil
"""
  def safe_str_to_int(int) when is_integer(int), do: int

  def safe_str_to_int(str) when is_bitstring(str) do
    case Integer.parse(str) do
      {int, _} -> int
      _ -> nil
    end
  end

  def safe_str_to_int(_), do: nil

end