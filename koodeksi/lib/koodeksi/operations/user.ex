defmodule Koodeksi.Operations.User do
  alias Koodeksi.Repo
  alias Koodeksi.Operations.Common
  import Ecto.Query
  @moduledoc """
    This file defines several user-related operations, e.g verifying credentials, executing a signup, and generating an user data JSON blob
  """
  def check_credentials(username, password, allow_inactive) when username == nil or password == nil or allow_inactive == nil, do: {:error, "Please provide all information requested"}

  @doc """
      Checks if provided credentials are valid from the database

      If they are valid, a tuple `{:ok, user_id}` will be returned. If not, `{:error, message}`; the message can be shown to the user,
      as it will be ensured that nothing harmful will be revealed.

      This function will log all attempts, and will fail with an exception if logging fails (as it is not an ordinary situation).
  """
  def check_credentials(username, password, allow_inactive) when (is_bitstring(username) and is_bitstring(password) and is_boolean(allow_inactive)) do
    user = Repo.one(from u in Koodeksi.DB.User, where: u.username == ^username)
    res = cond do
      (user == nil or (Comeonin.Bcrypt.checkpw(password, user.pwd_hash) == false)) -> {:error, "Invalid username or password"} # The user was not found
      (user.inactive == true && !allow_inactive) -> {:error, "This account is not active, and therefore can not be logged into."}
      (true) -> {:ok, user.id}
    end

    res_message_fn = fn
      {:ok, uid} -> "Successful authentication for #{username}, identified by ID #{uid}"
      {:error, msg} -> "Failed authentication (#{msg}), for user '#{username}', ID #{user.id}"
    end

    cond do
      (user == nil) -> Common.log_simple_text(text: "Login attempt to nonexistent username '#{username}'", sensitive: true)
      (true) -> Common.log_user_action(id: user.id, text: res_message_fn.(res))
    end

    # Return res
    res
  end

  def check_credentials(%{:username => u, :password => p, :inactive => i}) when (is_bitstring(u) and is_bitstring(p) and is_boolean(i)) do
    check_credentials(u, p, i)
  end

  def check_credentials(nil), do: {:error, "Please provide all information requested"}

  @doc """
    Attempts to create a new user. Returns {:ok, user_struct} if this succeeded, otherwise {:error, error_list} if it did not.

    This function is dangerous; it places no intristic limits beyond the ones required by the data format, and therefore can create admin users. Use `signup_safe/3` for user-exposed forms.
    Do notice that error_list will always be a list of strings, and contains one or more error messages.

    This function will fail with a exception if it is unable to log the signup; errors are not logged permanently, as they are, most likely, not of great interest.

    See `Koodeksi.DB.User` for precise parameter definitions; parameter keys are expected to be atoms
"""
  def signup_full(params) do
    Repo.transaction(fn ->
      # Attempt first to insert a new user;
      transaction_res = Repo.insert(Koodeksi.DB.User.signup(params))
      # Check what the state is, and attempt to log it
      log_res = case transaction_res do
        {:ok, user} -> (fn ->
          Common.log_user_action(id: user.id, text: "Successful sign-up: UID #{user.id}, name '#{user.username}', email: #{user.email}, inactive: #{user.inactive}, is admin: #{user.administrative_user_date != nil}'", sensitive: true)
          user
        end)
        {:error, changeset} -> (fn ->
          Repo.rollback(Common.changeset_errors_to_list(changeset))
        end)
      end

      log_res.()
    end)
  end

  @doc """
    Attempts to create a new user, as in `signup_full/1`
    However, this function contains a special `superuser` parameter. When it is true, this function works essentially like the the `signup_full/1`

    If, however, superuser is false, then following will happen:
      - `disclaimer_agree` is expected be true in the params, or otherwise an error will be returned. This is to enforce that TOS has been accepted, by requiring that an affirmation as such has been explicitly sent to server
      - `inactive` and `admin` are both set to false, and stripped out of the parameters. This prevents creation of administrative or spammy, inactive accounts from user-entered data

    Other fields not used by `signup_full/1` will be ignored; the caller is expected to check other restrictions (e.g CAPTCHA) before calling this function.
"""
  def signup_safe(params, superuser \\ false) do
    if (superuser) do
      # Superuser mode, do not check for TOS acceptance
      signup_full(params)
    else
      # Enforce that TOS/disclaimer is accepted
      if (params[:disclaimer_agree] == true) do
        # If so, drop the dangerous options
        signup_full(Map.drop(params, [:inactive, :admin]) |> Map.merge(%{:admin => false, :inactive => false}))
      else
        {:error, "You must agree to the disclaimer to sign up for an account"}
      end
    end
    #
  end

  @doc """
    Determines a so-called "security key" for an UID, or returns nil if it cannot be determined.

    Current implementation is a relatively simple one, which takes the last 6 characters of the password hash. This has following effects:
      - The security key is usually invalidated on password changes; this is a desirable effect, to assure that there is an easy, user-accessible way to invalidate any sessions known and unknown to them
      - The chance of a password change generating a security key that matches the previous one is minimal.
          - Hashes are stored as Base64 codes, drawing from the character set: './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789' - which is, 64 characters
          - Assuming there is equal probability for any 6-character combination, the chance of generating a randomly matching combination is 1/(64^6), which is roughly 1.4551915228366851806640625 × 10^-11
            - This is roughly 1 out of 68 billion 719 million 476 thousand and 736 - an extremely small chance of either randomly guessing or randomly generating a matching pair. You are substantially more likely to win the jackpot in a lottery (1 in 15 million)
      - The security key doesn't take a substantial amount of space to store, and therefore is suitable for cookie-based sessions.
      - It is relatively quick to query from a database, and can be used with distributed services as the security key does not depend on any server-specific state

    This also has the feature of enforcing inactiveness; if the user is declared inactive, no valid key will be returned in any case. This effectively enforces bans; if the user is declared inactive, they are no longer allowed to execute operations in an user context.

    This may, possibly, be later replaced by something else - but for the time being, this should by above reasoning be a fairly sane and secure implementation. It also has the additional bonus of implicitly filtering out nonexistent UIDs, as obviously one cannot have a valid hash if one doesn't even exist. Therefore, no nonexistent UID can have any valid security key, and by extension no session for such UIDs will be accepted.
"""
  def security_key_for_uid(uid) do
    query = from u in Koodeksi.DB.User, where: u.id == ^uid, select: {u.pwd_hash, u.inactive}
    res = Repo.one(query)
    {hash, inactive} = if (res != nil), do: res, else: {nil, nil} # Conditional pattern match; if the result is nil/not found, treat the elements as nil as well. A simple pattern match would crash!

    if (hash != nil && !inactive) do
      String.slice(hash, -6..-1) # Slice the last 6 characters of a string
    else
      nil # No hash, no result; or alternatively, the user is inactive
    end
  end

  @doc """
    Verifies a security key for an UID; this can be passed directly as a parameter to AuthTools
"""
  def verify_security_key_for_uid(uid, security_key) do
    correct_key = security_key_for_uid(uid)
    (correct_key != nil && security_key == correct_key)
  end

  @doc """
    Generates a structure called "User JSON blob"; this should be preloaded into each page on sending, and may be requested separately from a specific JSON URL after session state changes (e.g login, logout)
    It is not an error to call with a nil UID; in this case, an empty structure will be returned which signals that there is no logged in user.

    This will also return a nil value if the user doesn't exist at all.

    An user blob contains:
      - UID
      - Username
      - Administrative status (is/is not an admin)
"""
  def generate_user_json_blob(nil) do
    nil
  end

  def generate_user_json_blob(uid) do
    Repo.one(from(u in Koodeksi.DB.User, where: u.id == ^uid, select: %{"uid": u.id, "username": u.username, "admin": not is_nil(u.administrative_user_date)}))
  end

  @doc """
    Generates an user page JSON preload. This preload contains basic information about the user: username, email, inactivity and administrative information (if so permitted)

    If successful, the returned structure will be as follows

    ```
    {
      "result" => "success"
      "admin_status" => <true | false> # If this is true, the user viewing this page has admin privileges and can exert special changes to this user.
      "can_edit" => <true | false> # If this is true, the user in question can make ordinary edits. This may be caused by being the user itself, or alternatively an administrator
      "username" => <username as a string>
      "email" => <email as a string>
      "is_admin" => <is the viewed user an admim>

      # Following data is only sent if the viewer is an admin
      "administrative_date" => <date of administrative privileges>
      "inactive" => <true | false> # True if user is inactive
    }
    ```
"""

  def generate_user_page_json_preload(nil, _) do
    %{"result" => "error"}
  end

  def generate_user_page_json_preload(target_user_uid, scope_uid) when is_integer(target_user_uid) do
    has_admin_privileges = compare_admin_dates(target_user_uid, scope_uid)
    can_edit = (has_admin_privileges || (target_user_uid == scope_uid)) && (target_user_uid != nil && scope_uid != nil) # Ordinary edits can either be executed by an admin, or by the user themselves. Add also an extra logic safeguard assuring that guests cannot edit even if some other parts of the logic fail

    # Fill the first information records
    base_info = %{"admin_status" => has_admin_privileges, "can_edit" => can_edit}
    # Try to retrieve the basic info for the targeted user
    query = from(u in Koodeksi.DB.User, where: u.id == ^target_user_uid, select: %{"username" => u.username, "email" => u.email, "is_admin" => not is_nil(u.administrative_user_date), "inactive" => u.inactive, "administrative_date" => u.administrative_user_date})
    data = Repo.one(query)


    # If this is nil, we haven't found what we want
    res = (if (data == nil) do
      %{"result" => "notfound"}
    else
      merged_data = if (!has_admin_privileges), do: merged_data = Map.drop(data, ["administrative_date", "inactive"]), else: data

      # Construct a result map; report success, merge base info and appropriately selected data
      res = %{"result" => "success", "user_id" => target_user_uid} |> Map.merge(base_info) |> Map.merge(merged_data)
    end)

    res
  end

  def generate_user_page_json_preload(_, _), do: %{"result" => "error"}

  @doc """
    Similar to `generate_user_page_json_preload/2`, but instead of having a target UID, instead attempt to find a target by username. If no such username is found, returns a not found error
"""
  def generate_user_page_json_preload_by_username(target_username, scope_uid) when is_bitstring(target_username) do
    found_uid = Repo.one(from(u in Koodeksi.DB.User, where: u.username == ^target_username, select: u.id))
    if (found_uid == nil), do: %{"result" => "notfound"}, else: generate_user_page_json_preload(found_uid, scope_uid)
  end

  def generate_user_page_json_preload_by_username(_, _), do: %{"result" => "error"}

  @doc """
    Checks if the specified UID is an administrator: returns true or false if possible to determine, and nil otherwise
"""

  def is_uid_admin(nil) do
    nil
  end

  def is_uid_admin(uid) do
    Repo.one(from(u in Koodeksi.DB.User, where: u.id == ^uid, select: not is_nil(u.administrative_user_date)))
  end

  def compare_admin_dates(_, nil), do: false

  @doc """
    Compares administrative user dates for two users. As per policy, it is required that one administrator is more "senior" than another to commit an operation on them
    This function checks that; if it returns true, operating user can execute admin operations over the target user

    If the target UID is nil, it will be assumed it is a guest, and therefore permissions are granted if the user is an admin. Nil operating users will always return false, as obviously they cannot have extra permissions.
  """
  def compare_admin_dates(target_uid, operating_user) do
    target_user_date = if target_uid == nil, do: nil, else: Repo.one(from(u in Koodeksi.DB.User, where: u.id == ^target_uid, select: u.administrative_user_date))
    operating_user_struct = Repo.one(from(u in Koodeksi.DB.User, where: u.id == ^operating_user, select: {u.administrative_user_date, u.inactive}))
    {operating_user_date, inactive} = if operating_user_struct != nil, do: operating_user_struct, else: {nil, false}

    (operating_user_date != nil && (!inactive) && (target_user_date == nil || NaiveDateTime.compare(operating_user_date, target_user_date) == :lt || target_uid == operating_user)) # Return true iff the operating user has administrative privileges, and the target either doesn't have them, doesn't exist, or the operating user has an earlier date (unless the admin wants to operate on themselves). Also enforce that inactive users cannot exert administrative privileges
  end
end