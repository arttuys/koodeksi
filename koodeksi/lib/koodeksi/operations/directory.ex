defmodule Koodeksi.Operations.Directory do
  alias Koodeksi.Repo
  import Ecto.Query

  @moduledoc """
    Directory-specific operations, e.g creating a new directory
"""

  @doc """
    Attempts to create a new directory. Returns an OK/error tuple, as in:
      {"ok": true} if successful
      {"error": <error message>} if unsuccessful

    This function is safe to call directly, as it enforces baseline validation and permission checks, refusing to commit actions which are not permitted.
    However, this function specifically enforces that a path slug cannot be nil when a parent directory is non-nil; whileas that would otherwise be technically permitted, it is not typically a desired situation.
"""
  def new_directory_json(root_id, user_id, title, slug) when ((is_integer(root_id) and root_id >= 0) and (is_integer(user_id) and user_id >= 0) and is_bitstring(title) and is_bitstring(slug)) do
    # Request permissions first; we need an edit permission to be able to create a new directory.
    perm_res = Koodeksi.Operations.DirDocument.user_directory_privileges(root_id, user_id, [:edit])

    # One particular catch is that for the time being, transactions are flattened (and therefore, no savepoints as normally supported in Postgres). Therefore, a transaction fail at a previous level could have unexpected and undesired consequences - and for that reason, we do not wrap this functionality into a transaction.
    cond do
      elem(perm_res, 0) == :denied -> %{"error" => "Permission denied"}
      elem(perm_res, 0) == :error -> %{"error" => "Unable to check that you are authorized to do this action, please try again later"}
      (root_id != nil && slug == "") -> %{"error" => "You must provide a slug for this directory"}
      # All other possible results are valid
      true -> fn ->
                 IO.puts("Adding with slug: #{inspect slug}")
                 changeset = Koodeksi.DB.Directory.new_directory(%{:title => title, :path_slug => slug, :owner_id => user_id, :parent_directory_id => root_id, :anyone_can_edit => true, :anyone_can_access => false, :visible_to_all => true})
                 case Repo.insert(changeset) do
                   {:ok, struct} -> %{"ok" => true} # This is OK
                   {:error, changeset} -> %{"error" => Koodeksi.Operations.Common.changeset_errors_to_list(changeset)}
                 end
              end.()
    end
  end

  def new_directory_json(_, _, _, _) do
    %{"error" => "Incomplete or invalid information supplied"}
  end

end