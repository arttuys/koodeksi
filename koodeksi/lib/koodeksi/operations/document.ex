defmodule Koodeksi.Operations.Document do
  alias Koodeksi.Repo
  import Ecto.Query

  @moduledoc """
    This module contains document-specific operations, e.g document gateway entrypoint
  """

  @doc """
    Attempts to create a new empty document. Returns an OK/error tuple, as in:
        {"ok": true} if successful
        {"error": <error message>} if unsuccessful

    This function is safe to call directly from routing, as validation and permission checks are enforced.
"""
  def new_document_json(root_id, user_id, title, slug) when ((is_integer(root_id) and root_id >= 0) and (is_integer(user_id) and user_id >= 0) and is_bitstring(title) and is_bitstring(slug)) do
    # First, check that we are factually allowed to modify the base folder
    perm_res = Koodeksi.Operations.DirDocument.user_directory_privileges(root_id, user_id, [:edit])

    # As with the neighboring new_directory_json/4 function, we can not use savepoints as normally supported in Postgres - and therefore, we do not wrap the function in a transaction
    cond do
      elem(perm_res, 0) == :denied -> %{"error" => "Permission denied"}
      elem(perm_res, 0) == :error -> %{"error" => "Unable to check that you are authorized to do this action, please try again later"}
      (root_id != nil && slug == "") -> %{"error" => "You must provide a slug for this new document"}
      # All other possible results are valid
      true -> fn ->
        changeset = Koodeksi.DB.Document.new_document(%{:title => title, :path_slug => slug, :owner_id => user_id, :parent_directory_id => root_id, :anyone_can_access => false, :visible_to_all => true})
          case Repo.insert(changeset) do
          {:ok, _} -> %{"ok" => true} # This is OK
          {:error, changeset} -> %{"error" => Koodeksi.Operations.Common.changeset_errors_to_list(changeset)}
        end
      end.()
    end
  end

  def new_document_json(_, _, _, _) do
    %{"error" => "Incomplete or invalid information supplied"}
  end

  @doc """
    Checks if we can list a given directory; this is a simple helper function which doesn't do much beyond that
"""
  def check_weak_view_for_directory(dir_id, user_id) when (is_number(dir_id)) do
    query_res = Koodeksi.Operations.DirDocument.user_directory_privileges(dir_id, user_id, [:view])
    type = elem(query_res, 0)
    # Since we only have one permission to request, we can simply check for approval
    (type == :allowed || type == :admin_allowed)
  end

  def check_weak_view_for_directory(_, _), do: false # By default, return a falsehood

  def build_initial_schema(doc_id, has_edit_permission) when (is_number(doc_id) and doc_id >= 0) do
      # First, initialize static properties. As we are expected to provide a valid starting structure, we need to establish default values as well
      schema = %{"edit_permission" => has_edit_permission, "provisional_blocks" => []}

      # Retrieve raw block identifiers and their orders
      block_data = Repo.all(from b in Koodeksi.DB.Block, select: %{:id => b.id, :order => b.order}, where: b.document_id == ^doc_id)

      # Map to a list of block identifiers
      block_ids = Enum.map(block_data, fn %{:id => id} -> id end)
      # Construct an initial list of block structures. We do not preload anything yet, just set up stubs so that the clientside has a valid initial state
      block_structures = Enum.reduce(block_data, %{}, fn %{:id => id, :order => order}, acc ->
        acc |> Map.put("block-#{id}", %{"loaded" => false, "errors" => [], "order" => order})
      end)

      schema = Map.put(schema, "doc_id", doc_id)
      schema = Map.put(schema, "blocks", block_ids)
      schema = Map.put(schema, "last_refresh_time", (Repo.one!(from d in Koodeksi.DB.Document, select: d.last_edited, where: d.id == ^doc_id) || Repo.one!(from d in Koodeksi.DB.Document, select: d.created, where: d.id == ^doc_id)) |> DateTime.from_naive!("Etc/UTC") |> DateTime.to_unix())

      # Select appropriate document plugin options
      found_opts = Repo.all(from dop in Koodeksi.DB.DocumentPluginOptions, select: %{:plugin => dop.plugin, :data => dop.data_map}, where: dop.document_id == ^doc_id)
      plugins_with_set_options = Enum.map(found_opts, fn x -> x[:plugin] end) |> Enum.uniq
      plugins_without_set_options = Map.keys(KoodeksiPlugins.PluginGateway.list_of_plugins()) |> Enum.reject(fn x -> Enum.member?(plugins_with_set_options, x) end)

      merged_opt_map = Enum.map(found_opts, fn x -> %{x[:plugin] => x[:data]} end) |> Enum.reduce(%{}, &Map.merge/2)
      full_map = Map.merge(merged_opt_map, KoodeksiPlugins.PluginGateway.empty_dpos(plugins_without_set_options))
      schema = Map.put(schema, "document_plugin_opts", full_map)

      # Return a final schema
      %{"schema" => schema} |> Map.merge(block_structures)
  end

  def build_initial_schema(_, _) do
    nil
  end

  @doc """
    Checks user-specific privileges for a given document

    Due to the reliance on the parent directory's privileges, it may be slightly tricky to determine the actual permissions.
    This function takes care of the smaller details, returning a simple yes/no answer wherever they be permitted to view, edit, or nothing
"""
  def document_privilege_check(doc_preload, doc_id, user_id) do

    doc_data = if (doc_preload != nil), do: doc_preload, else: Repo.get!(Koodeksi.DB.Document, doc_id)

    privileges = Koodeksi.Operations.DirDocument.user_document_privileges(doc_id, user_id, [:weak_view, :strong_view, :edit])
    categorical_approval = (elem(privileges, 0) == :allowed) || (elem(privileges, 0) == :admin_allowed) # If a full approval is returned, we need not to separately check for different permissions

    # Check first that this is not an error; if it is, we cannot reasonably check rest of the permissions
    not_an_error = elem(privileges, 0) != :error
    # Retrieve rejected permissions, if it is not an error, but not a categorical approval
    rejected_permissions = if (not_an_error && !categorical_approval), do: elem(privileges, 1), else: nil # If this is not a categorical approval, but not an error either, it is a rejection. And from a rejection, we can extract rejected privileges
    # Can we view? This is a categorical requirement for the rest of the permissions
    can_view = not_an_error && (categorical_approval || (!Enum.member?(rejected_permissions, :strong_view)) || (!Enum.member?(rejected_permissions, :weak_view) && check_weak_view_for_directory(doc_data.parent_directory_id,user_id)))
    # Can we edit? This is dependent on having view permissions in the first place
    can_edit = not_an_error && (categorical_approval || (can_view && (!Enum.member?(rejected_permissions, :edit))))

    # Return the values

    {not_an_error, can_view, can_edit}
  end

  @doc """
    Generates a document view preload, or alternatively an error message.

    This essentially builds a preliminary schema to bootstrap the document view with. This schema may be later on updated with
    changes taking place, but in a large amount of cases this function may be sufficient as no changes are exerted.

    If the result is a success, the key "to_preload" will contain an initial schema to save; each subkey is to be treated as a single view path

    This function is safe to use without permission checks, as it will take care of them on its own.
"""
  def generate_document_view_preload(doc_id, user_id) when (is_number(doc_id) and doc_id >= 0) do
      # First, check if the document exists
      doc_data = Repo.get(Koodeksi.DB.Document, doc_id)
      if doc_data == nil do
        %{"result" => "notfound"} # Not found, no permissions can be granted
      else
        # Check if we can get permissions; account for both levels of view permissions, and a possible edit permission
        {not_an_error, can_view, can_edit} = document_privilege_check(doc_data, doc_id, user_id)

        # We've now determined the applying permissions
        cond do
          (!not_an_error) -> %{"result" => "error"}
          (!can_view) -> %{"result" => "denied"} # No viewing permission
          true -> %{"result" => "success", "to_preload" => build_initial_schema(doc_id, can_edit)} # Everything else is OK, we can build an initial schema now
        end
      end
  end

  def generate_document_view_preload(_, _) do
    %{"result" => "error"}
  end

  ####################################

  @doc """
    Document gateway; this is the gateway used by the document view root for committing various transactions and operations from the document page.

    Params are expected to contain at least the "type" parameter, which signifies the type of transaction

    Same practices as most other functions are followed:
    {
      result: <success | error>
      overlay: <if a successful result for an overlay transaction>
      to_preload: <in case of a preload-requesting transaction>
    }

"""
  def document_gateway(doc_id, user_id, params) do
    IO.puts("Reached document gateway: #{inspect doc_id}, #{inspect user_id}, #{inspect params}")
    # Check first if there are any special transaction types
    cond do
      doc_id == nil -> %{"result" => "error"}
      !Map.has_key?(params, "type") -> %{"result" => "error"}
      !is_bitstring(params["type"]) -> %{"result" => "error"}
      # Now, key can be checked for special keys. Currently, only the initial preload is such a key
      params["type"] == "build-initial-preload" -> generate_document_view_preload(doc_id, user_id)
      # Anything else is redirected to the transaction processor
      true -> KoodeksiPlugins.PluginGateway.overlay_transaction(doc_id, user_id, params)
    end
  end
end