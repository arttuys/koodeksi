defmodule Koodeksi.Operations.DirDocument do
  alias Koodeksi.Repo
  import Ecto.Query

  @moduledoc """
    This file defines various operations related to directories and documents. These tend to be more complex operations; simpler operations have respective simple modifier methods available via Koodeksi.DB.Document and Koodeksi.DB.Directory
    This module is rather long, but for a reason: many directory and document operations are related to each other, and it is hard to make a strong logical division (yet, at least). For the time being, functionality of both is in the same file
  """

  @doc """
    Resolves a given path. More precisely, it takes a textual path, and returns a discrete database identifier.
    It is required that one provides a root directory; otherwise, it is impossible for the path resolver to start resolving from the correct location as the database has no discrete specified starting point.

    This function also returns slugs encountered en route with titles, in structures like:
    [
      {"title" => title, "slug" => slug, is_directory => <true|false>, command => <string | undefined>},
      ...
    ]
    The tail of the list is always the latest found element, and it is the only one which can be a document.

    This call takes place inside a Ecto transaction

    {:invalid} - When the path does not resolve to any valid location as related to the root point, or there was a syntax error with the path
    {:error} - Something unexpected happened whilst resolving the path. This is not typical, and can be treated as "500 Internal Error"
    {:directory, id, command, slug_elements} - The path resolves to a valid directory and to a valid command
    {:document, doc_id, dir_id, command, slug_elements} - The path resolves to a valid document (prefix to the dollar sign), and to a valid command. The immediate parent directory is also returned, for the purposes of access control.
"""
  def resolve_path(text, root_id) do
    # Check first that the text resolves to a valid path.
    if (!Regex.match?(Koodeksi.Utils.StdRegexes.sane_path_url(), text)) do
      {:invalid} # This is a malformed path, and cannot be accepted.
    else
      res = Repo.transaction(fn ->
        recursive_resolve(String.split(text, "/"), root_id, []) # As our regex guarantees that all parts are nonempty, we can simply do this.
      end)

      case res do
        {:ok, result} -> result
        {:error, err} -> {:error} # As there are no "bang" functions inside, this typically indicates some deeper trouble - and therefore an error
      end
    end
  end

  @doc """
    Generates a JSON preload for the path trampoline. On the clientside, all directory and document paths resolve first to a special path trampoline component. This component's
    only duty is to check for a preload, request it if necessary, and then load in the correct component according to the serverside response.

    This function returns a structure in following format:
    ```
    {
      result: <dir_list | doc_view | notfound | denied | error>
      to_preload: <data> # Only added if necessary
      slugs: <list of slugs and titles, last one being the current node>
    }
    ```
    --
    Explanations for result types:
      "dir_list" => path resolves to a directory listing; preload data is a directory list structure akin to `list_json_directory_index/2`
      "doc_view" => path resolves to a document view; preload data is document schema with adjunct plugin details (TODO)
      "notfound" => path doesn't resolve to anything valid
      "denied" => path resolves to a valid destination, but the user doesn't have the appropriate permissions to view it.
      "error" => the request was received, but something failed without a further explanation
"""
  def generate_trampoline_preload(path, user_id, dir_root_id) when is_bitstring(path) and (is_nil(user_id) or (is_integer(user_id) and user_id >= 0)) and (is_integer(dir_root_id) and dir_root_id >= 0) do
      IO.puts("Generating preload with path '#{inspect path}', user ID #{inspect user_id}, directory ID #{inspect dir_root_id}")
       # First, attempt to resolve the subpath
      path_res = resolve_path(path, dir_root_id)
      case path_res do
        {:error} -> %{"result" => "error"} # Internal error
        {:invalid} -> %{"result" => "notfound"} # Not a valid path or does not resolve to anything.
        {:directory, id, command, slugs} -> trampoline_preload_directory(id, user_id, command, slugs) # This is a directory, redirect to directory-specific handler
        {:document, doc_id, dir_id, command, slugs} -> trampoline_preload_document(doc_id, dir_id, user_id, command, slugs)
        x -> %{"result" => "error"} # Anything else is an internal error.
      end
  end

  def generate_trampoline_preload(path, user_id, dir_root_id)
    do
    IO.puts("Invalid preload request: '#{inspect path}', user ID #{inspect user_id}, directory ID #{inspect dir_root_id}")
    %{"result" => "error"}
  end # If guards above do not allow such call, return an error


  # Handles document view inquiries, and return a schema
  def trampoline_doc_view(doc_id, user_id, slugs) do
    json_res = Koodeksi.Operations.Document.generate_document_view_preload(doc_id, user_id)
    case json_res do
      %{"result" => "success", "to_preload" => schema} -> %{"result" => "doc_view", "to_preload" => schema, "slugs" => slugs}
      rest -> %{"result" => rest["result"]}
    end
  end

  # Handle document-related queries
  def trampoline_preload_document(doc_id, dir_id, user_id, command, slugs) do
    case command do
      "view" -> trampoline_doc_view(doc_id, user_id, slugs) # Document view
      _ -> %{"result" => "notfound"} # This is not a valid command.. not found?
    end
  end

  # Handle a directory listing.
  defp trampoline_dir_list(dir_id, user_id, slugs) do
    json_index = list_json_directory_index(dir_id, user_id)
    case json_index do
      %{"result" => "success", "list" => list} -> %{"result" => "dir_list", "to_preload" => json_index, "slugs" => slugs}
      rest -> %{"result" => rest["result"]}
    end
  end

  # Handle directory-related queries
  defp trampoline_preload_directory(dir_id, user_id, command, slugs) do
    case command do
      "list" -> trampoline_dir_list(dir_id, user_id, slugs) # Directory listing.
      _ -> %{"result" => "notfound"} # This is not a valid command.. not found?
    end
  end

  @doc """
    Generates a JSON directory index for an user ID, or alternatively issues an error message.

    This function, essentially, builds a list of subfolders and subdocuments of a specified directory. Along with each node, a list of privileges
    will be supplied; this allows the UI to show only the options that the user has a right to use. If user has no right to some directory at all, it
    is not listed and is therefore essentially nonexistent for that user. Same applies for invisible nodes; even if the user has a right to view them, this does not necessarily imply a listing

    It is not an error to have a nil user ID; this simply indicates that the user is a guest.

    This call takes place inside a Ecto transaction

    Successfully retrieved result is of following form:
    ```
    {
      "result": "success",
      "title": <title of the directory | "Root" if not defined>
      "this_dir_privileges": <permissions concerning this directory>,
      "id": <id of this directory being listed>
      "admin_override": <true | false> # Simple check to see if this directory has admin-overrided
      "list": [
        {
          "title": <title of the node>
          "type": <directory | document>
          "id": <identifier for the respective node>
          "path_slug": <path slug for this node>
          "invisible: <true | false> # If this node would not be ordinarily listed due to an invisibility flag, but is due to a special relationship. Do note that having an admin-overridden "list" permission also implies a special relationship, but it must be checked separately
          "permissions": [
            ...,
            {
              type: <applicable permission type, see permission check functions in this module>
              admin_override: <true | false> # Indicates if this permission is only available due to administrative privileges
            }
           ]
          "owner_id" => <id of the owner>
          "owner_name" => <name of the owner>
        }
      ]
    }
    ```

    Other possible results include:
    ```
    {"result": "denied"} # User has no privilege to list this directory
    {"result": "notfound"} # This directory was not found
    {"result": "error"} # Unspecified error of some other nature; these would be unspecified directory IDs or nonexistent user IDs. These are hard to invoke by users (and ideally impossible due to validation), and therefore do not really warrant a special error code to the clientside.
    ```
"""
  def list_json_directory_index(dir_id, user_id) when (is_number(dir_id) and dir_id >= 0) do
    res = Repo.transaction(fn ->
      dir_title = Repo.one(from d in Koodeksi.DB.Directory, select: d.title, where: d.id == ^dir_id)
      dir_exists = !is_nil(dir_title) # Observing that every directory has a title, as per defined schema
      user_exists = if (user_id == nil || dir_exists != true), do: false, else: Repo.one(from u in Koodeksi.DB.User, select: count(u.id), where: u.id == ^user_id) == 1
      # Prepare a variable for admin override determination
      access_type = nil
      # Prepare self-privileges as well
      self_privileges = if dir_exists && (user_exists || user_id == nil), do: get_privilege_set(user_directory_privileges(dir_id, user_id, [:list, :view, :edit, :manage]), [:list, :view, :edit, :manage]), else: nil
      cond do
        dir_exists == nil -> %{"result" => "error"}
        dir_exists == false -> %{"result" => "notfound"}
        user_exists == false && user_id != nil -> %{"result" => "error"}
        # Alright, the input seems essentially valid at its surface. Let's check if the user has an actual permission
        (access_type = elem(user_directory_privileges(dir_id, user_id, [:view]), 0)) == :denied -> fn ->
          if (user_exists) do
            Koodeksi.Operations.Common.log_dir_user_action(user_id: user_id, directory_id: dir_id, text: "Denied an user a directory listing due to lack of privileges")
          else
            Koodeksi.Operations.Common.log_dir_user_action(directory_id: dir_id, text: "Denied guest a directory listing due to lack of privileges")
          end
          %{"result" => "denied"}
        end.() # No permission granted.
        # We can now safely generate a directory listing, as it has been determined that the user has a right to receive them
        true -> %{"result" => "success", "list" => generate_dir_listing_structure(dir_id, user_id), "title" => dir_title, "id" => dir_id, "this_dir_privileges" => self_privileges, "admin_override" => access_type == :admin_allowed}
      end
    end)

    case res do
      {:ok, res} -> res
      {:error, _} -> %{"result" => "error"}
    end
  end

  # If guards above do not match, pass an error
  def list_json_directory_index(_, _) do
    %{"result" => "error"}
  end
  # From a raw user_XX_privileges result, transform it into a substructure
  defp get_privilege_set(result, all_possibilities) do
    # Let's check the simple cases first
    case result do
      {:allowed} -> (fn ->
        # This is easy. Transform the all possibility list
        all_possibilities
        |> Enum.map(fn atom ->
          %{"type" => Atom.to_string(atom), "admin_override" => false}
        end)
      end.())
      ({:admin_allowed, missing}) -> (fn ->
        # Also similarly easy. Compose from all possibilities, checking that if it is one of the missing privileges, it will be marked as an admin override
        all_possibilities
        |> Enum.map(fn atom ->
          %{"type" => Atom.to_string(atom), "admin_override" => Enum.member?(missing, atom)}
        end)
      end.())
      ({:denied, missing}) -> (fn ->
        # Filter out privileges we do not have, and return them
       all_possibilities
       |> Enum.reject(fn atom -> Enum.member?(missing, atom) end)
       |> Enum.map(fn atom ->
          %{"type" => Atom.to_string(atom), "admin_override" => false}
        end)
      end.())
      _ -> [] # Any other result should not give any privileges, as in this case we are not certain what it means
    end
  end

  defp generate_dir_listing_structure(dir_id, user_id) do
    # Before calling this function, we've checked that the user in question actually has a right to view the contents of the containing directory.
    # First, pull in a list of subfolders
    dir_list = Repo.stream(from dir in Koodeksi.DB.Directory, where: dir.parent_directory_id == ^dir_id)
    # And then subdocuments
    document_list = Repo.stream(from doc in Koodeksi.DB.Document, where: doc.parent_directory_id == ^dir_id)

    # Let's now define transforming functions; given a single item, it must either return a structure, or nil if this item is not to be included.
    dir_transform = fn item ->
      privilege_data = get_privilege_set(user_directory_privileges(item.id, user_id, [:list, :view, :edit, :manage]), [:list, :view, :edit, :manage]) # Retrieve privilege data

      owner_name = case Repo.one(from u in Koodeksi.DB.User, select: u.username, where: u.id == ^item.owner_id) do
        nil -> "<unknown>"
        val -> val
      end
      # Check if we have the list privilege
      if (!Enum.any?(privilege_data, fn permission -> permission["type"] == "list" end)) do
        nil
      else
        %{"title" => item.title, "id" => item.id, "type" => "directory", "path_slug" => item.path_slug, "invisible" => !item.visible_to_all, "permissions" => privilege_data, "owner_id" => item.owner_id, "owner_name" => owner_name}
      end
    end
    doc_transform = fn item ->
      privilege_data = get_privilege_set(user_document_privileges(item.id, user_id, [:list, :weak_view, :strong_view, :edit, :manage]), [:list, :weak_view, :strong_view, :edit, :manage]) # Retrieve privilege data
      owner_name = case Repo.one(from u in Koodeksi.DB.User, select: u.username, where: u.id == ^item.owner_id) do
        nil -> "<unknown>"
        val -> val
      end
      # Check if we have the list privilege
      if (!Enum.any?(privilege_data, fn permission -> permission["type"] == "list" end)) do
        nil
      else
        %{"title" => item.title, "id" => item.id, "type" => "document", "path_slug" => item.path_slug, "invisible" => !item.visible_to_all, "permissions" => privilege_data, "owner_id" => item.owner_id, "owner_name" => owner_name}
      end
    end

    final_list = (Enum.map(dir_list, dir_transform)) ++ (Enum.map(document_list, doc_transform))
                 |> Enum.reject(fn x -> x == nil end)
                 |> Enum.sort_by(&{&1["title"]})

    final_list # Return the final list
  end

  @doc """
    Checks if user has "an interest" in relation to the specified directory. This practically means one or more of the following:
      - User is the owner of the directory
      - User has ownership or editing rights of some immediate descendant of this directory.

    Returns one of the following
      {:error, reason} - It was unable to be determined if this privilege exists
      {:none} - No interest
      {:dir_owner} - User is a directory owner
      {:content_owner} - User owns content, either a subdirectory or a document
      {:contents_interest} - User has editing permissions to an immediate subdocument
"""
  def user_interest_in_directory(user_id, dir_id) when is_number(dir_id) and dir_id >= 0 do
    if (user_id == nil) do
      {:none} # Nevermind what the directory is, a guest cannot have any interest
    else
      try do
        # TODO: Is there an improved approach? We are more interested in the existence of a suitable subnode, not in the preciser details; although, so far, I have not figured out a more self-explanatory way to check
        dir_data = Repo.one!(from d in Koodeksi.DB.Directory, where: d.id == ^dir_id, preload: [:subdirectories, documents: [:editors]])
        cond do
          dir_data.owner_id == user_id -> {:dir_owner} # User is the owner of this directory
          Enum.any?(dir_data.subdirectories, fn dir -> dir.owner_id == user_id end) || Enum.any?(dir_data.documents, fn doc -> doc.owner_id == user_id end) -> {:content_owner} # User owns a subdirectory or a document.
          Enum.any?(dir_data.documents, fn doc -> Enum.any?(doc.editors, fn editor -> editor.id == user_id end) end) -> {:contents_interest} # User owns a document or is an editor for one
          true -> {:none} # No interest found
        end
      rescue
        Ecto.NoResultsError -> {:error, "No such directory or user was found"}
      end
    end
  end

  def user_interest_in_directory(_, _) do
    {:error, "Invalid parameters"}
  end
  # Recursive resolve; given a list of elements, attempt to resolve them through the given root directory. Elements are assumed to be directories, unless there's only one with a prefix in which case it is a document.
  # For each element discovered by this function, it will be added as a slug element
  defp recursive_resolve([head | tail], dir_id, slug_elements) do
    # Check if the list has length of one
    if (List.first(tail) == nil) do
      # This is the end of the path; try splitting at the dollar sign
      [doc_slug, command] = String.split(head, "$")
      if (String.length(doc_slug) > 0) do
        # Attempt to search for a matching document
        doc_data = Repo.one(from d in Koodeksi.DB.Document, select: {d.id, d.title, d.path_slug}, where: (d.path_slug == ^doc_slug) and (d.parent_directory_id == ^dir_id))
        if doc_data != nil do
          {doc_id, doc_title, doc_slug} = doc_data
          {:document, doc_id, dir_id, command, slug_elements ++ [%{"title" => doc_title, "slug" => doc_slug, "is_directory" => false, "command" => command}]}
        else
          {:invalid}
        end # If this path slug resolves to a valid document, return it; otherwise return invalid
      else
        # No document ID; we've located the directory we want, and we know the command for it.
        {:directory, dir_id, command, slug_elements |> List.update_at(-1, fn struct -> Map.put(struct, "command", command) end)}
      end
    else
      # The first element is a directory. Attempt to locate it, and add it to the slug list.
      query_res = Repo.one(from d in Koodeksi.DB.Directory, select: {d.id, d.title, d.path_slug}, where: d.path_slug == ^head and d.parent_directory_id == ^dir_id)
      if (query_res != nil) do
          {new_dir_id, dir_title, dir_path_slug} = query_res
          recursive_resolve(tail, new_dir_id, slug_elements ++ [%{"title" => dir_title, "slug" => dir_path_slug, "is_directory" => true}])
      else
          {:invalid}
      end # If the directory ID is found, recursively resolve forward; otherwise return an {:invalid}
    end
  end

  @doc """
    Checks if user has a right to do certain operations for a document.
    Caller needs to provide a document id, the applying user ID (or if a guest, nil), and the required privileges. One of the following results may be returned:

    {:error, reason} - if it was not possible to determine a definite answer. Primarily this should occur due to a nonexistent ID
    {:allowed} - if the operations are allowed with no caveats or conditions. This is returned if the user has a fully legitimate claim to their permissions, ignoring that they may be administrators.
    {:admin_allowed, missing_privileges} - if the operations are allowed, but with the caveat that because the user is an administrator, they are exempt from most access control. This should be signaled in the UI in some way, hinting to the admins that their access is solely due to their status, and that careless actions may have undesirable results.
    {:denied, missing_privileges} - if any of the operations are not allowed due to the options; a list of missing rights is returned

    Current approach is as follows; currently outstanding privilege requirements are held in a list. Various checks from simplest to most complex, highest to lowest permission levels are conducted, which may or may not return acceptances for permissions. An acceptance may be for one or more permissions, depending on what is discovered in relation to the user.
    If at any point the list empties, it means that the user's authorization has been satisfied for the requested privileges. If the function runs out of checks to complete, user's admin permissions will be checked as a last resort, which may still render an allowed result (although with a distinct caveat)

    If the user is not an admin and the permission list could not be satisfied, a denied result is returned. In both cases, a list of missing privileges is returned

    Check functions can safely use the exclamation mark versions of Repo function calls, as this top level function has a corresponding rescue clause. This call takes place inside a Ecto transaction.
    --

    Valid permission types are as follows:
      :list => one of the weakest permissions, this signifies that the user is allowed to see the document's existence in the parent folder, but not even necessarily view it. No consideration is pointed towards the fact if the user is or is not in general allowed to list a directory; this permission is only for the scope of a single document
      :weak_view => like :strong_view below: viewing is permitted, but iff the user is allowed to list the parent directory. This implies no strong relationship between the user and the document
      :strong_view => user is allowed to view the document and interact with its readily provisioned features, but may not edit its contents otherwise. This is a stronger version of :weak_view, and implies a right to access notwithstanding to listing of the parent directory (although in usual cases, :strong_view implies the user being allowed to list a directory).
      :edit => user is allowed to alter the document's contents, but nothing else. Will include :view and :list as well.
      :manage => user is allowed to alter anything about the document, up to changing its ownership to another user. This implies other permissions (and in all cases, they are returned along with this permission)
"""
  @doc_check_functions [&Koodeksi.Operations.DirDocument.check_is_doc_owner/3, &Koodeksi.Operations.DirDocument.check_has_editing_privileges/3, &Koodeksi.Operations.DirDocument.check_has_public_access/3]
  def user_document_privileges(doc_id, user_id, required_perms) when is_number(doc_id) and doc_id >= 0 do
    cond do
      doc_id == nil -> {:error, "You must specify a document"}
      Enum.any?(required_perms, fn perm -> !Enum.member?([:list, :weak_view, :strong_view, :edit, :manage], perm) end) -> {:error, "You are asking for permissions that are not valid"}
      true -> try do
          # Wrap this in a transaction; we are making multiple interrelated queries
          res = Repo.transaction(fn ->
            check_node_privileges(required_perms, @doc_check_functions, doc_id, user_id)
          end)

          case res do
            {:ok, val} -> val
            {:error, _} -> {:error, "Internal error whilst checking privileges"}
          end
        rescue
          _ in Ecto.NoResultsError -> {:error, "No such document or user ID was found"}
        end
    end
  end

  def user_document_privileges(_, _, _) do
    {:error, "Invalid parameters"}
  end

  # Case for no outstanding permissions
  defp check_node_privileges([], _, _, _) do
    {:allowed} # No outstanding permissions, approving
  end

  # Case for running out of possibilities to check
  defp check_node_privileges(missing, [], _, user_id) do
    # Whoopsie, we ran out of possible checks, but we still have outstanding permissions.
    # Last chance: if the user is an administrator, we can grant them access with the attached caveat
    if (Koodeksi.Operations.User.is_uid_admin(user_id) == true), do: {:admin_allowed, missing}, else: {:denied, missing}
  end

  # Base case
  defp check_node_privileges(outstanding_permissions, [check_function | check_tail], node_id, user_id) do
    # Check what permissions we acquire, in scope of outstanding permissions
    filled_permissions = check_function.(node_id, user_id, outstanding_permissions)
    # Call recursively, removing granted permissions from our list of outstanding permissions
    check_node_privileges(Enum.reject(outstanding_permissions, fn(val) -> Enum.member?(filled_permissions, val) end), check_tail, node_id, user_id)
  end

  ############################## Checking functions for documents
  # All of these functions take a list of outstanding permissions; this may be useful, as if you already have all permissions that the function can issue

  # Checks if given user is the owner of the given document
  def check_is_doc_owner(doc_id, user_id, outstanding_permissions) do
    cond do
      outstanding_permissions == [] -> [] # We already have all privileges, no need to check
      user_id == nil -> [] # Guests cannot gain owner privileges
      Repo.one!(from d in Koodeksi.DB.Document, select: d.owner_id, where: d.id == ^doc_id) == user_id -> [:list, :weak_view, :strong_view, :edit, :manage] # The owner naturally has all permissions.
      true -> [] # In all other cases, no permissions granted.
    end
  end

  # Checks if the given user has an editing permission to the document, automatically granting list, view, and edit permissions.
  def check_has_editing_privileges(doc_id, user_id, outstanding_permissions) do
    cond do
      (user_id == nil) -> [] # Guests cannot get editing privileges
      !Enum.any?([:list, :weak_view, :strong_view, :edit], fn grantable -> Enum.member?(outstanding_permissions, grantable) end) -> [] # We cannot grant anything required
      Repo.one(from ed in Koodeksi.DB.DocumentEditPerm, where: ed.document_id == ^doc_id and ed.user_id == ^user_id) != nil -> [:list, :weak_view, :strong_view, :edit] # An editing permission exists, grant list, view, and editing permission
      true -> []
    end
  end


  # Determines if this document otherwise viewable or listable by an user. It is assumed that the user is allowed to list the parent directory; the directory listing function should check this separately, before relying on the permissions given
  def check_has_public_access(doc_id, _, outstanding_permissions) do
    cond do
      !Enum.any?([:list, :weak_view], fn grantable -> Enum.member?(outstanding_permissions, grantable) end) -> [] # We cannot grant anything required
      true -> fn ->
                res = Repo.one!(from d in Koodeksi.DB.Document, select: %{:anyone_can_view => d.anyone_can_access, :visible_to_all => d.visible_to_all}, where: d.id == ^doc_id)
                (if res.anyone_can_view, do: [:weak_view], else: []) ++ (if (res.anyone_can_view && res.visible_to_all), do: [:list], else: []) # Grant a view permission if allowed; also, if there is public access and visibility, grant listing
              end.()
    end
  end

  ######################

  @doc """
    Checks if user has a right to do certain operations for a directory. This is quite akin to the `user_document_privileges/3`
    Caller needs to provide a directory id, the applying user ID (or if a guest, nil), and the required privileges. One of the following results may be returned:

    {:error, reason} - if it was not possible to determine a definite answer. Primarily this should occur due to a nonexistent ID
    {:allowed} - if the operations are allowed with no caveats or conditions. This is returned if the user has a fully legitimate claim to their permissions, ignoring that they may be administrators.
    {:admin_allowed, missing_privileges} - if the operations are allowed, but with the caveat that because the user is an administrator, they are exempt from most access control. This should be signaled in the UI in some way, hinting to the admins that their access is solely due to their status, and that careless actions may have undesirable results.
    {:denied, missing_privileges} - if any of the operations are not allowed due to the options; a list of missing rights is returned

    Current approach is as follows; currently outstanding privilege requirements are held in a list. Various checks from simplest to most complex, highest to lowest permission levels are conducted, which may or may not return acceptances for permissions. An acceptance may be for one or more permissions, depending on what is discovered in relation to the user.
    If at any point the list empties, it means that the user's authorization has been satisfied for the requested privileges. If the function runs out of checks to complete, user's admin permissions will be checked as a last resort, which may still render an allowed result (although with a distinct caveat)

    If the user is not an admin and the permission list could not be satisfied, a denied result is returned. In both cases, a list of missing privileges is returned

    Check functions can safely use the exclamation mark versions of Repo function calls, as this top level function has a corresponding rescue clause.

    This call takes place inside a Ecto transaction.

    --
    Valid permissions are as follows:
      :list => User is allowed to see the directory in a parent directory listing. This is not necessarily required for to view.
      :view => User is allowed to list the given directory, but may not commit changes to its structure. This may be granted either by public access, or some interest in the immediate vicinity. Note that this is not required for access to the subfolders. This is required if one wants to access an immediate subdocument to which the user has only a :weak_view permission; :strong_view bypasses this requirement. Note that this is not necessarily related to :list!
      :edit => User is allowed to create new files and subdirectories, and rename plus delete those they own. Is always granted if the user owns anything immediately under the directory.
      :manage => User can rename and delete items by other users, and usurp ownership of those items (effectively a power of eminent domain). They can also change the directory's ownership at will. Only granted to the registered owner of the directory.
  """
  @dir_check_functions [&Koodeksi.Operations.DirDocument.check_user_directory_interest/3, &Koodeksi.Operations.DirDocument.check_directory_public_view/3]
  def user_directory_privileges(dir_id, user_id, required_perms) when is_number(dir_id) and dir_id >= 0 do
    cond do
      dir_id == nil -> {:error, "You must specify a directory"}
      Enum.any?(required_perms, fn perm -> !Enum.member?([:list, :view, :edit, :manage], perm) end) -> {:error, "You are asking for permissions that are not valid"}
      true -> try do
                # Wrap this in a transaction; we are making multiple interrelated queries
                res = Repo.transaction(fn ->
                  check_node_privileges(required_perms, @dir_check_functions, dir_id, user_id)
                end)
                # In this case, we may have a rollback. Check for such an event
                case res do
                  {:ok, result} -> result
                  {:error, message} -> {:error, message}
                end
              rescue
                _ in Ecto.NoResultsError -> {:error, "No such directory or user ID was found"}
              end
    end
  end

  def user_directory_privileges(_, _, _) do
    {:error, "Invalid parameters"}
  end

  ##########################

  # Checks for user's interest in the given directory; this can grant a variety of permissions
  def check_user_directory_interest(dir_id, user_id, _) do
    case (user_interest_in_directory(user_id, dir_id)) do
      {:error, reason} -> Repo.rollback(reason) # Error means rollback
      {:none} -> [] # No interest, no privileges
      {:dir_owner} -> [:list, :view, :edit, :manage] # Directory owner, this grants all privileges
      {:content_owner} -> [:list, :view, :edit] # Content owner, grants editing privileges
      {:contents_interest} -> [:list, :view] # Has an editing permission to the contents of some document, this only grants a permission to list but nothing else
    end
  end

  # Check if this directory has public accessibility; if it is so, it may grant some privileges
  def check_directory_public_view(dir_id, user_id, required_perms) do
    cond do
      !Enum.any?([:list, :view, :edit], fn grantable -> Enum.member?(required_perms, grantable) end) -> [] # We cannot grant anything required
      true -> fn ->
        res = Repo.one!(from d in Koodeksi.DB.Directory, select: %{:anyone_can_access => d.anyone_can_access, :visible_to_all => d.visible_to_all, :anyone_can_edit => d.anyone_can_edit}, where: d.id == ^dir_id)
        edit_perm = if (res.anyone_can_edit && user_id != nil), do: [:edit], else: [] # Tack on an additional edit permission, if other conditions are fulfilled
        (if res.anyone_can_access, do: [:view] ++ edit_perm, else: []) ++ (if (res.anyone_can_access && res.visible_to_all), do: [:list], else: []) # Grant a view+edit permission if applicable; also, if there's public visibility, grant a list permission
      end.()
    end
  end
end