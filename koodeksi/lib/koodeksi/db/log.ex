defmodule Koodeksi.DB.Log do
  @moduledoc """
    This file defines the Log schema, which is used to express audit/logging records for various events
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "logs" do
    # References to relevant records
    belongs_to :block, Koodeksi.DB.Block, foreign_key: :block_id
    belongs_to :submission, Koodeksi.DB.Submission, foreign_key: :submission_id
    belongs_to :document, Koodeksi.DB.Document, foreign_key: :document_id
    belongs_to :directory, Koodeksi.DB.Directory, foreign_key: :directory_id
    belongs_to :user, Koodeksi.DB.User, foreign_key: :initiating_user_id
    # Fields describing the data
    field :event_date, :naive_datetime
    field :log_data, :map
    field :sensitive, :boolean
    field :originating_server, :string
  end

  @doc """
  This changeset extracts the IDs from the params, requires the log data, and assigns the date to be the latest
"""
  def changeset(log_record, params \\ %{}) do
    log_record
    |> cast(params, [:block_id, :submission_id, :document_id, :directory_id, :initiating_user_id, :log_data, :sensitive, :originating_server])
    |> foreign_key_constraint(:block_id)
    |> foreign_key_constraint(:submission_id)
    |> foreign_key_constraint(:document_id)
    |> foreign_key_constraint(:directory_id)
    |> foreign_key_constraint(:initiating_user_id)
    |> validate_required([:log_data, :originating_server])
    |> put_change(:event_date, NaiveDateTime.utc_now())
  end
end