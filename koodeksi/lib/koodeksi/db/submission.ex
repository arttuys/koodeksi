defmodule Koodeksi.DB.Submission do
  @moduledoc """
    This module defines the Submission schema, which corresponds to a single submission as sent for a block.
    Not all blocks accept submissions, and this is entirely dependent on the Elixir backend of the plugin responsible for the block in question
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "submissions" do
    belongs_to :block, Koodeksi.DB.Block, foreign_key: :block_id
    belongs_to :user, Koodeksi.DB.User, foreign_key: :user_id
    field :contents, :binary
    field :score, :integer

    # Declare logs
    has_many :logs, Koodeksi.DB.Log, foreign_key: :submission_id

    timestamps()
  end

  @doc """
    Creates a new submission and returns a changeset that can be used to add it to the database.
"""
  def new_submission(block_id, user_id, contents, score \\ nil) do
    %Koodeksi.DB.Submission{}
    |> cast(%{:block_id => block_id, :user_id => user_id, :contents => contents, :score => score}, [:block_id, :user_id, :contents, :score])
    |> validate_required([:block_id, :contents])
  end

  @doc """
    Alters the contents of a submission. This may be required in some cases, but should ideally be avoided in interest of retaining permanent records
"""
  def alter_contents(submission, contents) do
    submission
    |> cast(%{:contents => contents}, [:contents])
    |> validate_required([:contents])
  end

  @doc """
    Alters the score of a submission. This usually shouldn't be needed to done post-factum, but for specialized cases, it is worthwhile to be available regardless
"""
  def alter_score(submission, score) do
    submission
    |> cast(%{:score => score}, [:score])
  end
end