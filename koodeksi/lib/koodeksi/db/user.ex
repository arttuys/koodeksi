defmodule Koodeksi.DB.User do
@moduledoc """
  This module defines the User schema, which corresponds to user data as stored in the database
"""
  use Ecto.Schema
  import Ecto.Changeset
  alias Koodeksi.Utils.StdRegexes
  schema "users" do
    # Username
    field :username, :string
    # Password, virtual field! Hash is generated automatically upon signup changeset
    field :password, :string, virtual: true
    # Password hash
    field :pwd_hash, :string
    # Email address
    field :email, :string

    # Is inactive account?
    field :inactive, :boolean, default: false
    # Is the user an administrator, and if so, when he was declared to be one?
    field :administrative_user_date, :naive_datetime, default: nil
    # Is this user an admin from the relevant point of time?
    field :admin, :boolean, virtual: true, default: false

    ##
    # Reference to the directories an user may own. Do note that the database migration settings will enforce that users may not be deleted if any directories exist.
    has_many :owned_directories, Koodeksi.DB.Directory, foreign_key: :owner_id
    # Reference to the documents an user may own. As above, the database enforces that users may not be deleted if documents exist
    has_many :owned_documents, Koodeksi.DB.Document, foreign_key: :owner_id
    ##
    # Declare edit permissions
    has_many :edit_permissions, Koodeksi.DB.DocumentEditPerm, foreign_key: :user_id
    has_many :edit_permitted_documents, through: [:edit_permissions, :document]

    # Declare submissions
    has_many :document_submissions, Koodeksi.DB.Submission, foreign_key: :user_id

    # Declare log records
    has_many :initiated_action_logs, Koodeksi.DB.Log, foreign_key: :initiating_user_id

    timestamps()
  end

  @doc """
  Signup changeset; this generates a new user, and therefore does not take a base user. Only takes a limited set of fields from the params.
"""
  def signup(params \\ %{}) do
    %Koodeksi.DB.User{}
    |> cast(params, [:username, :password, :email, :inactive, :admin])
    |> username_internal_changeset() # This changeset enforces proper format for an username
    |> password_internal_changeset() # This changeset changes a normal password into a hash, and verifies its strength
    |> email_internal_changeset() # This enforces proper email format
    |> admin_boolean_date # Set up proper admin permissions date
  end

  @doc """
  Changes the username only
"""

  def change_username(user, username) do
    user
    |> cast(%{:username => username}, [:username])
    |> username_internal_changeset()
  end

  @doc """
  Changes only the password for a given user. It is assumed that the user is authorized to change the password
"""
  def change_password(user, password) do
    user
    |> cast(%{:password => password}, [:password])
    |> password_internal_changeset()
  end

  @doc """
  Changes the email only
  """
  def change_email(user, email) do
    user
    |> cast(%{:email => email}, [:email])
    |> email_internal_changeset()
  end
  @doc """
  Activates or inactivates an user, and sets to today/rescinds admin rights if so requested. Giving a nil to a parameter will cause no action to be taken.
"""
  def set_access(user, active, is_admin) when active == nil and is_admin != nil, do: (cast(user,%{:administrative_user_date => (unless is_admin, do: nil, else: NaiveDateTime.utc_now())}, [:administrative_user_date]))

  def set_access(user, active, is_admin) when is_admin == nil and active != nil, do: user |> cast(%{:inactive => !active}, [:inactive])

  def set_access(user, active, is_admin) when (active != nil and is_admin != nil), do: user |> cast(%{:inactive => !active, :administrative_user_date => (unless is_admin, do: nil, else: NaiveDateTime.utc_now())}, [:inactive, :administrative_user_date])

  def set_access(user, nil, nil) do
    user |> cast(%{}, [])
  end

  defp admin_boolean_date(changeset) do
    if (get_change(changeset, :admin) == true) do
      # If we want this user to be an admin, then declare it as such
      changeset |> put_change(:administrative_user_date, NaiveDateTime.utc_now())
    else
      # Else ensure that the user will not be one
      changeset |> put_change(:administrative_user_date, nil)
    end
  end

  defp check_exclusion_from_reserved(changeset, atom) do
    validate_change(changeset, atom, fn atom, data -> if (Regex.match?(StdRegexes.reserved_names(), data)), do: [{atom, "must be not empty or reserved"}], else: [] end)
  end

  defp username_internal_changeset(changeset) do
    changeset
    |> unique_constraint(:username, message: "is already taken")
    |> validate_required([:username])
    |> validate_length(:username, min: 1, max: 20)
    |> check_exclusion_from_reserved(:username)
    |> validate_format(:username, StdRegexes.sane_slug(), message: "must consist only of: ASCII letters, numbers, _ and -") # Usernames are considered to be equivalent to sane slugs in terms of requirements
  end

  defp password_internal_changeset(changeset) do
    changeset
    |> validate_required([:password])
    |> bypass_empty_pwd
    |> validate_password_strength
    |> hash_password()
  end

  defp email_internal_changeset(changeset) do
    changeset
    |> validate_required([:email])
    |> validate_format(:email, ~r/@/, message: "must be valid") # Enforce that an email address must contain a @, to even resemble a valid email.
  end

  defp bypass_empty_pwd(changeset) do
    # It seems that functions down the line are unable to handle nulls. Let's put an empty password if none is provided, as that should also certainly fail
    if (get_change(changeset, :password) == nil), do: put_change(changeset, :password, ""), else: changeset
  end

  defp validate_password_strength(changeset) do
    pwd = get_change(changeset, :password) # Retrieve the password
    cond do
      String.length(pwd) < 6 -> add_error(changeset, :password, "must be at least 6 characters")
      String.length(pwd) > 255 -> add_error(changeset, :password, "must be no more than 255 characters")
      (String.graphemes(pwd) |> Enum.uniq |> length) < 4 -> add_error(changeset, :password, "must consist of at least 4 distinct characters")
      # TODO - More stringent password tests!
      true -> changeset # All good.
    end
  end

  defp hash_password(changeset) do
    # Generate a hash via the password
    hash = Comeonin.Bcrypt.hashpwsalt(get_change(changeset, :password))
    changeset |> put_change(:pwd_hash, hash)
  end
end