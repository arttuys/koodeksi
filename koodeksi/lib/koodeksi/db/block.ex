defmodule Koodeksi.DB.Block do
  @moduledoc """
    This file defines the Block schema, which defines a single block - a document can contain any amount of blocks in a specific order
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Koodeksi.Utils.StdRegexes

  schema "blocks" do
    # Each block belongs to a single document
    belongs_to :document, Koodeksi.DB.Document, foreign_key: :document_id
    # The order integer; this integer, in combination with the document ID, defines the place of presentation for a block.
    field :order, :integer
    # The plugin identifier, a string
    field :plugin, :string
    # The contents of a block
    field :contents, :binary
    # The latest submission to this block
    field :last_submission, :naive_datetime

    # Declare that a block has several submissions
    has_many :submissions, Koodeksi.DB.Submission, foreign_key: :block_id
    # Declare logs
    has_many :logs, Koodeksi.DB.Log, foreign_key: :block_id

    # Timestamps; when this block had been added, and last updated in some way
    timestamps()
  end

  @doc """
  Declares a new block; caller must provide a document ID, order, and plugin name
"""
  def new_block(params \\ %{}) do
    %Koodeksi.DB.Block{}
    |> cast(params, [:order, :plugin, :contents, :document_id])
    |> doc_order_unique_constraint() # Enforce that order is held
    |> plugin_name_changeset_internal() # Validate plugin slug
    |> validate_required([:order, :document_id]) # Require order and document ID to be given; contents are not required
  end

  @doc """
  Changes the contents of a given block
"""
  def change_contents(block, contents) do
    block
    |> cast(%{:contents => contents}, [:contents])
  end

  @doc """
  Changes the ordered position of a given block. This operation will fail if there exists an another block with the same order integer in the same document.
  """
  def change_order(block, order_int) do
    block
    |> cast(%{:order => order_int}, [:order])
    |> doc_order_unique_constraint
  end
  @doc """
  Declare that there has been a submission to this block
  """
  def submitted_now(document) do
    document
    |> cast(%{:last_submission =>  NaiveDateTime.utc_now()}, [:last_submission])
  end

  defp plugin_name_changeset_internal(changeset) do
    changeset
    |> validate_required([:plugin])
    |> validate_format(:plugin, StdRegexes.sane_slug())
  end

  defp doc_order_unique_constraint(changeset) do
    changeset |>
      unique_constraint(:order, name: :blocks_document_id_order_index, message: "there may not be 2 blocks in a same document with the same order integer")
  end
end