defmodule Koodeksi.DB.DocumentEditPerm do
  @moduledoc """
    This file defines the DocumentEditPerms schema; basically, this table contains one row for a single authorization for some user to edit a document
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "document_edit_perms" do
    belongs_to :document, Koodeksi.DB.Document, foreign_key: :document_id
    belongs_to :user, Koodeksi.DB.User, foreign_key: :user_id
  end
  @doc """
    This changeset adds checks that the document edit permission record is valid, and only casts the prerequisite parameters
"""
  def permission_changeset(permission,params \\ %{}) do
    permission
    |> cast(params, [:document_id, :user_id])
    |> unique_perms()
  end

  defp unique_perms(changeset) do
    changeset |> unique_constraint(:user, name: :document_edit_perms_document_id_user_id_index, message: "one user may not have multiple edit permissions for the same document")
  end
end