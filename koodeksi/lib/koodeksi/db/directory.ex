defmodule Koodeksi.DB.Directory do
  @moduledoc """
  This file defines the Directory schema, which corresponds to a logical directory containing subdirectories and documents.
"""
  use Ecto.Schema
  import Ecto.Changeset
  alias Koodeksi.Utils.StdRegexes

  schema "directories" do
    # Title of a directory
    field :title, :string
    # Path slug of a directory
    field :path_slug, :string
    # Owner of a given directory.
    belongs_to :directory_owner, Koodeksi.DB.User, foreign_key: :owner_id
    # Visibility of a directory
    field :visible_to_all, :boolean
    # Free or restricted editing access to a directory
    field :anyone_can_edit, :boolean
    # Free or restricted viewing access to a directory
    field :anyone_can_access, :boolean

    # Declare the tree structure
    belongs_to :parent, Koodeksi.DB.Directory, foreign_key: :parent_directory_id
    has_many :subdirectories, Koodeksi.DB.Directory, foreign_key: :parent_directory_id

    # Declare that directories have documents
    has_many :documents, Koodeksi.DB.Document, foreign_key: :parent_directory_id

    # Declare logs
    has_many :logs, Koodeksi.DB.Log, foreign_key: :directory_id

    timestamps()
  end

  # Do observe that the following changesets are concerned ONLY about a single record. In particular, path slug conflict avoidance will
  # need to check several tables, and this functionality will be delegated to a separate helper module

  @doc """
    Creates a changeset that will create a new empty directory. You may provide all options there can be, but some of them have migration-level defaults which can be used.

    Be aware that this expects receive the owner user ID and the parent directory ID, NOT the structures themselves.
"""
  def new_directory(params \\ %{}) do
    %Koodeksi.DB.Directory{}
    |> cast(params, [:title, :path_slug, :owner_id, :visible_to_all, :anyone_can_edit, :anyone_can_access, :parent_directory_id])
    |> title_internal_changeset
    |> path_slug_internal_changeset
    |> foreign_key_constraint(:owner_id, message: "Must have an owner")
    |> foreign_key_constraint(:parent_directory_id, message: "Must have a parent directory")
    |> validate_required([:owner_id])
  end

  defp check_exclusion_from_reserved(changeset, atom, allow_null) do
    validate_change(changeset, atom, fn atom, data -> if ((data == nil && !allow_null) || (data != nil && Regex.match?(StdRegexes.reserved_names(), data))), do: [{atom, "must be not empty or reserved"}], else: [] end)
  end

  @doc """
      Changes the title of a given directory
  """
  def change_title(directory, title) do
    directory
    |> cast(%{:title => title}, [:title])
    |> title_internal_changeset()
  end

  @doc """
      Changes the path slug of a directory
"""
  def change_path_slug(directory, path_slug) do
    directory
    |> cast(%{:path_slug => path_slug}, [:path_slug])
    |> path_slug_internal_changeset()
  end

  @doc """
      Changes the owner ID of this specific directory (not any subdirectories or files)
"""
  def change_owner_id(directory, owner_id) do
    directory
    |> cast(%{:owner_id => owner_id}, [:owner_id])
    |> foreign_key_constraint(:owner_id, message: "Must have an owner")
    |> validate_required([:owner_id])
  end

  @doc """
      Changes the parent directory ID. This operation is slightly dangerous, as this will likely have an effect on the URL, and may improperly done lead to a part of your tree becoming inaccessible!
"""
  def change_parent_directory_id(directory, parent_directory_id) do
    directory
    |> cast(%{:parent_directory_id => parent_directory_id}, [:parent_directory_id])
    |> foreign_key_constraint(:parent_directory_id, message: "Must have a parent directory")
  end

  @doc """
      Changes access rights of a directory
"""
  def change_access_rights(directory, params \\ %{}) do
    directory
    |> cast(params, [:visible_to_all, :anyone_can_edit, :anyone_can_access])
  end

  # Validate that a title is nominally valid and nonempty, plus that it is unique
  defp title_internal_changeset(changeset) do
    changeset
    |> validate_required([:title])
    |> validate_format(:title, StdRegexes.sane_title())
    |> check_exclusion_from_reserved(:title, false)
    |> title_dir_unique_constraint()
  end

  defp path_slug_internal_changeset(changeset) do
    changeset
    |> conditional_slug_format()
    |> slug_dir_unique_constraint()
  end

  # Conditionally check the slug format, if it is not null. Null values are intristically valid
  defp conditional_slug_format(changeset) do
    if (get_change(changeset, :path_slug) == nil) do
      changeset
    else
      validate_format(changeset, :path_slug, StdRegexes.sane_slug(), message: "must only contain ASCII characters and basic punctuation") # Path slugs must be a subset of ASCII
    end
    |> check_exclusion_from_reserved(:path_slug, true)
  end

  # Declare the unique constraint required for assuring title/parent directory uniqueness; this constraint has been defined in the migration as an index
  defp title_dir_unique_constraint(changeset) do
    changeset |>
    unique_constraint(:title, name: :directories_parent_directory_id_title_index, message: "there may not be 2 subfolders with the same name in a directory")
  end

  # Declare the unique constraint required for assuring slug/parent directory uniqueness; this constraint has been defined in the migration as an index
  defp slug_dir_unique_constraint(changeset) do
    changeset |>
      unique_constraint(:path_slug, name: :directories_parent_directory_id_path_slug_index, message: "there may not be 2 subfolders with the same path slug in a directory")
  end
end