defmodule Koodeksi.DB.Document do
  @moduledoc """
    This file defines the Document schema; it describes a single discrete document stored in Koodeksi database
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Koodeksi.Utils.StdRegexes

  schema "documents" do
    # Title of a document
    field :title, :string
    # Path slug
    field :path_slug, :string
    # Owner of a given document.
    belongs_to :document_owner, Koodeksi.DB.User, foreign_key: :owner_id
    # Visibility of a document
    field :visible_to_all, :boolean
    # Free or restricted viewing access to a directory
    field :anyone_can_access, :boolean

    # Declare edit permissions
    has_many :edit_permissions, Koodeksi.DB.DocumentEditPerm, foreign_key: :document_id
    has_many :editors, through: [:edit_permissions, :user]

    # Declare plugin options
    has_many :plugin_options, Koodeksi.DB.DocumentPluginOptions, foreign_key: :document_id

    # Declare blocks
    has_many :blocks, Koodeksi.DB.Block, foreign_key: :document_id

    # Declare that a document has a parent, a parent directory
    belongs_to :parent, Koodeksi.DB.Directory, foreign_key: :parent_directory_id

    # When this document has been created
    field :created, :naive_datetime
    # When this document or its parts has been last edited
    field :last_edited, :naive_datetime
    # Declare logs
    has_many :logs, Koodeksi.DB.Log, foreign_key: :document_id
  end

  @doc """
      Create a new document. You can not give this function a base document, as this is intended to create a completely new one.

      In particular, the timestamps on this document behave slightly differently compared to standard timestamps; creation is as expected, but modifications are counted also from block modifications, not solely document property modifications.
"""
  def new_document(params \\ %{}) do
    %Koodeksi.DB.Document{}
    |> cast(params, [:title, :path_slug, :owner_id, :parent_directory_id, :visible_to_all, :anyone_can_access])
    |> title_internal_changeset()
    |> path_slug_internal_changeset()
    |> validate_required(:owner_id) # Validate
    |> validate_required(:parent_directory_id)
    |> foreign_key_constraint(:owner_id, message: "Must have an owner")
    |> foreign_key_constraint(:parent_directory_id, message: "Must have a parent directory")
    |> put_change(:created, NaiveDateTime.utc_now())
  end

  defp check_exclusion_from_reserved(changeset, atom, allow_null) do
    validate_change(changeset, atom, fn atom, data -> if ((data == nil && !allow_null) || (data != nil && Regex.match?(StdRegexes.reserved_names(), data))), do: [{atom, "must be not empty or reserved"}], else: [] end)
  end

  @doc """
      Changes the title of a given document
  """
  def change_title(document, title) do
    document
    |> cast(%{:title => title}, [:title])
    |> title_internal_changeset()
  end

  @doc """
        Changes the path slug of a document
  """
  def change_path_slug(document, path_slug) do
    document
    |> cast(%{:path_slug => path_slug}, [:path_slug])
    |> path_slug_internal_changeset()
  end

  @doc """
        Changes the owner ID of this specific document
  """
  def change_owner_id(document, owner_id) do
    document
    |> cast(%{:owner_id => owner_id}, [:owner_id])
    |> validate_required([:owner_id])
    |> foreign_key_constraint(:owner_id, message: "Must have an owner")
  end

  @doc """
        Changes the parent directory ID. Be careful with this operation, as this likely alters URLs, and has effects on access privileges as well.
  """
  def change_parent_directory_id(document, parent_directory_id) do
    document
    |> cast(%{:parent_directory_id => parent_directory_id}, [:parent_directory_id])
    |> foreign_key_constraint(:parent_directory_id, message: "Must have a parent directory")
  end

  @doc """
        Changes access rights of a documents
  """
  def change_access_rights(document, params \\ %{}) do
    document
    |> cast(params, [:visible_to_all, :anyone_can_access])
  end

  @doc """
        Declare that the document has been edited at the date and time of this function's calling.
"""
  def edited_now(document) do
    document
    |> cast(%{:last_edited =>  NaiveDateTime.utc_now()}, [:last_edited])
  end
  # Validate that a title is nominally valid and nonempty, plus that it is unique. Same rules apply to a document title as to a directory title
  defp title_internal_changeset(changeset) do
    changeset
    |> validate_required([:title])
    |> validate_format(:title, StdRegexes.sane_title())
    |> check_exclusion_from_reserved(:title, false)
    |> title_doc_unique_constraint()
  end

  # Validate that a path slug is valid. Same rule as in a directory path slug
  defp path_slug_internal_changeset(changeset) do
    changeset
    |> validate_required([:path_slug])
    |> validate_format(:path_slug, StdRegexes.sane_slug())
    |> check_exclusion_from_reserved(:path_slug, false)
    |> slug_doc_unique_constraint()
  end

 defp title_doc_unique_constraint(changeset) do
    changeset |>
      unique_constraint(:title, name: :documents_parent_directory_id_title_index, message: "there may not be 2 documents with the same name in a directory")
  end

 defp slug_doc_unique_constraint(changeset) do
    changeset |>
      unique_constraint(:path_slug, name: :documents_parent_directory_id_path_slug_index, message: "there may not be 2 documents with the same path slug in a directory")
  end

end