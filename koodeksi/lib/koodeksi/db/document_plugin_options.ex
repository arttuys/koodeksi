defmodule Koodeksi.DB.DocumentPluginOptions do
  @moduledoc """
    This file defines the DocumentPluginOptions schema, which corresponds to plugin specific settings on each document.
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Koodeksi.Utils.StdRegexes

  schema "document_plugin_options" do
    # Document identifier
    belongs_to :document, Koodeksi.DB.Document, foreign_key: :document_id
    # Name of the plugin
    field :plugin, :string
    # The map containing JSONable data
    field :data_map, :map
  end

  @doc """
    Changeset for effectively generating an entirely new plugin record, or making drastic changes to pre-existing records. This will enforce constraint checks as changeset checks, and forces proper plugin names.
    However, one should beware that the data map, in addition of being non-NULL, is required to be JSONable. Non-JSON structures may behave unpredictably, as the JSONifier library (Poison, by default) transforms the structure into a valid JSONable structure
"""
  def changeset(pluginopts, params \\ %{}) do
    pluginopts
    |> cast(params, [:plugin, :document_id, :data_map])
    |> validate_required([:data_map, :document_id])
    |> document_plugin_uniq_constraint
    |> plugin_name_changeset_internal
  end

  defp document_plugin_uniq_constraint(changeset) do
    changeset
    |> unique_constraint(:document_id, name: :document_plugin_options_document_id_plugin_index, message: "a plugin may not have multiple options in the same document")
  end

  defp plugin_name_changeset_internal(changeset) do
    changeset
    |> validate_required([:plugin])
    |> validate_format(:plugin, StdRegexes.sane_slug())
  end

  @doc """
    A simple helper routine, this does nothing beyond changing the contents of a data map. As for the time being, no validations exist beyond it being not null

    Beware: Poison (the JSONifier) library will automatically transform atoms into string keys. This will happen with no warning or exception whatsoever
"""
  def change_content(pluginopts, map) do
    pluginopts
    |> cast(%{:data_map => map}, [:data_map])
    |> validate_required([:data_map])
  end
end