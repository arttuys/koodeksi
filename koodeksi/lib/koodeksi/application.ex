defmodule Koodeksi.Application do
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(Koodeksi.Repo, []),
      # Start the Koodeksi plugin gateway,
      worker(KoodeksiPlugins.PluginGateway, []),
      # Start the endpoint when the application starts
      supervisor(KoodeksiWeb.Endpoint, []),
      # Start your own worker by calling: Koodeksi.Worker.start_link(arg1, arg2, arg3)
      # worker(Koodeksi.Worker, [arg1, arg2, arg3]),
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Koodeksi.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    KoodeksiWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
