# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :koodeksi,
  ecto_repos: [Koodeksi.Repo],
  server_name: "koodeksi_main", # Server's name
  root_dir_id: 1, # Identifier of the root directory.
  static_ip_func: fn ip -> true end # Static IP approval function; if this returns false for any IP address, that IP is refused access to Koodeksi and instead redirected to an error page

# Define the plugins the serverside should support and recognize
config :koodeksi, KoodeksiPlugins.PluginGateway,
  plugin_definitions: %{"html" => KoodeksiPlugins.Plugins.HTML}

# Configures the endpoint
config :koodeksi, KoodeksiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FKgMKSrmMbcIdjeI7kU5D4lqQUDA2RVSGTuH1jWiU3RFDZc7cZSJlSjWvVUaXShP",
  render_errors: [view: KoodeksiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Koodeksi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
