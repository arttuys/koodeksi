# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Koodeksi.Repo.insert!(%Koodeksi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.



{was_ok, struct_or_err} = Koodeksi.Operations.User.signup_full(%{:username => "admin", :password => "koodeksi1234", :email => "invalid@test.test", :admin => true, :inactive => false})

case was_ok do
  :ok-> fn -> IO.puts "A new administrative user with username 'admin' and password 'koodeksi1234' has been established." end
  err -> fn -> IO.puts "Something odd happened: #{inspect struct_or_err}" end
end

# Next, attempt to make a directory

if (was_ok == :ok) do
  base_dir = Koodeksi.DB.Directory.new_directory(%{:title => "Root", :owner_id => struct_or_err.id, :anyone_can_edit => true, :anyone_can_access => false, :visible_to_all => true}) |> Ecto.Changeset.put_change(:id, 1)
  Koodeksi.Repo.insert!(base_dir)
else
  IO.puts "Skipped directory creation due to a problem"
end