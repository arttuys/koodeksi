defmodule Koodeksi.Repo.Migrations.GenInitialDatabase do
  @moduledoc """
    This migration sets up the initial form of Koodeksi database, as defined on 26.11.2017.

    This includes: documents, blocks, submissions, directories, and users.
  """
  use Ecto.Migration

  def change do
    # One must observe that tables, unless otherwise specified, will have an implict :id column.
    # Create user tables
    create table(:users) do
      add :username, :string, null: false # User must have an username of some kind
      add :pwd_hash, :string, null: false # User must have a password hash. This is assumed to be a combined hash of some kind, including a salt
      add :email, :string, null: false # All users must have an email address

      add :inactive, :boolean, null: false, default: false # An inactive account cannot be used to execute any kinds of submission or editing operations, and may not be logged on. This is a flag switched by administrators

      # There are administrative users; they are (at their choice) exempt from any visibility, viewing or editing restrictions. They may also alter other users nearly at will; only exception is administrators added before them. Crucially, an administrator may, at will, revoke their own privileges.
      add :administrative_user_date, :naive_datetime, null: true, default: nil # If an user has an administrative user date, they are an administrator. An admin may add new admins, and revoke rights of admins _that were added after them_.

      timestamps()
    end

    # Enforce that usernames are entirely unique
    create unique_index(:users, [:username])
    # Allow users to be easily searched via email addresses
    create index(:users, [:email])

    # Create a table for directories; a directory may contain other directories and documents
    create table(:directories) do
      add :title, :string, null: false # Each directory has a human-readable title
      add :path_slug, :string, null: true # Each directory has a path slug, which denotes its part in a tree structure. It is allowed to be nil, but if it is nil in a non-root folder, it will lead to undefined behavior (probably rendering the directory and any all subdirectories/documents entirely unreachable by URLs). Directories and documents may have the same slug, but documents are always prefixed with a character (e.g '!'/exclamation mark) to indicate that they are documents
      add :owner_id, references(:users, on_delete: :restrict, on_update: :update_all), null: false # Each directory is owned by an user. Do not allow deletion if folders exist, as this may have unwanted consequences and knock-on effects on other users. Require that a) directories are manually deleted, or b) ownership interests are transferred to some other user.
      add :visible_to_all, :boolean, null: false, default: true # A folder may be invisible, which means it will not be shown in indexes at all unless you are its owner. You may get a directory listing if you know it is there and you have sufficient permissions.

      # There are two possible privilege levels for folder modifications for users. Guests are never allowed to edit or own directories.
      # 1) Anyone can edit: anyone can add new subfolders and files, alter them, and delete their own. They may not tamper with files or subdirectories owned by other users.
      # 2) Restricted: only the owner may create new files or subfolders - but the owner may transfer their ownership, effectively granting access to them. This access is at will - the owner of a directory has free reign to alter ownership of anything under their directory, allowing moderation of contents within. This has effect only on the immediate directory - subdirectories are regulated by their own settings.
      add :anyone_can_edit, :boolean, null: false, default: false
      # More stringent restriction than above, there are two privilege levels for access
      # 1) Anyone can access: if the URL is known, you can list the contents of this directory. It does not matter if the directories above are not listable, as long as this folder is viewable.
      # 2) Restricted: the viewer must have some sort of an ownership/editing privilege interest immediately under this directory, or must be the owner of the directory. This can lead to scenarios where you are not allowed to view the parent directories of a directory you own - but this is considered an acceptable tradeoff for ensuring that DB queries are limited to a reasonable number. Also note that even restricted directories will be listed, unless they are invisible.
      add :anyone_can_access, :boolean, null: false, default: true

      # Optionally, a directory may have a parent directory. If the parent directory is deleted, delete this directory as well.
      add :parent_directory_id, references(:directories, on_delete: :delete_all, on_update: :update_all), null: true

      # Add timestamps
      timestamps()
    end

    create index(:directories, [:owner_id]) # Directories should be easy to collate according to their owner
    create index(:directories, [:parent_directory_id], where: "parent_directory_id IS NOT NULL") # Index by a parent directory. Index only those with a parent directory, as logical.
    create unique_index(:directories, [:parent_directory_id, :path_slug]) # If we know the parent directory of some directory and its path slug, it should be possible to locate an unique result if one exists.
    create unique_index(:directories, [:parent_directory_id, :title]) # Enforce that for a given parent directory, the title of an immediate directory subnode should be unique

    # Create document tables. This only holds document metadata; other tables store the actual contents
    create table(:documents) do
      add :title, :string, null: false # Each document has a title
      add :path_slug, :string, null: false # Each document has a path slug, which will be used in URLs. This is only a part of the URL - the whole path is composed from several path slugs, leading down a tree of database entries. It will be ensured that document URLs have a specific prefix in their final part, as to unambiguously separate them from directory URLs. Therefore, a subdirectory and a document can have the same slug.
      add :parent_directory_id, references(:directories, on_delete: :restrict, on_update: :update_all), null: false # Each document has an unique parent directory. Enforce that a simple deletion of a directory should not be possible, if it would cause a document to be deleted. This may cause apparently hard to explain prohibitions on deletion: as a deletion of a parent directory implies deletion of subdirectories, the original event may be blocked by an extant document. If a directory changes its ID, follow that
      add :owner_id, references(:users, on_delete: :restrict, on_update: :update_all), null: false # A document owner is a user. Do not allow users to be erased if they own any documents, and if an user ID of an owner changes, so does the document's ID as well. Guests can not own documents, as they do not have an ID.
      add :visible_to_all, :boolean, null: false, default: true # A document may be invisible, in which case it is not listed in indexes (unless you are its owner or you have an editing privilege) but is accessible if the correct path is known
      add :anyone_can_access, :boolean, null: false, default: true # A publicly readable document may be accessed by any user who can list the parent folder (this allows primitive group access control); a nonpublic one requires you to have edit access, and is not shown on indexes if you do not have prerequisite privileges.

      add :created, :naive_datetime, null: false # A document has been certainly created sometime
      add :last_edited, :naive_datetime, null: true, default: nil # A document may never have had edits, or blocks modified. This does not account for submissions; they are accomdated by blocks themselves
    end

    create index(:documents, [:owner_id]) # Documents should be easy to collate according to their owner
    create unique_index :documents, [:parent_directory_id, :title] # Enforce that for a given parent directory, a title should be unique.
    create unique_index :documents, [:parent_directory_id, :path_slug] # Enforce that for a given parent directory, a path slug should be unique.
    # An document may have multiple users with editing permissions, along with the owner. This structure pairs that; if a certain pair exists, then the specified user has a right to edit a specified document, without being its owner.
    create table(:document_edit_perms) do
      add :document_id, references(:documents, on_delete: :delete_all, on_update: :update_all), null: false
      add :user_id, references(:users, on_delete: :delete_all, on_update: :update_all), null: false
    end

    create unique_index :document_edit_perms, [:document_id, :user_id] # Enforce that a document-user editing permission pair is unique
    create index(:document_edit_perms, [:document_id]) # Index all permissions for a document
    create index(:document_edit_perms, [:user_id]) # Index all permissions held by an user.

    # A plugin may need to store document-specific options. This table is intended for that, and stores a map containing data
    create table(:document_plugin_options) do
      add :document_id, references(:documents, on_delete: :delete_all, on_update: :update_all), null: false
      add :plugin, :string, null: false
      add :data_map, :map, null: false
    end

    create index :document_plugin_options, [:document_id] # All options must be indexable by document
    create unique_index :document_plugin_options, [:document_id, :plugin] # Require that there is only one record per document and plugin

    # Create a table for blocks; blocks being the constituent elements of documents
    create table(:blocks) do
      add :document_id, references(:documents, on_delete: :delete_all, on_update: :update_all), null: false # A block is bound to a document, and is deleted when a document is deleted
      add :order, :integer, null: false # Each block contains an order integer; this delineates the order they are to be presented in.
      add :plugin, :string, null: false # Each block has a defined plugin, which is responsible for providing display and editing capability for a given block. Care should be taken when editing this, as it may cause problems due to now-unusable submissions.
      add :contents, :binary, null: true # A block may contain data - but not necessarily. If it does not contain that, it will not be rendered on screen. This is useful for assuring that a slot will be guaranteed for a given block

      add :last_submission, :naive_datetime, null: true, default: nil # A block will keep track when the latest submission was posted - and it is possible there are not any
      timestamps()
    end

    create unique_index :blocks, [:document_id, :order] # Enforce consistent order by requiring there be a unique match when you look for a document ID with a given order integer
    create index :blocks, [:document_id] # Blocks must be indexable by document ID

    # Create a table for submissions; a submission is an user-sent data unit, bound to a block. It is up to the plugin if they are allowed at all, or modifiable in any way. A plugin will inquire about submissions existing for a given user ID or a block.
    # Crucially, guests may be possibly allowed to make submissions, but this depends on the plugin and its settings.
    create table(:submissions) do
      add :block_id, references(:blocks, on_delete: :delete_all, on_update: :update_all), null: false # All submissions must be bound to a block - and if the block is permanently deleted, so are the submissions.
      add :user_id, references(:users, on_delete: :delete_all, on_update: :update_all), null: true # All submissions have a single, definite owner - unless they are a guest, in which case the field wil be NULL. If this owner disappears, so will a submission, unless it is manually changed to guest ownership.
      add :contents, :binary, null: false # A submission must contain some arbitrary contents.
      add :score, :integer, null: true, default: nil # A submission also may have a score. User scores are calculated with these values.
      timestamps()
    end

    # Create a log table; this table will store all kinds of log data, which can be then later on viewed via an audit log.
    create table(:logs) do
      # All foreign keys are only nilified when the original block is deleted; this ensures that log records do not disappear when, say, a block or a submission gets erased.
      add :block_id, references(:blocks, on_delete: :nilify_all, on_update: :update_all), null: true, default: nil # Anyone who owns the document the block is attached to, may view this log record
      add :submission_id, references(:submissions, on_delete: :nilify_all, on_update: :update_all), null: true, default: nil # Anyone who owns the block a submission refers to, may view it
      add :document_id, references(:documents, on_delete: :nilify_all, on_update: :update_all), null: true, default: nil # Same for documents
      add :directory_id, references(:directories, on_delete: :nilify_all, on_update: :update_all), null: true, default: nil # As of directories, directory's owner has a right to view all events that take place under a directory they own
      add :initiating_user_id, references(:users, on_delete: :nilify_all, on_update: :update_all), null: true, default: nil # User-related log records are only viewable by the user themself and admins
      # If some log record no longer has any accessible foreign keys (unlikely), they may only be viewed by admins
      add :event_date, :naive_datetime, null: false # Store the date when this occurred
      add :log_data, :map, null: false # Crucially, this should store copies of the fields referenced in the foreign keys, as a historial record if the referenced items are later deleted, changed..
      # A log record may be sensitive; in this case, only administrators may see it.
      add :sensitive, :boolean, null: false, default: false
      # A log record always has an originating server. This will be used to ensure that different servers show only the records related to them, even if several of them share databases (and by extension, an userbase).
      add :originating_server, :string, null: false
    end

    create index(:logs, :event_date) # Create an index for event dates; if nothing else is indexable, then look up using a date
    Enum.each([:block_id, :submission_id, :document_id, :directory_id, :initiating_user_id, :originating_server], fn(x) -> (create index(:logs, [x], where: "#{x} IS NOT NULL")) end) # Create an index for each foreign key. These indexes are partial, as there'd be no purpose indexing by NULL values
  end
end
