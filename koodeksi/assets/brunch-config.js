exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {

      joinTo: {
          "js/koodeksi-app.js": /^js/,
          "js/external.js": /^(?!js)/
      },
      order: {
        after: ["js/load-components.js"]
      }
       
      //
      // To change the order of concatenation of files, explicitly mention here
      // order: {
      //   before: [
      //     "vendor/js/jquery-2.1.1.js",
      //     "vendor/js/bootstrap.min.js"
      //   ]
      // }
    },
    stylesheets: {
      joinTo: "css/app.css",
      order: {
        before: [
            "css/normalize.css" // Enforce that normalization is always done before other CSS merges
        ]
      }
    },
    templates: {
      joinTo: "js/koodeksi-templates.js"
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to "/assets/static". Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(static)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: ["static", "css", "js", "legacy-vendor", "vue-components"],
    // Where to compile files to
    public: "../priv/static"
  },

  // Configure your plugins
  plugins: {
    babel: {
      // Do not use ES6 compiler in vendor code
    },
    vue: {
      extractCSS: true,
      out: '../priv/static/css/vue_components.css'
    },
    sass: {
      options: {
        includePaths: ["node_modules/font-awesome/scss"] // Pull in SCSS required by Font-Awesome, the awesome collection of icons!
      }
    },
    copycat: {
      "fonts": [
          "node_modules/font-awesome/fonts"
      ]
    }
  },

  modules: {
    autoRequire: {
    }
  },

  npm: {
    enabled: true,
      aliases: {
        'vue': "vue/dist/vue.common.js"
      },
      globals: {
        Vue: 'vue',
        Vuex: 'vuex',
        VueRouter: 'vue-router',
        $: 'jquery',
        jQuery: 'jquery'
      }
  }
};
