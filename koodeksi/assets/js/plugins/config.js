/**

 This file contains the configuration parameters for the document plugins available.
 Essentially, one should declare the names of plugin files available; the plugins themselves are expected to lie under the "vue-components/structure/plugins/" path

 **/

var plugins_available = {"HTML block": "html"};

export {plugins_available};