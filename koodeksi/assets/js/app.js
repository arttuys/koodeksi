
// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

//import socket from "./socket"

import * as ActionTypes from './document_state/action_types'

/////////////

/**
 * Bootstraps the user interface of the page by initializing the Vue object. It can be assumed that all operations as provided in the 'init-infra' module have been completed.
 * These preliminary operations include, for instance, setting up Vuex and vue-router, and preparing associated internal data structures.
 *
 * This will also, if available, store the page-specific preload data in the state structure
 *
 * @param init_data Initialization data as provided by `infrastructure.js`; this includes the Vuex and vue-router components
 */
function start(init_data) {
    // Set a timer to update the time periodically
    window.setInterval(function() {init_data.documentState.dispatch(ActionTypes.UPDATE_TIME)}, 1000);

    // Initialize a structure to access these
    window.koodeksi = {
        vuex: init_data.documentState,
        vuerouter: init_data.documentRouter
    };

    let vue_root = new Vue({
        el: '#app',
        template: '<kd-root></kd-root>',
        data: {
        },
        store: init_data.documentState,
        router: init_data.documentRouter
    });


    //init_data.documentState.commit(MutationTypes.PUSH_TERMINAL_ROW, {id: "test_term", line: "single", important: true});
    //init_data.documentState.commit(MutationTypes.PUSH_TERMINAL_ROW, {id: "test_term", line: "double", important: false});
}

export {start};

