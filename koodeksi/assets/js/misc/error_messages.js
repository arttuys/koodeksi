let errors = {
    notfound: {
        title: "404 Not Found",
        icon_class: "fa-question-circle",
        blurb: ["You apparently tried to access a resource that doesn't exist, or is not accessible via this site.",
                "This may be due to a variety of reasons, varying from a faulty link to a mistyped address. You may also try to contact the maintainer for the page or the link you tried to follow."]
    },
    unauthorized: {
        title: "403 Forbidden",
        icon_class: "fa-lock",
        blurb: ["You attempted to access a resource which you are not permitted to access. This action has been logged.",
                "If you believe this is a mistake and you should have access, you should contain the maintainer of the said resource for redress."]
    },
    internalerr: {
        title: "500 Internal Server Error",
        icon_class: "fa-exclamation-triangle",
        blurb: ["For some unknown reason, an internal error has occurred, and your request cannot be therefore fulfilled",
                "This may be caused by a transient fault of some kind, so try again after a brief while. If this problem persists, please contact the site's administrator to ensure that the problem will be rectified in due course."]
    },
    service_unavailable: {
        title: "503 Service Unavailable",
        icon_class: "fa-ban",
        blurb: ["This Koodeksi site is currently experiencing an unusually heavy load, or for some other reason refuses to serve your request right now.", "This is typically a transient condition; if this error message persists, please contact the site's administrator."]
    },
    network_ban: {
        title: "403 Forbidden",
        icon_class: "fa-ban",
        blurb: ["This site's administrator has banned this IP address or network from accessing this Koodeksi service. This may either be a transient or a permanent condition.", "Most likely, you should be aware of the reasons leading up to this situation. If you feel like this is a mistake, please contact the site's administrator for redress."],
        hide_top_menu: true // This is a special case; even though the client can display most pages without relying much on the server, it would result in a situation where no action actually sticks. Therefore, it is imperative that all navigation is disabled as to avoid giving false hope.
    }
};

export {errors}