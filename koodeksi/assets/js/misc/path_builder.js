/**
 * Composes a /content URL from the given path slugs.
 *
 * Do observe that input is NOT validated - callers of this function are expected to only rely on safe information, e.g that sourced directly from the database, which includes validations
 *
 * @param slugs Intermediate slugs to the path; this is empty if the final destination is the root directory
 * @param final_command Command to be presented
 * @param command_in_document If this is true, treat the last slug as a document reference instead of a new directory. It is an error to set this to true with no intermediate slugs
 */
function slugs_to_url(slugs, final_command, command_in_document) {
    let base = "/content";

    if (slugs.length < 1 && command_in_document === true) {
        throw "Cannot have a document command without any path slugs";
    }

    // For the intermediate slugs except the last, apply them to the URL
    for (let i = 0; i < slugs.length - 1; i++) {
        base += ("/" + slugs[i]);
    }

    // Apply the end of the URL
    base += "/" + slugs[slugs.length-1] + (command_in_document ? "$" : "/$") + final_command;
    // Return the base URL
    return base;
}

export {slugs_to_url};