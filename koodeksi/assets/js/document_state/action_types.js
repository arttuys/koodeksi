/*
 * This file contains the simple declarations for various action types. Their behavior is defined elsewhere
 */
export const ENTER_ROW_TO_TERMINAL = 'ENTER_ROW_TO_TERMINAL';
export const UPDATE_TIME = 'UPDATE_TIME';

export const UPDATE_USER_SESSION = 'UPDATE_USER_SESSION';
export const APPLY_OVERLAY = 'APPLY_OVERLAY';