import * as MutationTypes from "./mutation_types"

/**
 * There are scenarios where one may need to update the user data. This most likely happens in connection to logins and logouts, but may also happen
 * due to unexpected server-side revocations or additions of privileges.
 *
 * This action executes an asynchronous call to the API which returns the up-to-date user blob. No separate tokens are required; the privileges available are indicated by a session cookie, which is inaccessible from JS.
 * @param commit Commit function to effect mutations
 */
export function updateUserAction({commit}) {
    function failedLoad() {
        // Regardless of the cause of error, a reload in case of a failure is in order, as wrongly applied or broken user credentials could cause even more unexpected behavior.
        location.reload(true);
    }

    function dataReceived(data) {
        commit(MutationTypes.UPDATE_USER_DATA, data["user"]);
    }

    // Execute an asynchronous call to the API, and on success, update the user data
    return $.ajax({url: "/json/auth/user_blob",
            dataType: "json",
            cache: false,
            type: "get",
            success: dataReceived,
            error: failedLoad
    });
}