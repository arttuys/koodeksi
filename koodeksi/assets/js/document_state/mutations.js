/*
 * This file contains definitions for mutations; mutations are synchronous operations that alter the state.
 * These are called by actions; these should preferably not be called directly, as then one would need to guarantee synchronization
 * to changes done
 */

/**
 *
 * Verifies that a terminal exists, and if necessary, creates it
 *
 * @param state Vuex state object which we alter
 * @param id Identifier for the terminal (must be a string)
 */
function verifyTerminalExists(state, id) {
    let terms = state.terminals;
    if (typeof terms[id] === "undefined") {
        Vue.set(terms, id, []); // Set it so that Vue can detect it too.
    }
}

/**
 *
 * Pushes a new row to the terminal data store; this effectively results in a new row of text appearing to the selected terminal
 *
 * @param state Vuex state object to manipulate
 * @param id Identifier for the terminal
 * @param line Line to add
 * @param important True/false to denote if this row is 'important'; effectively, it will be displayed in bold.
 */
export function pushTerminalRow(state, {id, line, important}) {
    if (typeof id !== "string" || id.length < 1) throw "Identifier must be a string and nonempty";
    if (typeof line !== "string") throw "Line sent must be a string";
    if (typeof important !== "boolean") throw "Importance must be expressed as a boolean";
    console.debug(`Terminal push - ${id}: ${line}`);
    verifyTerminalExists(state, id); // Ensure that our terminal exists

    var term_array = state.terminals[id];
    term_array.push({text: line, important: important});
}

/**
 * Pushes a new breadcrumb to the array stored in the Vuex state object. It will appear in the top navigation bar
 *
 * @param state Vuex state object
 * @param name Name of the breadcrumb (e.g "Sign in")
 * @param addr Relative address for the breadcrumb (must be valid for a router link)
 */
export function pushBreadcrumb(state, {name, addr}) {
    if (typeof name !== "string" || name.length < 1) throw "Name must be a string and nonempty";
    if (!(typeof addr === "string" && addr.length >= 1) && addr != null) throw "Address must either be a string, or null";
    console.debug(`Pushing breadcrumb: ${name}, for address '${addr}'`);

    var breadcrumb_array = state.page_properties.breadcrumbs;
    breadcrumb_array.push({name: name, addr: addr});
}

/**
 * Pops a breadcrumb; used when, for instance, going up one level in a directory tree.
 * @param state Vuex state object
 */
export function popBreadcrumb(state) {
    // Pop only if there are items
    if (state.page_properties.breadcrumbs.length > 0) state.breadcrumbs.pop();
}

/**
 * Resets the breadcrumb tree entirely; this effectively drops out all pre-existing items, allowing new ones to be laden in.
 * @param state Vuex state object
 */
export function resetBreadcrumbs(state) {
    state.page_properties.breadcrumbs.splice(0);
}

/**
 * Changes the time display on the top of the page to show a new time.
 * @param state Vuex state object
 * @param time New time, as a string
 */
export function changeDisplayedTime(state, time) {
    state.misc.displayed_time = time;
}

/**
 * Changes the page title property; this affects the centered heading in the menu bar.
 * @param state Vuex state object
 * @param title New title, as a string
 */
export function changeDisplayedTitle(state, title) {
    state.page_properties.title = title;
    document.title = ("Koodeksi - " + title);
}

/**
 * Updates the user data in the Vuex state, simply replacing the old data with the new one provided.
 *
 * @param state Vuex state object
 * @param user_data User data, or nil if logged out
 */
export function updateUserData(state, user_data) {
    Vue.set(state, "user", user_data);
}

/**
 * Erases a preload; by latest at after a page change, it may not be wise to hold on to a preload. If a navigation is about to occur, and there are prior pages, this mutation is called to clean up the preload data.
 * One exception: if 'retain_preload_after_change' is set, it will be erased and preload will be left instead. This is to allow single exceptions
 * @param state State
 */
export function erasePreload(state) {

    if (state.retain_preload_after_change) {
        console.debug("Retain flag set, retaining preload for this one time.");
        state.retain_preload_after_change = false;
    } else {
        console.debug("Discarding all preloads..");
        Vue.set(state, "preload", null);
    }
}

/**
 * Retains a preload. In all cases, this function flags the 'retain_preload_after_change' variable so that the preload is not erased on immediately following navigation
 * However, if the value is also non-null, it replaces the preload state with the given value.
 * @param state Current state
 * @param value If not null, replace preload state
 */
export function retainPreload(state, value) {
    state.retain_preload_after_change = true;
    if (value !== null && value !== undefined) Vue.set(state, "preload", value);
}

/**
 * After a navigation change, it is a good idea to clean up any view specific data that may linger. This mutation takes care of that.
 *
 * @param state Current state
 */
export function clearView(state) {
    console.debug("Discarding view data..");
    Vue.set(state, "view", {});
}


export function clearSpecificViewPath(state, {view_path}) {
    if (typeof view_path !== "string" || view_path.length < 1) throw "View path must be a string and nonempty";
    Vue.set(state.view, view_path, null);
}
/**
 * If it is not pertinent to show the menu bar, one can hide it from here.
 */
export function setHideTopMenu(state, hide) {
    Vue.set(state.misc, "hide_top_menu", hide === true);
}
/**
 * Initializes a subfield in view-specific data to an initial state. This may be useful when working with forms.
 * This does not overwrite anything if there is already something initialized
 *
 * @param state State
 * @param view_path Name of the view subfield to alter
 * @param data Data to initialize with, if not already initialized
 */
export function initializeView(state, {view_path, data}) {
    if (typeof view_path !== "string" || view_path.length < 1) throw "View path must be a string and nonempty";
    if (data === null || data === undefined) throw "Data must be defined!";

    if (state.view[view_path] === null || state.view[view_path] === undefined) Vue.set(state.view, view_path, data);
}

/**
 *
 * Alters a subfield in view data; this is accomplished by a changing function that will be called if the view subfield exists.
 *
 * @param state State
 * @param view_path Name of the view subfield to alter, if it exists
 * @param data_func Function to call; if a defined result (may also be null!) is returned, it will entirely replace the contents. If undefined, then no Vue.set will be called; this does not meant that changes are not commmited, the function may still make changes!.
 */
export function alterView(state, {view_path, data_func}) {
    if (typeof view_path !== "string" || view_path.length < 1) throw "View path must be a string and nonempty";
    if (typeof data_func !== "function") throw "Data function must be a function!";

    // Call the function, if everything is defined
    if (state.view[view_path] !== null && state.view[view_path] !== undefined) {
        const res = data_func(state.view[view_path]);
        if (res !== undefined) {
            Vue.set(state.view, view_path, res);
        }
    }

}

/**
 * Alters a block field. This is a convenience function for blocks, which will inevitably need to conveniently access its internal subfields via Vuex
 * @param state State
 * @param block_id Block identifier
 * @param key Key to edit
 * @param value Value to assign
 */
export function alterBlockField(state, {block_id, key, value}) {
    if (typeof block_id !== "number" || typeof block_id !== "string") throw "Block ID must either be a number or a string";
    if (typeof key !== "string") throw "Key must be a string!";
    let key_str = "block-" + block_id.toString();

    Vue.set(state.view[block_id], key, value);
}