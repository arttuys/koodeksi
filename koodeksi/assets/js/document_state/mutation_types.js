/*
 * This file contains the simple declarations for various mutation types. Many of them look similar to mutations; this is due to the requirement that
 * mutations are synchronous, which may be hard to assure from a graphical user interface. Many of them then are simple wrappers executing the prerequisite mutation.
 */
export const PUSH_TERMINAL_ROW = 'PUSH_TERMINAL_ROW';

export const RESET_BREADCRUMBS = 'RESET_BREADCRUMBS';
export const PUSH_BREADCRUMB = 'PUSH_BREADCRUMB';
export const POP_BREADCRUMB = 'POP_BREADCRUMB';
// Generic page contents
export const UPDATE_PAGE_TITLE = 'UPDATE_PAGE_TITLE';
export const CHANGE_DISPLAYED_TIME = 'CHANGE_DISPLAYED_TIME';
export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';
export const SET_HIDE_TOP_MENU = 'SET_HIDE_TOP_MENU';
// Preload and view specific data
export const ERASE_PRELOAD = 'ERASE_PRELOAD';
export const RETAIN_PRELOAD = 'RETAIN_PRELOAD';
export const RESET_VIEW_DATA = 'RESET_VIEW_DATA';
export const RESET_SPECIFIC_VIEW_PATH = 'RESET_SPECIFIC_VIEW_PATH';
export const INITIALIZE_VIEW = 'INITIALIZE_VIEW';
export const ALTER_VIEW = 'ALTER_VIEW';
// DocView block specific stuff
export const ALTER_BLOCK_FIELD = 'ALTER_BLOCK_FIELD';