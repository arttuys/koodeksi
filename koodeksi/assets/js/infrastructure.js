/*
    Koodeksi - infrastructure.js

    This file contains the code required to set up the initial scaffolding for the page.
    This essentially means:
        - Preparing various Vue components, Vuex and VueRouter
        - Initializing the state to a sane default
        - Setting up all actions and mutations, which will be the interface used by the Vue components for various larger operations.
        - Setting up routing, ensuring that router links end up to correct components.

 */
// Initialize Vuex and VueRouter
import VueObserveVisibility from 'vue-observe-visibility'

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueObserveVisibility);

// Unfortunately, the nice lazy optimization of Brunch seemingly does not pull in all prerequisite modules. These 'shim' lines are required to hint Brunch to ensure all requisite files are located
import 'babel-runtime/helpers/defineProperty'
import 'babel-runtime/helpers/typeof'
import 'babel-runtime/core-js/promise'
import 'babel-runtime/core-js/object/entries'
import 'babel-runtime/helpers/sliced-to-array'
import 'babel-runtime/core-js/json/stringify'
var KOODEKSI_AVAILABLE_PLUGINS = require("./plugins/config").plugins_available;
// Load Koodeksi Terminal component
Vue.component("kd-terminal", require("../vue-components/terminal/terminal.vue"));

// Load Koodeksi Root and associated components
Vue.component("kd-root", require("../vue-components/structure/root/root.vue"));
Vue.component("kd-std_errorbox", require("../vue-components/structure/misc/std_errorbox.vue"));
// Load main document view and associated components
Vue.component("kd-path-trampoline", require("../vue-components/structure/dynamic/path_trampoline.vue"));
Vue.component("kd-document-view-root", require("../vue-components/structure/dynamic/document/view_root"));
Vue.component("kd-document-block", require("../vue-components/structure/dynamic/document/block.vue"));

// Enumerate plugins
//for (var i = 0; i < plugin_names.length; i++) {
//    var pname = plugin_names[i];
//    Vue.component("kd-plugin-" + pname, require("../vue-components/structure/plugins/" + pname + ".vue"));
//}
var keylist = Object.keys(KOODEKSI_AVAILABLE_PLUGINS);
for (let key in Object.keys(KOODEKSI_AVAILABLE_PLUGINS)) {
    var ptitle = keylist[key];
    console.debug("Loading plugin: " + ptitle);
    var pname = KOODEKSI_AVAILABLE_PLUGINS[ptitle];
    Vue.component("kd-plugin-" + pname, require("../vue-components/structure/plugins/" + pname + ".vue"));
}
// Load miscellanous components
Vue.component("kd-misc-index", require("../vue-components/structure/misc/pages/index"));
Vue.component("kd-misc-formbox", require("../vue-components/structure/misc/form_box.vue"));
Vue.component("kd-misc-loadspinner", require("../vue-components/structure/misc/load_spinner.vue"));
Vue.component("kd-misc-tablelist", require("../vue-components/structure/misc/tablelist.vue"));
Vue.component("kd-page-signin", require("../vue-components/structure/misc/pages/signin.vue"));
Vue.component("kd-page-signout", require("../vue-components/structure/misc/pages/signout.vue"));
Vue.component("kd-page-signup", require("../vue-components/structure/misc/pages/signup.vue"));
Vue.component("kd-page-user", require("../vue-components/structure/misc/pages/user.vue"));
/// DO NOT TOUCH BELOW THIS PART - This initializes the state and route

import "phoenix_html"

import * as DocumentMutations from "js/document_state/mutations"
import * as MutationTypes from "js/document_state/mutation_types"
import * as ActionTypes from "js/document_state/action_types"
import * as UserActions from "js/document_state/user_actions"



// Testing REPL action

function repl_action({commit, state}, {id, line}) {
    commit(MutationTypes.PUSH_TERMINAL_ROW, {id: id, line: line, important: true});
    commit(MutationTypes.PUSH_TERMINAL_ROW, {id: id, line: `${eval(line)}`, important: false});
}

// Updates the shown time
function updateTimeAction({commit, state}) {
    commit(MutationTypes.CHANGE_DISPLAYED_TIME, new Date().toUTCString());
}

// Updates the page title, and changes (simultaneously) the document's title
function updateTitle({commit, state}, title) {
    commit(MutationTypes.UPDATE_PAGE_TITLE, title);
}

/**
 * Bootstraps the initial state of the page, by generating the prerequisite data structures
 *
 * @param first_preload Server-supplied preload; this may be empty, or may contain some data. This is intended to be useful for the first page component that loads, as it will likely contain relevant data
 * @param user_data User data; this is preloaded by the server as well, and contains the current credentials as identified by the server.
 * @returns {{documentRouter: *, documentState: Store|*}}
 */
function generateInitialState(first_preload, user_data) {
// Initialize the core state storage
    const documentState = new Vuex.Store(
        {
            state: {
                /* Page-specific properties that are typically changed on each navigation */
                page_properties: {
                    title: "",
                    /* Define breadcrumbs, which will be shown by the breadcrumb display component */
                    breadcrumbs: []
                },
                /* View-specific data, depends on the component */
                view: {},
                /* Miscellaneous properties*/
                misc: {
                    displayed_time: "",
                    hide_top_menu: false // By default, show the top menu; it may be hidden where required
                },
                plugin_names: KOODEKSI_AVAILABLE_PLUGINS,
                /* In some cases, e.g using a path trampoline, it may be beneficial to retain preloads after a navigation. */
                retain_preload_after_change: false,
                /* Preload data; this may be used by the loaded page. This is often sent by the server, but may also be loaded by a separate AJAX request */
                preload: first_preload,
                /* User data; this may be preloaded, or it may be altered even after the page is loaded*/
                user: user_data,
                terminals: {}
            },
            mutations: {
                // Pushes a terminal event to the state array.
                [MutationTypes.PUSH_TERMINAL_ROW]: DocumentMutations.pushTerminalRow,
                [MutationTypes.PUSH_BREADCRUMB]: DocumentMutations.pushBreadcrumb,
                [MutationTypes.POP_BREADCRUMB]: DocumentMutations.popBreadcrumb,
                [MutationTypes.RESET_BREADCRUMBS]: DocumentMutations.resetBreadcrumbs,
                [MutationTypes.CHANGE_DISPLAYED_TIME]: DocumentMutations.changeDisplayedTime,
                [MutationTypes.UPDATE_PAGE_TITLE]: DocumentMutations.changeDisplayedTitle,
                [MutationTypes.UPDATE_USER_DATA]: DocumentMutations.updateUserData,
                [MutationTypes.ERASE_PRELOAD]: DocumentMutations.erasePreload,
                [MutationTypes.RETAIN_PRELOAD]: DocumentMutations.retainPreload,
                [MutationTypes.RESET_VIEW_DATA]: DocumentMutations.clearView,
                [MutationTypes.INITIALIZE_VIEW]: DocumentMutations.initializeView,
                [MutationTypes.RESET_SPECIFIC_VIEW_PATH]: DocumentMutations.clearSpecificViewPath,
                [MutationTypes.ALTER_VIEW]: DocumentMutations.alterView,
                [MutationTypes.SET_HIDE_TOP_MENU]: DocumentMutations.setHideTopMenu
            },
            actions: {
                // Enters a row into a terminal; this may be invoked by an user, but also by code.
                [ActionTypes.ENTER_ROW_TO_TERMINAL]: repl_action,
                [ActionTypes.UPDATE_TIME]: updateTimeAction,
                [ActionTypes.UPDATE_PAGE_TITLE]: updateTitle,
                [ActionTypes.PUSH_BREADCRUMB]: function ({commit, state}, hash) {
                    commit(MutationTypes.PUSH_BREADCRUMB, hash);
                },
                [ActionTypes.RESET_BREADCRUMBS]: function ({commit, state}) {
                    commit(MutationTypes.RESET_BREADCRUMBS);
                },
                [ActionTypes.UPDATE_USER_SESSION]: UserActions.updateUserAction
            },
            getters: {
                // Define a shorthand for path from home; this implicitly adds a link to home, as required
                path_from_home: state => {
                    return [{name: "KOODEKSI", addr: "/"}].concat(state.page_properties.breadcrumbs);
                },
                page_title: state => {
                    return state.page_properties.title;
                },
                current_time: state => {
                    return state.misc.displayed_time;
                },
                user: state => {
                    return state.user;
                }
            }
        }
    );

    const documentRouter = new VueRouter(
        {
            mode: 'history',
            routes: [
                {path: '/', component: Vue.component('kd-misc-index')},
                {path: '/signin', component: Vue.component('kd-page-signin')},
                {path: '/signout', component: Vue.component('kd-page-signout')},
                {path: '/signup', component: Vue.component('kd-page-signup')},
                {path: '/errors/:id', component: Vue.component('kd-std_errorbox'), props: true},
                {path: '/content/:subpath+', component: Vue.component('kd-path-trampoline'), props: true},
                {path: '/user/:username', component: Vue.component("kd-page-user"), props: true},
                {path: '/*', redirect: '/errors/notfound'}]
        }
    );

    documentRouter.afterEach((to, from) => {
        // A particular observation; the only hook taking place after this hook is component-specific beforeRouteEnter callback!
        // If a component requires special initialization, it should take place there!

        // Erase preloads after navigating.
        if (from.matched.length > 0) documentState.commit(MutationTypes.ERASE_PRELOAD);
        // Clear view data
        documentState.commit(MutationTypes.RESET_VIEW_DATA);
    });

    return {"documentRouter": documentRouter, "documentState": documentState}
}

export {generateInitialState};