<!--

    Koodeksi - Document View Root

    This file is intended to be the root point for a document view.

    --

    A document view is special in a way that it can generate a lot of interactions and state changes on its own, unlike other pages, which typically only utilize
    the preload on its own but do not alter it further. There is also a substantial case to implement on-page refreshes; so far, it has been mostly judged that in case of
    state changes, it is often easier to simply hard reload the page than to implement the coding required for a soft one.

    --

    The way this component should work, is that once a preload is retrieved, following things should happen:
        - The initially provided schema (as in the preload data) is copied to an appropriate set of Vuex view paths
            - This schema will contain initial block IDs, and if so appropriately judged, their contents. This is to facilitate preloading; in all cases one needs to know the proper block ID, and it may be beneficial to know the contents as well to be able to render anything.
            - It will also contain knowledge if the user is to have permission to use editing tools.
            - Document plugin options are also to be provided
        - The schema will be then utilized to build a list of blocks.
            - A single block is a single Vue component; this block will contain prerequisite common editing and positioning controls if needed, and will control the position of a single element.
            - If appropriate, the block may be provided a size approximation; this will allow lazy loading on demand
            - The subcomponent itself will be signaled the following information
                - Its view path (containing all prerequisite information to show the block's contents)
                    - IMPORTANT: A block will not necessarily contain a database-backed ID! In this scenario, one should interpret it either as a new block scenario (only show editing controls), or an error (if no edit permissions are granted)

    View paths are to be provided as follows:
        "schema": contains the general schema, permissions, and order for the given page
            {
                edit_permission: <true | false> # Defines if the user has an edit permission for this page. If one exists, blocks should show editing tools for this user.
                blocks: [
                    <blocks IDs; these must all be numeric, anything else shall be a provisional ID!>
                ],
                provisional_blocks: [
                    <contains blocks IDs for those which do not yet have a database number, but have a definite order nevertheless. Each block though should have a random string ID STARTING WITH A LETTER, which can be used as a provisional designation>
                ]
                last_refresh_time: <unixtime> # This should indicate the last time this document has been refreshed
                document_plugin_opts: {
                    <for each plugin applicable, contains a substructure; do note that plugins should be able to handle missing options gracefully, and if necessary, generate a good initial state>
                    XXXX: {
                        doc_page_toc: { # Optional TOC dictionary, used to build a table of contents. Do note that ONLY established/saved blocks are considered; provisional blocks cannot participate in a TOC directory.
                            <for each block ID>: [{
                                title: <string> # Title for this specific element
                                depth: <integer> 1-6, with 1 being a top-level heading, and 6 being the most specific subheading
                                hint: <string> # By default, TOC links jump to the block; if your block provides a more specific position, fill this with the hint
                            }, ...]
                        }
                    }
                }
            }
         "block-XXXXXX": contains the data of a single block, where XXXXXX is either a number, or a provisional identifier (provisional identifiers should start with a letter)
         {
            loaded: <true | false> # Has this block been loaded yet? In case of provisional blocks, this simply controls if the editing screen should be shown
            order: <numeric ID> # Blocks are sorted according to this order. Provisional blocks are always after actual blocks with the same ordering number, as per provided by the insertion API which guarantees placement right after some existing order. In other cases, order of blocks with same order number is undefined
            errors: [] # If any error messages have arisen, they should be placed in this block. User will be provided a chance to reload (if there's no way to render a plugin), or alternatively the plugin is expected to render the error in a suitable way if view data and a plugin specification exists. All errors must be strings

            plugin: <string> # Plugin identifier for this block; this identifies the proper component to be used for rendering this block
            view_XXXXX: <..> # Plugin-specific view data. This data should be sufficient to render a block.
            edit_XXXXX: <..> # Plugin-specific edit data. This data may contain more data than the view data, but must nevertheless be self-sufficient; only keys with the "edit_" prefix will be sent to the plugin when editing is triggered.
         }
    --

    Several expectations are made by this component
        - Following routes should exist
            - /json/document/gateway (POST; must have transaction type, and document ID at least, may also require a block ID or something else as well)
                - Format for the request:
                {
                    doc_id => <document identifier where this transaction applies to>
                    transaction => <transaction data>
                }
                - Standard transaction types (<type> in transaction data JSON structure):
                    - <load-block> => gets full data for a block; after this operation, the block should be entirely viewable and possibly editable
                    - <push-new> => creates a new block. Edit data, plugin identifier, current order number and provisional block ID should be sent; in return, block ID and other data is to be provided, or alternatively an error message.
                    - <build-initial-preload> => when provided with a document ID, returns an apparent initial preload instead of a simple overlay; can be used as a reset mechanism in case of concurrent modification. This also changes the type of the success result; the data is stored in "to_preload" instead of "overlay"
                    - <...> => other transaction types may exist, but they all must return overlays. In general, these special types may generate submissions; the standard transactions only cover viewing, which is typically something that behaves the same for most, if not all plugins
                - Results return in this form:
                    {
                        result => <success | error | ...>
                        overlay => <result for overlay-generating transactions>
                        ----
                        to_preload => <result for a successful build-initial-preload transaction>
                    }
                - Most returning results are to be a kind of an "overlay" (barring special cases above); a JSON structure of the following form should be generated
                    - "added", "removed", "updated", "full_resets" signaling which block IDs are to be where.
                        - "added" => add or wholesale replace this block directly to the block data (and schema, if not already found), without alterations. Should contain appropriate block IDs, schema will be updated automatically
                        - "removed" => remove this block from block data and schema. May contain both provisional blocks (strings) and actual blocks (integers)
                        - "updated" => for each block ID provided, update ONLY keys that are directly provided - and ONLY if the blocks exist in the schema; no new blocks can be created with updates. This is different to "added", which wholesale replaces structures at a rougher level. Use for, say, ordering or incremental view updates. May both address provisional and actual blocks.
                        - "refresh_time" => if provided, update schema refresh time to this, to indicate that this document is now up to date
                        - "plugin_opts_updates" => if plugin options have changed, this contains a substructure:
                                {
                                    <plugin to be updated>: <all subkeys which are updated>
                                }
                        - "full_resets" => for each block ID provided, reload their contents fully. Resets their contents, but not the schema. Effectively, erases their view data
                        - "general_error" => if an error cannot be attributed to any single block, it should be indicated here.
                - If no overlay can be returned, an error will be indicated at the point of call per standard form; this typically means the block where the transaction was initiated from; failing that, an alert of some other kind should be shown.

        - For each plugin, a component named "kd-plugin-<pluginname>" should exist; this will be treated as a subcomponent for a given block which is managed by a given plugin
-->

<template>
    <div class="document_grid">
        <component v-bind:is="tocComponent" :toc_data="toc_data"></component>
        <div id="main_box">
            <template v-if="current_error.length > 0">
                <div class="content_inline_box kd_danger">
                    <strong><p v-for="error in current_error">{{error}}</p></strong>
                </div>
            </template>
            <template v-for="block in blocks_shown">
                <kd-document-block :block_id="block.block_id" :provisional="block.provisional"
                                   :edit_opts_visible="edit_opts_visible"
                                   v-on:transaction="process_transaction"></kd-document-block>
            </template>
            <template v-if="has_edit_perms">
                <div class="content_inline_box">
                    <template v-if="edit_opts_visible">
                        <p>Edit mode engaged. Select the buttons below to execute appropriate operations</p>
                        <p>
                            <template v-for="(title, name) in plugin_titles_and_names">
                                <a href="#">Add new {{name}}</a>
                            </template>
                        </p>
                    </template>
                    <template v-else>
                        <p>You have not enabled edit options. The page now shows as it would show to an ordinary user.
                            Click the link below to enable editing</p>
                    </template>
                    <br>
                    <a href="#" v-on:click="switch_edit_mode">{{edit_btn_title}}</a>
                </div>
            </template>
        </div>
    </div>
</template>

<script>
    import * as MutationTypes from '../../../../js/document_state/mutation_types'

    var tocComponent = require("./toc");

    module.exports = {
        data: function () {
            return {current_error: [], tocComponent: tocComponent, edit_opts_visible: false};
        },
        beforeMount: function () {
            // As specified, we should have an initial setup in the preload data.
            // The server returns a set of view paths, which correspond to the initial structures.
            // After these structures are inserted into the view path structures, we can readily show a basic document
            var paths = this.preload_data;
            for (const [key, value] of Object.entries(paths)) {
                this.$store.commit(MutationTypes.INITIALIZE_VIEW, {view_path: key, data: value});
            }
        },
        props: ["preload_data", "simple_slugs"],
        methods: {
            /**
             * If an error occurs, this function is to be used to append it
             **/
            append_error: function (str) {
                this.current_error.push(str.toString())
            },
            /**
             * Processes a given transaction; this is typically called in response to an event from a block component, in which case
             * this will send the transaction onwards, and apply necessary changes
             **/
            process_transaction: function (params) {
                console.log(JSON.stringify(params));

                var _this = this;
                // Right, we have a transaction. Let's send it ahead
                $.ajax({
                        url: "/json/document/gateway",
                        dataType: "json",
                        data: {doc_id: this.doc_id, transaction_data: JSON.stringify(params)},
                        cache: false,
                        type: "post",
                        headers: {
                            "x-csrf-token": window.csrf_token
                        },
                        success: function (data) {
                            console.log(JSON.stringify(data));
                            _this.append_overlay(data)
                        },
                        error: function () {
                            _this.append_error("Could not send a request to the server; try again, and failing that, please refresh this page")
                        }
                    }
                );
            },
            append_overlay: function (res) {
                if (res.result !== "success") {
                    this.append_error("Could not complete transaction: " + res.result)
                } else {
                    var overlay = res.overlay;
                    if (overlay.added !== undefined) {
                        for (const block in overlay.added) {
                            var block_data = overlay.added[block]
                            this.$store.commit(MutationTypes.ALTER_VIEW, {view_path: "block-" + block, data_func: function(view) {
                                    return block_data;
                                }})
                        }
                    }
                }
                /// TODO: Implement rest, "removed", et al
            }
        ,
        switch_edit_mode: function () {
            this.edit_opts_visible = !this.edit_opts_visible
        }
    },
    computed:
    {
        /**
         * Dynamically changing title for the edit button
         *
         **/
        edit_btn_title: function () {
            return this.edit_opts_visible ? "Discard unsaved changes and return to view mode" : "Enable edit mode"
        },
        /**
         * Plugin names
         **/
        plugin_titles_and_names: function () {
            return this.$store.state.plugin_names;
        }
    ,
        /**
         * Boolean value, if the user has edit permissions or not
         **/
        has_edit_perms: function () {
            if (this.$store.state.view.schema === undefined) return false;
            return this.$store.state.view.schema.edit_permission;
        }
    ,
        /**
         * Document identifier for AJAX calls
         **/
        doc_id: function () {
            var schema = this.$store.state.view.schema;
            if (schema === undefined) {
                return null // No schema available
            }

            return schema.doc_id;
        }
    ,
        /**
         * Blocks that are to be shown to the user. The returned array is sorted in the proper order, and will be assured that the appropriate view path exists for each provided ID
         * @returns {Array} Array of blocks
         */
        blocks_shown: function () {
            var schema = this.$store.state.view.schema;
            if (schema === undefined) {
                return [] // No schema available
            }

            // Retrieve block identifiers
            var db_blocks = schema.blocks;
            var provisional_blocks = schema.provisional_blocks;

            var found_blocks = [];
            // Check that there's a view path backing a block
            for (let value of db_blocks) {
                // Check that the path exists
                let pathname = "block-" + value.toString();
                if (!(this.$store.state.view[pathname] === undefined)) {
                    found_blocks.push({
                        "block_id": value.toString(),
                        "provisional": false,
                        "order": this.$store.state.view[pathname].order
                    })
                }
            }

            // Do the same for provisionals
            for (let value of provisional_blocks) {
                // Check that the path exists
                let pathname = "block-" + value.toString();
                if (!(this.$store.state.view[pathname] === undefined)) {
                    found_blocks.push({
                        "block_id": value.toString(),
                        "provisional": true,
                        "order": this.$store.state.view[pathname].order
                    })
                }
            }

            // Order the blocks
            found_blocks.sort(function (a, b) {
                if (b.order !== a.order) {
                    return b.order > a.order ? 1 : 1
                }
                if (b.provisional != a.provisional) {
                    return b.provisional ? -1 : 1;
                }

                return 0; // Equivalent
            });

            // Now, return them
            return found_blocks;
        }
    ,
        /**
         * TODO: ToC data, for rendering to the table of contents part
         * @returns {Array} ToC array
         */
        toc_data: function () {
            // Retrieve the store state first
            var pluginopts = this.$store.state.view.schema.document_plugin_opts;
            var items = [];
            // Let's collect the appropriate TOC options
            for (const [plugin, structure] of Object.entries(pluginopts)) {
                var toc = plugin["doc_page_toc"];

                if (toc === null || toc === undefined) continue; // Not defined, skip this plugin

                for (const [block_id, structure] of Object.entries(toc)) {
                    if (!this.$store.state.view.schema.blocks.includes(block_id)) continue; // Not in the list of blocks

                    // Check if this block is appropriately stored in the schema
                    var block_view = this.$store.state.view["block_" + block_id.toString()];
                    if (block_view === null || block_view === undefined) continue; // If this is undefined, we cannot determine the proper order; and therefore, cannot continue.

                    for (let row of structure) {
                        // Now we have all necessary data. Construct a structure; a special observation, if a block isn't yet loaded, a plugin-provided hint will not yet work - so in that case, substitute one with a default hint.
                        var data_hash = {
                            title: row.title,
                            depth: row.depth,
                            hint: block_view.loaded ? row.hint : undefined,
                            order: block_view.order
                        };
                        items.push(data_hash);
                    }
                }
            }

            items.sort(function (a, b) {
                return a.order - b.order;
            }); // See: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort, compareNumbers()
            return items;
        }


    }
    }
</script>

<style lang="scss">
</style>