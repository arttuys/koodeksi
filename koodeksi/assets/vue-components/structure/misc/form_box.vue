<!--

Koodeksi - Standard Form Box

Defines a standardized form box. A form box is basically a form, with following elements:
    - A title (shown in a small font, possibly optional)
    - Central component; this is what the user edits. Form box will be supplied its Vuex state path, to allow the form to find the pertinent contents. It is the central component's duty to fill it though.
    - Optionally, buttons to trigger a sending event or a reset (if available). Their properties are to be supplied as props, and they will trigger standardized behavior. This is helpful to, for instance, transparently utilize CSRF protection

    - A form box expects a following data structure under its state path:
    ```
    {
        data: {---}, # This is sent via the standardized sending methods, with each key as a parameter. This may also be a function, which will then retrieve the data
        errors: nil, # If not nil, a small error blurb will be displayed. This is also handled by automated sending
        buttons: { # Each button has their own name, which will be shown here
            "<button name>": {
                class: "<class name>" # Class of the button, in reference to SCSS styles
                enabled: true/false # If this button is to be enabled right now
                block_on_async: true/false # If this button either causes an async action that may cause side effects, or is otherwise affected by them; by setting this to true, only one of buttons with this property can be clicked at a time, waiting until their action completes
                action: [ # One of these choices:
                {
                    # There may be a single function, in which case it will only receive references to Vuex and vue-router, plus the state path (intended to be stored under 'view')
                    # It is intended that handler functions should not directly effect changes to Vue, and instead operate via Vuex state changes
                    func: function(path)
                },
                {
                    # Ideally, one defines a POST address; the data in "data" substructure is sent, and depending on the data received, different action may be taken.
                    path: "/signin", # POST path to use
                    on_success: function(path, data), # When successful, when {"ok" => data} is received, this is the function called. Return value is insignificant, but a Promise may be returned which will be then resolved.
                    on_error, function(path, err_type, post) # When errored, when {"error" => data} is received, this will be called. The entire POST request will be provided to the function, unlike with OK. This is optional; if not provided, a default handler will be called. If this function returns a non-nil result, the default handler will be called with that data (post). It is possible that post is null if the request did not even succeed

                }
                ]
            }
        }

    }
    - Other subkeys may exist in the data structure; those a formbox will not touch or interact with directly.
    - A form box will NEVER generate data out of blue; if this state structure doesn't exist or is substantially incomplete, no functionality will activate. It is the parent component's duty to establish the prerequisite structures!
    ```
-->

<template>
    <div class="content_inline_box kd_form_box">
            <p class="inline_box_title" v-if="title != null">{{title}}</p>
            <div class="nested_box kd_danger" v-if="errors.length > 0">
                <template v-for="error in errors">
                    <p><strong>{{error}}</strong></p>
                </template>
            </div>
            <slot name="preform">

            </slot>
            <form action="#" v-bind:class="class_bools">
                <slot></slot>
                <template v-for="(button, name) in buttons">
                    <button v-on:click.prevent="onButton(name)" :class="button.class" :disabled="button.enabled === false || (button.block_on_async && async_in_progress)">{{name}}</button>
                </template>
            </form>
    </div>
</template>

<script>
    import * as MutationTypes from '../../../js/document_state/mutation_types';
    import * as ActionTypes from '../../../js/document_state/action_types';
    module.exports = {
        data: function() {
            return {async_in_progress: false};
        },
        props: ["view_path", "title", "form_class"],
        methods: {
            /**
             * Called when an error occurs, upstream from a specified handler
             * @param text_reason If a special text reason should be shown; this will not be shown if the POST response contains error messages or if this is NULL
             * @param result_json A JSON structure containing the POST response
             */
          defaultOnError: function(text_reason, result_json) {
              // Check the text reason
                if (text_reason !== null && text_reason !== undefined) {
                    if (typeof result_json !== "object" || result_json["suppress_text_reason"] !== true) {
                        this.$store.commit(MutationTypes.ALTER_VIEW, {view_path: this.view_path, data_func: function(val) {
                                Vue.set(val, "errors", [text_reason]);
                                return undefined;
                        }})
                    }
                }
              if (typeof result_json !== "object") return; // Not an object, return as there's little action that can take place..

              // Check for certain special error keys, to determine if certain actions should take place
              if (result_json["should_reload"]) {
                  // Error requests us to reload.
                  location.reload(true);
                  return; // If a reload doesn't work, short-circuit and exit
              }
              if (result_json["should_update_user"]) {
                  // Error requests us to reload the user session, as the server believes we may have incorrect or irrelevant user data.
                  this.$store.dispatch(ActionTypes.UPDATE_USER_SESSION); // This will also trigger a reload if unsuccessful
              }
              // If the resulting error is a string..
              if (result_json["error"] !== null) {
                  this.$store.commit(MutationTypes.ALTER_VIEW, {view_path: this.view_path, data_func: function(val) {
                    Vue.set(val, "errors", result_json["error"]);
                    return undefined;
                  }})
              }
          },
          onButton: function(button_name) {
              this.$store.commit(MutationTypes.ALTER_VIEW, {view_path: this.view_path, data_func: function(val) {
                      Vue.set(val, "errors", []); // Reset the errors to empty
                      return undefined; // No need to call Vue.set second time in the mutation
                  }});
              // Retrieve button data
              let button_data = this.$store.state.view[this.view_path].buttons[button_name];
              if (button_data === undefined || button_data === null || button_data.enabled !== true) return; // Poorly defined button data, or this button is not even supposed to be enabled!

              // Check if this action is something that requires async blocking; by default, block
              const blocksAsync = button_data.block_on_async || true;

              // Let's check what the action is for this
              let action = button_data.action;
              if (typeof action === "object" && action != null) {
                if (blocksAsync) this.async_in_progress = true;
                // Does this have a path?
                const postPath = action.path;
                // Set a _this to retain access to our current scope
                let _this = this;
                if (typeof postPath === "string") {
                    // First, retrieve our data.
                    let data_def = this.$store.state.view[this.view_path].data;
                    let params = (typeof data_def === "function") ? data_def() : data_def;
                    // Send in a POST call
                    $.ajax({url: postPath,
                        dataType: "json",
                        cache: false,
                        type: "post",
                        data: params,
                        headers: {
                            "x-csrf-token": window.csrf_token
                        },
                    }).done(function (value) {
                        // Good, everything succeeded, and we have a value. Let's check what it contains
                        if (value["ok"] !== undefined) {
                            // We have a definite OK
                            if (action.on_success !== undefined) {
                                let res = action.on_success(_this.view_path, value["ok"]);
                                Promise.resolve(res).then(function(_) {_this.async_in_progress = false;}).catch(function(reason) {console.error("Success handler in KDFormBox rejected a promise"); console.error(reason)});
                            }

                        } else {
                            // This seems like an error.
                            let holdingPromise = Promise.resolve(value);
                            if (action.on_error !== undefined) {
                                holdingPromise = Promise.resolve(action.on_error(_this.view_path, "server-returned", value))
                            }

                            holdingPromise.then(function(value) {
                                if (value !== null && value !== undefined) {
                                    _this.defaultOnError(null, value); // Call the default handler
                                }
                                _this.async_in_progress = false; // Reset the async block once we have

                            }).catch(function(reason) {console.error("Error value handler in KDFormBox rejected a promise"); console.error(reason);});
                        }
                    }).fail(function (jqxhr, text_reason) {
                        // JQuery error, call our error handler
                        let holdingPromise = Promise.resolve(text_reason);
                        if (action.on_error !== undefined) {
                            holdingPromise = Promise.resolve(action.on_error(_this.view_path, text_reason, null))
                        }

                        holdingPromise.then(function(value) {
                            if (value !== null && value !== undefined) {
                                _this.defaultOnError("Could not submit your request right now. Try again after a while", value); // Call the default handler if we have something worthwhile
                            }
                            _this.async_in_progress = false; // Reset the async block once we have
                        }).catch(function(reason) {
                            console.error("Error handler rejected a promise");
                            console.error(reason);
                        })
                    });
                } else {
                    // Call an ordinary function
                    const func = action.func;
                    let res = func(this.view_path);
                    // Resolve it as a promise. Create us a holding variable to keep us a pathway to affect our state
                    let _this = this;
                    Promise.resolve(res).then(function(_) {
                        // Once the promise has resolved safely, release the async lock. If some error does happen, it will not be released; this is probably safer than allowing indeterminate actions to take place.
                        _this.async_in_progress = false;
                    }).catch(function(reason) {console.error("Function call handler in KDFormBox rejected a promise: " + reason)});
                }
              }

          }
        },
        computed: {
            class_bools: function() {
                return {[this.form_class]: true}
            },
            buttons: function() {
                const view_data = this.$store.state.view[this.view_path];
                if (view_data === null || view_data === undefined || view_data.buttons === null || view_data.buttons === undefined) return []; // No buttons, when no view data exists

                return view_data.buttons || []; // Otherwise, return the applying buttons
            },
            errors: function() {
                const view_data = this.$store.state.view[this.view_path];
                if (view_data === null || view_data === undefined || view_data.errors === null || view_data.errors === undefined) return []; // No errors can exist when view data doesn't exist

                let error_data = view_data.errors;
                // Now, let's check its type, and behave accordingly
                if (typeof error_data === "string") {
                    return [error_data];
                } else if (Array.isArray(error_data)) {
                    return error_data;
                } else {
                    return [] // We cannot represent anything else!
                }
            }
        }
    };
</script>

<style lang="scss">

</style>