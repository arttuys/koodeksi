defmodule Koodeksi.Utils.WittyQuotesTest do
  use ExUnit.Case

  test "WittyQuotes returns a nonempty string" do
    assert (String.length(elem(Koodeksi.Utils.WittyQuotes.random_quote(),1)) > 0)
  end
end