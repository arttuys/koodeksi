defmodule Koodeksi.Utils.StdRegexesTest do
  use ExUnit.Case

  test "Sane path URL regex approves and rejects appropriately" do
    paths = [
      {"", false},
      {"ÄÖ", false},
      {"manage$index", true},
       {"index", false},
      {"ruby/doc$test", true},
      {"abc//def", false},
      {"top$doc/document", false},
      {"$manage", true},
      {"python/$manage", true}
      ]

    Enum.each(paths, fn {key, val} ->
      assert Regex.match?(Koodeksi.Utils.StdRegexes.sane_path_url(), key) == val, "Testing #{key} for sane path, expecting #{val}"
    end)
  end
end