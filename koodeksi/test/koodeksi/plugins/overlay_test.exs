defmodule Koodeksi.PluginsTests.OverlayTest do
  use Koodeksi.DataCase
  alias KoodeksiPlugins.Overlay

  test "Basic overlay behavior is correct" do
    base_name = 53431
    overlay = Overlay.empty_overlay() |> Overlay.update_block(base_name, %{"magic" => "magic"}) |> Overlay.add_block(base_name, %{"memes" => "memes"}) |> Overlay.remove_block(base_name)
    assert overlay.added == %{}, "Additions are empty"
  end
end