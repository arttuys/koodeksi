defmodule Koodeksi.DB.BlockTest do
  use Koodeksi.DataCase
  alias Koodeksi.DB.User
  alias Koodeksi.DB.Directory
  alias Koodeksi.DB.Document
  alias Koodeksi.DB.Block
  alias Koodeksi.DB.Submission
  alias Koodeksi.DB.Log

  setup do
    # Set up a basic default structure
    {:ok, user} = Repo.insert(User.signup(%{:username => "Test", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}))
    {:ok, dir} = Repo.insert(Directory.new_directory(%{:title => "Root", :path_slug => nil, :owner_id => user.id, :parent_directory_id => nil})) # Set up a directory
    {:ok, _} = Repo.insert(Document.new_document(%{:title => "Test Document", :path_slug => "testdoc", :parent_directory_id => dir.id, :owner_id => user.id}))
    :ok
  end

  test "blocks can be inserted, and they seem to work properly" do
    # Get the document ID
    uid = Repo.one(from u in "users", where: u.username == "Test", select: u.id)

    doc_id_query = from d in "documents", where: d.title == "Test Document", select: d.id
    doc_id = Repo.one(doc_id_query)

    dir_id = Repo.one(from d in "directories", where: d.title == "Root", select: d.id)

    # Attempt to insert a new empty block
    block_cset = Block.new_block(%{:document_id => doc_id, :order => 0, :plugin => "markdown"})
    assert block_cset.valid?(), "Changeset is valid"
    {:ok, block} = Repo.insert(block_cset)

    # Check that all properties seem to be OK
    assert block.contents == nil, "No contents in an empty block"
    assert block.document_id == doc_id
    assert block.order == 0
    assert block.plugin == "markdown"
    assert block.last_submission == nil, "No submissions by default"

    # Attempt to insert a conflicting block; orders must not conflict
    block_cset = Block.new_block(%{:document_id => doc_id, :order => 0, :plugin => "latex"})
    assert block_cset.valid?(), "Changeset is apparently valid"
    {:error, _} = Repo.insert(block_cset) # This should fail!

    # Also try a certainly invalid plugin slug
    block_cset = Block.new_block(%{:document_id => doc_id, :order => 3, :plugin => "9882 NotGood"})
    refute block_cset.valid?(), "Changeset should be invalid due to invalid plugin slug"

    # Now, add a second one that is perfectly valid
    block_cset = Block.new_block(%{:document_id => doc_id, :order => 2, :plugin => "fileupload", :contents => <<0,12,11>>})
    assert block_cset.valid?(), "Changeset is valid"
    {:ok, block2} = Repo.insert(block_cset)

    # Check that blocks behave as expected
    assert block2.contents == <<0, 12, 11>>, "Contents were provided"
    assert block2.document_id == doc_id
    assert block2.order == 2
    assert block2.plugin == "fileupload"
    assert block2.last_submission == nil, "No submissions by default"

    # Test that block changes work as expected
    {:ok, block} = Repo.update(Block.change_contents(block, <<1,99,121>>))
    assert block.contents == <<1,99,121>>
    {:ok, block2} = Repo.update(Block.change_order(block2, 9))
    assert block2.order == 9
    {:ok, block} = Repo.update(Block.submitted_now(block))
    date_cmp = NaiveDateTime.compare(block.last_submission, NaiveDateTime.utc_now())
    assert date_cmp == :lt

    # Next, submissions!
    # A nil content should fail
    refute Submission.new_submission(block.id, uid, nil).valid?()
    # A filled content should succeed
    {:ok, submission} = Repo.insert(Submission.new_submission(block.id, uid, <<0, 2, 6>>, 3))
    {:ok, submission2} = Repo.insert(Submission.new_submission(block2.id, uid, <<33, 1, 9>>, 5) |> Submission.alter_contents(<<99,12,45>>) |> Submission.alter_score(9))
    assert submission.block_id == block.id
    assert submission.user_id == uid
    assert submission.contents == <<0, 2, 6>>
    assert submission.score == 3
    assert submission2.block_id == block2.id
    assert submission2.user_id == uid
    assert submission2.contents == <<99,12,45>> # Test sneakily that the content altering function works
    assert submission2.score == 9 # Same for scorekeeping
    # Test that log records work approximately validly
    log_chset = Log.changeset(%Log{}, %{:block_id => block.id, :submission_id => submission.id, :document_id => doc_id, :directory_id => dir_id, :initiating_user_id => uid, :sensitive => true, :originating_server => "test", :log_data => %{"test" => "works!"}})
    assert log_chset.valid?()
    {:ok, log} = Repo.insert(log_chset)
    assert log.log_data == %{"test" => "works!"}
    date_cmp = NaiveDateTime.compare(log.event_date, NaiveDateTime.utc_now())
    assert date_cmp == :lt
    assert log.sensitive == true
    assert log.originating_server == "test"
    # Reload the submission
    submission = Repo.get(Submission, submission.id) |> Repo.preload([:logs])
    assert length(submission.logs) == 1, "Logs are of correct length"
    assert hd(submission.logs).id == log.id, "Logs are found from the correct location"

    # Reload the blocks
    block = Repo.one(from b in Koodeksi.DB.Block, where: b.id == ^block.id) |> Repo.preload([:submissions, :logs])
    block2 = Repo.one(from b in Koodeksi.DB.Block, where: b.id == ^block2.id) |> Repo.preload([:submissions])

    # Check that logs are found from blocks
    assert length(block.logs) == 1, "Logs are of correct length"
    assert hd(block.logs).id == log.id, "Logs are found from the correct location"

    # Test that submissions are found
    assert length(block.submissions) == 1, "Submission is found"
    assert Enum.all?(block.submissions, &(&1.id == submission.id))
    assert length(block2.submissions) == 1, "Submission is found"
    assert Enum.all?(block2.submissions, &(&1.id == submission2.id))

    dir = Repo.get(Directory, dir_id) |> Repo.preload([:logs])
    # Check that logs are found from directories
    assert length(dir.logs) == 1, "Logs are of correct length"
    assert hd(dir.logs).id == log.id, "Logs are found from the correct location"

    # Test that user knows its submissions
    user = Repo.get(User, uid) |> Repo.preload([:document_submissions, :initiated_action_logs])
    assert length(user.document_submissions) == 2
    assert Enum.any?(user.document_submissions, &(&1.id == submission.id))
    assert Enum.any?(user.document_submissions, &(&1.id == submission2.id))
    assert length(user.initiated_action_logs) == 1, "Logs are of correct length"
    assert hd(user.initiated_action_logs).id == log.id, "Logs are found from the correct location"
    # Test that documents can find blocks
    document = Repo.one(from d in Koodeksi.DB.Document, where: d.id == ^doc_id) |> Repo.preload([:blocks, :logs])
    assert length(document.blocks) == 2, "Correct amount of blocks"
    assert Enum.any?(document.blocks, &(&1.id == block.id)), "Block 1 found"
    assert Enum.any?(document.blocks, &(&1.id == block2.id)), "Block 1 found"
    assert length(document.logs) == 1, "Logs are of correct length"
    assert hd(document.logs).id == log.id, "Logs are found from the correct location"
  end
end