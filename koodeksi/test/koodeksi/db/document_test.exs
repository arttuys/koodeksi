defmodule Koodeksi.DB.DocumentTest do
  use Koodeksi.DataCase
  alias Koodeksi.DB.User
  alias Koodeksi.DB.Directory
  alias Koodeksi.DB.Document
  alias Koodeksi.DB.DocumentEditPerm
  alias Koodeksi.DB.DocumentPluginOptions

  setup do
    # Set up a basic default structure
    {:ok, user} = Repo.insert(User.signup(%{:username => "Test", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}))
    {:ok, _} = Repo.insert(User.signup(%{:username => "SecondTest", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}))
    {:ok, _} = Repo.insert(Directory.new_directory(%{:title => "Root", :path_slug => nil, :owner_id => user.id, :parent_directory_id => nil})) # Set up a directory
    :ok
  end

  test "validate document titles" do
    # Get the user's ID
    uid_query = from u in "users", where: u.username == "Test", select: u.id
    uid = Repo.one(uid_query)

    # Get the directory ID
    dir_id_query = from d in "directories", where: d.title == "Root", select: d.id
    dir_id = Repo.one(dir_id_query)

    # Check for reasonable, valid titles
    Enum.each(["Tips and tricks", "Best Ruby clichés"], fn(x) ->
      changeset = Document.new_document(%{:title => x, :path_slug => "path", :owner_id => uid, :parent_directory_id => dir_id})
      assert changeset.valid?(), "Checking #{x}"
    end)

    # Whileas these should not be valid at all, ever
    Enum.each(["  ", "!#!", "Quite\t"], fn(x) ->
      changeset = Document.new_document(%{:title => x, :path_slug => "path", :owner_id => uid, :parent_directory_id => dir_id})
      refute changeset.valid?(), "Checking #{x}"
    end)
  end

  test "refute invalid document path slugs" do
    # Get the user's ID
    uid_query = from u in "users", where: u.username == "Test", select: u.id
    uid = Repo.one(uid_query)

    # Get the directory ID
    dir_id_query = from d in "directories", where: d.title == "Root", select: d.id
    dir_id = Repo.one(dir_id_query)

    # Check that none of these are valid
    Enum.each(["%#¤!ka", "ÄLÖ", "Spaces Are Bad", "!nope"], fn(x) ->
      changeset = Document.new_document(%{:title => "Is Okay", :path_slug => x, :owner_id => uid, :parent_directory_id => dir_id})
      refute changeset.valid?(), "Checking: '#{x}'"
    end)
  end

  test "new documents can be added, their properties are correct, and edit permissions work" do
    uid_query = from u in "users", where: u.username == "Test", select: u.id
    uid = Repo.one(uid_query)

    # Get the 2nd user's ID
    uid2_query = from u in "users", where: u.username == "SecondTest", select: u.id
    uid2 = Repo.one(uid2_query)

    dir_id_query = from d in "directories", where: d.title == "Root", select: d.id
    dir_id = Repo.one(dir_id_query)

    # Attempt to create a document; this should succeed, with no conflicts
    document_cset = Document.new_document(%{:title => "Best practices for Python", :path_slug => "best_practices_python", :parent_directory_id => dir_id, :owner_id => uid, :visible_to_all => false, :anyone_can_access => true})
    assert document_cset.valid?(), "Changeset is valid"
    {:ok, doc1} = Repo.insert(document_cset)
    # Attempt to cause a conflict with titles. This should not succeed
    document_cset2 = Document.new_document(%{:title => "Best practices for Python", :path_slug => "best_practices_python_2", :parent_directory_id => dir_id, :owner_id => uid})
    assert document_cset2.valid?(), "Changeset is valid (so far)"
    {:error, _} = Repo.insert(document_cset2)
    # Attempt to cause a conflict with slugs. This should not succeed
    document_cset3 = Document.new_document(%{:title => "Best practices for Python - part 2", :path_slug => "best_practices_python", :parent_directory_id => dir_id, :owner_id => uid})
    assert document_cset3.valid?(), "Changeset is valid (so far)"
    {:error, _} = Repo.insert(document_cset3)

    # Compose a document from small functions
    document_composed_cset = %Document{}
                              |> Document.change_title("Composing Ruby Programs")
                              |> Document.change_path_slug("ruby_progs")
                              |> Document.change_owner_id(uid)
                              |> Document.change_parent_directory_id(dir_id)
                              |> Document.change_access_rights(%{:visible_to_all => true, :anyone_can_access => false})
                              |> cast(%{:created => NaiveDateTime.utc_now()}, [:created])
    # This should also succeed
    assert document_composed_cset.valid?(), "Composed document changeset should be valid"
    {:ok, doc2} = Repo.insert(document_composed_cset)

    # Let's test plugin options - this time for document 1!
    {:ok, pluginopt1} = Repo.insert(DocumentPluginOptions.changeset(%DocumentPluginOptions{}, %{:document_id => doc1.id, :plugin => "markdown", :data_map => %{:test_key => 1234}}))
    assert pluginopt1.document_id == doc1.id, "Document ID is right"
    assert pluginopt1.plugin == "markdown", "Plugin is correct"
    assert pluginopt1.data_map == %{:test_key => 1234}, "Data map is correct"
    # Before doing a document-specific check, we shall as well test the change function
    {:ok, _} = Repo.update(DocumentPluginOptions.change_content(pluginopt1, %{:changed => "this map has changed!"}))


    # A conflicting option should not succeed
    {:error, _} = Repo.insert(DocumentPluginOptions.changeset(%DocumentPluginOptions{}, %{:document_id => doc1.id, :plugin => "markdown", :data_map => %{:test_key => 1234}}))
    # Nor something with a bad name
    {:error, _} = Repo.insert(DocumentPluginOptions.changeset(%DocumentPluginOptions{}, %{:document_id => doc1.id, :plugin => "%#¤#!!", :data_map => %{:test => "impossible!"}}))
    # But we should be able to add a second plugin option..
    {:ok, pluginopt2} = Repo.insert(DocumentPluginOptions.changeset(%DocumentPluginOptions{}, %{:document_id => doc1.id, :plugin => "latex", :data_map => %{:text => "hello!"}}))

    # Let's remove it though.
    {:ok, _} = Repo.delete(pluginopt2)
    # Let's test permissions
    {:ok, _} = Repo.insert(DocumentEditPerm.permission_changeset(%DocumentEditPerm{}, %{:document_id => doc2.id, :user_id => uid2}))
    # Due to an unique constraint, this should not succeed again
    {:error, _} = Repo.insert(DocumentEditPerm.permission_changeset(%DocumentEditPerm{}, %{:document_id => doc2.id, :user_id => uid2}))

    # Reload both documents, including edit permissions and editors
    doc1 = Repo.get(Document, doc1.id) |> Repo.preload([:edit_permissions, :plugin_options])
    doc2 = Repo.get(Document, doc2.id) |> Repo.preload([:edit_permissions, :editors, :plugin_options])

    # Check that the documents contain what they are supposed to
    assert doc1.title == "Best practices for Python", "Title is correct"
    assert doc1.path_slug == "best_practices_python", "Path slug is correct"
    assert doc1.visible_to_all == false
    assert doc1.anyone_can_access == true
    c_datecmp = NaiveDateTime.compare(doc1.created, NaiveDateTime.utc_now())
    assert c_datecmp == :lt || c_datecmp == :eq
    assert length(doc1.edit_permissions) == 0, "No edit permissions for this document" # None!
    assert length(doc1.plugin_options) == 1, "Plugin options for doc1 exist"
    # Beware of the transparent JSON conversion!
    assert Enum.all?(doc1.plugin_options, &(&1.data_map == %{"changed" => "this map has changed!"} && &1.plugin == "markdown")), "Plugin options for doc1: #{inspect doc1.plugin_options}" # Test that the data map has changed properly, but the plugin name is still OK

    assert doc2.title == "Composing Ruby Programs", "Title is correct"
    assert doc2.path_slug == "ruby_progs", "Path slug is correct"
    assert doc2.visible_to_all == true
    assert doc2.anyone_can_access == false
    assert length(doc2.edit_permissions) == 1, "Edit permissions exist"
    assert length(doc2.plugin_options) == 0, "Plugin options for doc2 do not exist"
    assert Enum.any?(doc2.edit_permissions, &(&1.user_id == uid2)), "Correct user has an edit permission"
    assert Enum.all?(doc2.editors, &(&1.id == uid2)), "Reference to correct user exists"
    # Check that editing dates work as expected
    assert doc2.last_edited == nil
    {:ok, doc2} = Repo.update(doc2 |> Document.edited_now())
    le_datecmp = NaiveDateTime.compare(doc2.last_edited, NaiveDateTime.utc_now())
    assert le_datecmp == :lt || le_datecmp == :eq

    # Check that known foreign key references contain what they are supposed to
    dir = Repo.one(from d in Koodeksi.DB.Directory, where: d.title == "Root") |> Repo.preload(:documents)
    user = Repo.one(from u in Koodeksi.DB.User, where: u.username == "Test") |> Repo.preload(:owned_documents)
    user2 = Repo.one(from u in Koodeksi.DB.User, where: u.id == ^uid2) |> Repo.preload(:edit_permitted_documents)

    # Check that permissions can be found through here as well. There should be only one user
    assert Enum.all?(user2.edit_permitted_documents, &(&1.id == doc2.id)), "Only one editable document is found, and it is document 2"

    # Check that both directory reference and user reference contain correct data
    Enum.each([dir.documents, user.owned_documents], fn(x) ->
      assert length(x) == 2, "Must have correct amount of documents"
      assert Enum.any?(x, &(&1.id == doc1.id)), "Must find document 1"
      assert Enum.any?(x, &(&1.id == doc2.id)), "Must find document 2"
    end
    )

    # Test that we can not delete a directory when it contains documents; make sure the correct constraint is mentioned
    assert_raise(Ecto.ConstraintError, ~r/documents_parent_directory_id_fkey/, fn() -> Repo.delete(dir) end)

    # But if we delete all documents..
    {:ok, _} = Repo.delete(doc1)
    {:ok, _} = Repo.delete(doc2)

    # .. it should be OK
    {:ok, _} = Repo.delete(dir)
  end
end