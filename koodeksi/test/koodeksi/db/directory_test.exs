defmodule Koodeksi.DB.DirectoryTest do
  use Koodeksi.DataCase
  alias Koodeksi.DB.User
  alias Koodeksi.DB.Directory

  setup do
    {:ok, _} = Repo.insert(User.signup(%{:username => "Test", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}))
    :ok
  end

  defp recursive_directory_preload(dir) do
    preloaded_once = dir |> Repo.preload([:directory_owner, :subdirectories])
    %{preloaded_once | subdirectories: Enum.map(preloaded_once.subdirectories, &(recursive_directory_preload(&1)))}
  end

  test "validate titles" do
    # Get the user's ID
    query = from u in "users", where: u.username == "Test", select: u.id
    id = Repo.one(query)

    # These can hit a careless regex checker, so check for these valid options
    Enum.each(["Be considerate", "O'Malley", "Äläpäs"], fn(x) ->
      changeset = Directory.new_directory(%{:title => x, :path_slug => "path", :owner_id => id})
      assert changeset.valid?(), "Checking #{x}"
    end)

    # Whileas these should not be valid at all.
    Enum.each(["  ", "%¤", "Almost\n"], fn(x) ->
      changeset = Directory.new_directory(%{:title => x, :path_slug => "path", :owner_id => id})
      refute changeset.valid?(), "Checking #{x}"
    end)
  end

  test "refute invalid slugs" do
    # Get the user's ID
    query = from u in "users", where: u.username == "Test", select: u.id
    id = Repo.one(query)

    # Check that none of these are valid
    Enum.each(["%#¤!ka", "ÄLÖ", "Spaces Are Bad"], fn(x) ->
      changeset = Directory.new_directory(%{:title => "Is Okay", :path_slug => x, :owner_id => id})
      refute changeset.valid?(), "Checking: '#{x}'"
    end)
  end

  test "folder structures can be created, appropriately accessed, and stackingly deleted (when empty) via Ecto" do
    # Locate the user's id
    query = from u in "users", where: u.username == "Test", select: u.id
    id = Repo.one(query)
    # Create the first directory
    changeset = Directory.new_directory(%{:title => "Magic Files", :path_slug => "magicfls", :owner_id => id, :anyone_can_edit => true, :anyone_can_access => false, :visible_to_all => true})
    assert changeset.valid?(), "Changeset must be valid"
    {:ok, folder} = Repo.insert(changeset)
    # Create the subdirectory 1
    changeset = Directory.new_directory(%{:title => "Good Tricks", :path_slug => "good", :owner_id => id, :parent_directory_id => folder.id})
    assert changeset.valid?(), "Changeset must be valid"
    {:ok, _} = Repo.insert(changeset)

    # Attempt to cause a conflict with a nonunique title!
    changeset = Directory.new_directory(%{:title => "Good Tricks", :path_slug => "good2", :owner_id => id, :parent_directory_id => folder.id})
    assert changeset.valid?(), "Changeset must be valid" # Must be valid before a DB constraint kicks in
    {:error, _} = Repo.insert(changeset) # This is expected

    # Create the subdirectory 2
    changeset = Directory.new_directory(%{:title => "Bad Tricks", :path_slug => "bad", :owner_id => id, :parent_directory_id => folder.id})
    assert changeset.valid?(), "Changeset must be valid"
    {:ok, _} = Repo.insert(changeset)

    # Attempt to cause a conflict with a nonunique slug!
    changeset = Directory.new_directory(%{:title => "Double-Bad Tricks", :path_slug => "bad", :owner_id => id, :parent_directory_id => folder.id})
    assert changeset.valid?(), "Changeset must be valid" # Must be valid before a constraint kicks in at the DB
    {:error, _} = Repo.insert(changeset) # Expected to error



    # Test the composing functions as well
    changeset = %Directory{} |> Directory.change_title("Composing Things")
                             |> Directory.change_path_slug("composing")
                             |> Directory.change_owner_id(id)
                             |> Directory.change_parent_directory_id(folder.id)
                             |> Directory.change_access_rights(%{visible_to_all: false, anyone_can_edit: false, anyone_can_access: true})
    assert changeset.valid?(), "Changeset must be valid"
    {:ok, subfolder3} = Repo.insert(changeset)

    assert subfolder3.title == "Composing Things"
    assert subfolder3.path_slug == "composing", "Slug must match"
    assert subfolder3.owner_id == id # Owner is correct
    assert subfolder3.visible_to_all == false
    assert subfolder3.anyone_can_edit == false
    assert subfolder3.anyone_can_access == true

    # Reload the folder; this time preloading associations for immediate subfolders
    folder = Repo.one(from d in Koodeksi.DB.Directory, where: is_nil(d.parent_directory_id)) |> recursive_directory_preload()
    # Reload the user to check if their structure seems to show correct data
    user = Repo.one(from u in Koodeksi.DB.User, where: u.id == ^id) |> Repo.preload([:owned_directories])
    #IO.puts(inspect folder)

    # Check that the folder contains what we want
    assert folder.title == "Magic Files", "Title must match"
    assert folder.path_slug == "magicfls", "Slug must match"
    assert folder.directory_owner.username == "Test", "Username must be discoverable through the owner"
    assert folder.anyone_can_edit == true
    assert folder.anyone_can_access == false
    assert folder.visible_to_all == true
    assert length(folder.subdirectories) == 3, "Correct amount of subdirectories"
    assert length(user.owned_directories) == 4, "User relation shows the correct amount of directories"
    assert Enum.any?(folder.subdirectories, &(&1.title == "Good Tricks")), "Correctly titled folder found"
    assert Enum.any?(folder.subdirectories, &(&1.title == "Bad Tricks")), "Correctly titled folder found"
    assert Enum.any?(folder.subdirectories, &(&1.title == "Composing Things")), "Correctly titled folder found"
    # Now, let's erase the root structure
    {:ok, _} = Repo.delete(folder)

    # Now that we know for sure what we deleted, attempt to create a directory with an invalid parent
    changeset = Directory.new_directory(%{:title => "Triple-Bad Tricks", :path_slug => "triple_bad", :owner_id => id, :parent_directory_id => folder.id})
    assert changeset.valid?(), "Changeset must be valid" # Must be valid before a constraint kicks in at the DB
    {:error, _} = Repo.insert(changeset) # Expected to error

    # Do we discover anything?
    assert Repo.all(from d in Koodeksi.DB.Directory) == [], "Deletions should cascade and leave absolutely nothing when removing the root directory."
  end
end