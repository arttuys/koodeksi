defmodule Koodeksi.DB.UserTest do
  use Koodeksi.DataCase
  alias Koodeksi.DB.User # Pull in User

  @valid_user %{:username => "Test", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}
  @valid_admin %{:username => "Admin", :password => "KeepAdminsPasswordSafe1!!!", :email => "admin@user.invalid", :inactive => true, :admin => true}

  defp date_valid({:ok, naive_datetime}) do
    cmp = NaiveDateTime.compare(naive_datetime, NaiveDateTime.utc_now())
    cmp == :lt || cmp == :eq
  end

  defp date_valid(_) do
    false
  end

  test "a perfectly valid user should be depositable - but not with a duplicate username!" do
    changeset = User.signup(@valid_user)
    case Repo.insert(changeset) do
      {:ok, _} -> "OK!"
      {:error, changeset} -> flunk("Something did not insert properly! - #{inspect changeset}")
    end

    # Let's do it again!
    case Repo.insert(User.signup(@valid_user)) do
      {:ok, _} -> flunk("Could be deposited twice with the same username - not OK!")
      {:error, _} -> "OK"
    end
  end

  test "username, email, inactive, admin date fields save properly on signup" do
    {:ok, admin} = Repo.insert(User.signup(@valid_admin))
    assert admin.username == @valid_admin[:username], "Usernames are OK"
    assert admin.email == @valid_admin[:email], "Emails are OK"
    assert admin.inactive == @valid_admin[:inactive], "Inactivity is OK"
    date_cmp = NaiveDateTime.compare(admin.administrative_user_date, NaiveDateTime.utc_now())
    assert date_cmp == :lt || date_cmp == :eq, "Date is not null, and is lesser or equal to the current date."
  end

  test "privilege changes are done properly" do
    no_change_set = User.set_access(%User{}, nil, nil)
    assert(fetch_change(no_change_set, :inactive) == :error && fetch_change(no_change_set, :administrative_user_date) == :error, "No changes")
    deactivate_but_make_admin_set = User.set_access(%User{}, false, true)
    assert(fetch_change(deactivate_but_make_admin_set, :inactive) == {:ok, true} && date_valid(fetch_change(deactivate_but_make_admin_set, :administrative_user_date)), "Properly applied permission change : #{inspect deactivate_but_make_admin_set}")
  end

  test "composing an user from single methods work and fails to work predictably" do
    assert (%User{} |> User.change_username("admin") |> User.change_password("StrongPassword!45") |> User.change_email("test@test.test") |> User.set_access(true, false)).valid?()
    refute (%User{} |> User.change_username("") |> User.change_password("StrongPassword!45") |> User.change_email("test@test.test") |> User.set_access(true, false)).valid?()
    refute (%User{} |> User.change_username("admin") |> User.change_password("weak") |> User.change_email("test@test.test") |> User.set_access(true, false)).valid?()
    refute (%User{} |> User.change_username("admin") |> User.change_password("StrongPassword!45") |> User.change_email("invalid!") |> User.set_access(true, false)).valid?()
  end

  test "reject invalid usernames" do
    Enum.each(["", "ThisIsAbsolutelyExcessivelyFarTooLengthyForASaneUsername", "NoSpecials!", "Nono\n", "null"], fn(x) ->
      changeset = User.signup(%{:username => x, :password => "ThisIsAnAcceptablePassword1234", :email => "test@user.invalid", :inactive => false, :admin => false})
      refute changeset.valid?(), "Refuting validity for #{x}"
    end)
  end

  test "password change changeset succeeds" do
    # Let's test that a password actually changes
    result = Repo.insert(User.signup(@valid_user))
    if (elem(result,0) != :ok) do
      flunk("Did not succeed in inserting a test user!")
    end

    user_pass = elem(result, 1).pwd_hash

    result2 = Repo.update(User.change_password(elem(result, 1), "ThisIsAnotherStr0ngPassWord!"))

    if (elem(result2,0) != :ok) do
      flunk("Did not succeed in updating the user!")
    end

    refute (elem(result2,1).pwd_hash == user_pass), "Password hashes should not match"
  end

  test "invalid emails are not accepted" do
      refute (User.signup(%{:username => "SpamORama", :password => "AlsoDefinitelyAStrongPassword!!123", :email => "NotOK!"}).valid?()), "Invalid emails should not be OK"
  end

  test "implict fields have sane defaults" do
      changeset = User.signup(%{:username => "ANormalUser", :password => "SpamORamaIsN0tFun!", :email => "testuser@testuser.com"})
      assert changeset.valid?(), "Changeset must be valid"
      # Insert to a repo
      res = Repo.insert(changeset)
      # Pattern match, assuming that it went OK
      {:ok, user} = res
      assert (user.inactive == false), "Users must not end up as inactive from start"
      assert (user.administrative_user_date == nil), "Users must not, under any circumstances, be implictly declared as admins"
  end
end