defmodule Koodeksi.Operations.DirDocTest do
  @moduledoc """
    This module tests directory-document operations
"""

  use Koodeksi.DataCase
  alias Koodeksi.Operations.User # Pull in User
  alias Koodeksi.Operations.DirDocument

  setup do
    # Set up a basic default structure
    {:ok, standard_user} = Repo.insert(Koodeksi.DB.User.signup(%{:username => "Test", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}))
    {:ok, editor_user} = Repo.insert(Koodeksi.DB.User.signup(%{:username => "Editor", :password => "ThisIZ2AStrongPassword!!!", :email => "editor@user.invalid", :inactive => false, :admin => false}))
    {:ok, admin_user} = Repo.insert(Koodeksi.DB.User.signup(%{:username => "Admin", :password => "ThisIZ2AStrongPassword!!!", :email => "editor@user.invalid", :inactive => false, :admin => true}))

    # Root dir is owned by user 1
    {:ok, root_dir} = Repo.insert(Koodeksi.DB.Directory.new_directory(%{:title => "Root", :path_slug => nil, :owner_id => standard_user.id, :parent_directory_id => nil, :anyone_can_access => true, :visible_to_all => true, :anyone_can_edit => true})) # Set up a directory

    # Dir 1 is owned by user 1, but user 2 has an editing permission inside
    # Doc 1 owned by user 1, no edit privs for user 2, access but no view
    {:ok, dir_python} = Repo.insert(Koodeksi.DB.Directory.new_directory(%{:title => "Python", :path_slug => "python", :owner_id => standard_user.id, :parent_directory_id => root_dir.id, :anyone_can_access => true, :visible_to_all => true, :anyone_can_edit => false})) # Set up a directory
    document_cset = Koodeksi.DB.Document.new_document(%{:title => "Best practices Python", :path_slug => "best_practices", :parent_directory_id => dir_python.id, :owner_id => standard_user.id, :visible_to_all => false, :anyone_can_access => true})
    {:ok, doc} = Repo.insert(document_cset)
    # User 2 has editing privileges on doc 2, also owned by user 1
    # Otherwise visible and listable
    document_cset_alt = Koodeksi.DB.Document.new_document(%{:title => "Best practices Python II", :path_slug => "best_practices_2", :parent_directory_id => dir_python.id, :owner_id => standard_user.id, :visible_to_all => true, :anyone_can_access => true})
    {:ok, doc_alt} = Repo.insert(document_cset_alt)
    {:ok, _} = Repo.insert(Koodeksi.DB.DocumentEditPerm.permission_changeset(%Koodeksi.DB.DocumentEditPerm{}, %{:document_id => doc_alt.id, :user_id => editor_user.id}))
    # Insert a test directory under Python directory
    {:ok, _} = Repo.insert(Koodeksi.DB.Directory.new_directory(%{:title => "Indentation Policies", :path_slug => "indentation", :owner_id => standard_user.id, :parent_directory_id => dir_python.id, :anyone_can_access => true, :visible_to_all => true, :anyone_can_edit => false})) # Set up a directory


    # Ruby dir and doc are owned by user2
    {:ok, ruby_dir} = Repo.insert(Koodeksi.DB.Directory.new_directory(%{:title => "Ruby Data", :path_slug => "ruby", :owner_id => editor_user.id, :parent_directory_id => root_dir.id, :anyone_can_access => false, :visible_to_all => true, :anyone_can_edit => true})) # Set up a directory
    document_cset2 = Koodeksi.DB.Document.new_document(%{:title => "Best practices Ruby", :path_slug => "best_practices", :parent_directory_id => ruby_dir.id, :owner_id => editor_user.id, :visible_to_all => true, :anyone_can_access => false})
    {:ok, doc2} = Repo.insert(document_cset2)

    # Meme dir owned by user2, doc owned by user1
    {:ok, meme_dir} = Repo.insert(Koodeksi.DB.Directory.new_directory(%{:title => "Memes", :path_slug => "memes", :owner_id => editor_user.id, :parent_directory_id => root_dir.id, :anyone_can_access => true, :visible_to_all => false, :anyone_can_edit => true})) # Set up a directory
    document_cset2 = Koodeksi.DB.Document.new_document(%{:title => "Volume sliders", :path_slug => "volume_sliders", :parent_directory_id => meme_dir.id, :owner_id => standard_user.id, :visible_to_all => false, :anyone_can_access => false})
    {:ok, doc2} = Repo.insert(document_cset2)
    :ok
  end

  test "directory listings appear to be of a correct form" do
    user1_standard_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Test")
    user2_editor_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Editor")
    admin_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Admin")

    root_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: is_nil(d.parent_directory_id))
    python_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: d.path_slug == "python")
    ruby_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: d.path_slug == "ruby")
    IO.puts("User1 on root. Inspect this result manually!")
    IO.puts(inspect Poison.encode!(Koodeksi.Operations.DirDocument.list_json_directory_index(root_dir_id, user1_standard_id)))
    IO.puts("Admin on root. Inspect this result manually!")
    IO.puts(inspect Poison.encode!(Koodeksi.Operations.DirDocument.list_json_directory_index(root_dir_id, admin_id)))
    IO.puts("Guest on root. Inspect this result manually!")
    IO.puts(inspect Poison.encode!(Koodeksi.Operations.DirDocument.list_json_directory_index(root_dir_id, nil)))
    # Check that the Python directory results are of the correct form
    %{"result" => "success", "list" => list} = Koodeksi.Operations.DirDocument.list_json_directory_index(python_dir_id, user1_standard_id)
    # Check that each item of the list has the appropriate structure
    assert Enum.all?(list, fn item ->
      keylist = Map.keys(item)
      assert Enum.all?(["title", "id", "type", "path_slug", "invisible", "permissions", "owner_id", "owner_name"], fn x -> Enum.member?(keylist, x) && item[x] != nil end)
      # Test permissions
      assert Enum.all?(item["permissions"], fn permission ->
        assert Enum.all?(["type", "admin_override"], fn x -> Enum.member?(Map.keys(permission), x) && permission[x] != nil end)
      end)

    end)
    # Check separately that both a document and a directory exists, to assure both are listed
    assert Enum.any?(list, fn item -> item["type"] == "directory" end)
    assert Enum.any?(list, fn item -> item["type"] == "document" end)
    # Check the Ruby directory returns a denied result.
    %{"result" => "denied"} = Koodeksi.Operations.DirDocument.list_json_directory_index(ruby_dir_id, user1_standard_id)
    # Check that an invalid ID gives out a notfound
    %{"result" => "notfound"} = Koodeksi.Operations.DirDocument.list_json_directory_index(0, user1_standard_id)
  end

  test "directory permissions work properly" do
    user1_standard_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Test")
    user2_editor_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Editor")
    admin_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Admin")

    python_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: d.path_slug == "python")
    # User 1 is a directory owner; all permissions are granted
    {:allowed} = DirDocument.user_directory_privileges(python_dir_id, user1_standard_id, [:list, :view, :edit, :manage])
    # User 2 has editing permissions for a document; no edit or manage
    {:denied, lst} = DirDocument.user_directory_privileges(python_dir_id, user2_editor_id, [:list, :view, :edit, :manage])
    assert Enum.all?([:edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 2
    # Admin's situation is the same; public editing but no management
    {:admin_allowed, lst} = DirDocument.user_directory_privileges(python_dir_id, admin_id, [:list, :view, :edit, :manage])
    assert Enum.all?([:edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 2

    ruby_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: d.path_slug == "ruby")
    # Due to having no public access, user 1 has no permissions whatsoever
    {:denied, lst} = DirDocument.user_directory_privileges(ruby_dir_id, user1_standard_id, [:list, :view, :edit, :manage])
    assert Enum.all?([:list, :view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 4
    # User 2 is a directory owner; all permissions are granted
    {:allowed} = DirDocument.user_directory_privileges(ruby_dir_id, user2_editor_id, [:list, :view, :edit, :manage])
    # Admin has no access either; refuse all
    {:admin_allowed, lst} = DirDocument.user_directory_privileges(ruby_dir_id, admin_id, [:list, :view, :edit, :manage])
    assert Enum.all?([:list, :view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 4

    meme_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: d.path_slug == "memes")
    # This is an interesting corner case.
    # User 1 owns a document, and therefore is only missing a management permission
    {:denied, lst} = DirDocument.user_directory_privileges(meme_dir_id, user1_standard_id, [:list, :view, :edit, :manage])
    assert Enum.all?([:manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 1
    # User 2 is a directory owner; all permissions are granted
    {:allowed} = DirDocument.user_directory_privileges(meme_dir_id, user2_editor_id, [:list, :view, :edit, :manage])
    # Admin has an unusual property; right to view and edit due to public access, but no right to have the directory listed in a parent directory!
    {:admin_allowed, lst} = DirDocument.user_directory_privileges(meme_dir_id, admin_id, [:list, :view, :edit, :manage])
    assert Enum.all?([:list, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 2
  end

  test "document permissions work properly" do
    user1_standard_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Test")
    user2_editor_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Editor")
    admin_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Admin")
    doc_bp_python = Repo.one(from d in Koodeksi.DB.Document, where: d.title == "Best practices Python")

    # Check that user1 gets all privileges as an owner
    {:allowed} = DirDocument.user_document_privileges(doc_bp_python.id, user1_standard_id, [:list, :weak_view, :strong_view, :edit, :manage])
    # User2 should get a weak view, not even a list
    {:allowed} = DirDocument.user_document_privileges(doc_bp_python.id, user2_editor_id, [:weak_view])
    # Same for an ordinary admin user
    {:allowed} = DirDocument.user_document_privileges(doc_bp_python.id, admin_id, [:weak_view])
    {:denied, lst} = DirDocument.user_document_privileges(doc_bp_python.id, user2_editor_id, [:list, :weak_view, :strong_view, :edit, :manage])
    assert Enum.all?([:list, :strong_view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 4
    # Admin user should be granted access with a caveat
    {:admin_allowed, lst} = DirDocument.user_document_privileges(doc_bp_python.id, admin_id, [:list, :weak_view, :strong_view, :edit, :manage])
    assert Enum.all?([:list, :strong_view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 4

    doc_bp_python2 = Repo.one(from d in Koodeksi.DB.Document, where: d.title == "Best practices Python II")
    # User 1 the specified owner
    {:allowed} = DirDocument.user_document_privileges(doc_bp_python2.id, user1_standard_id, [:list, :weak_view, :strong_view, :edit, :manage])
    # User2 should get edit access; that is, everything except management permissions
    {:denied, [:manage]} = DirDocument.user_document_privileges(doc_bp_python2.id, user2_editor_id, [:list, :weak_view, :strong_view, :edit, :manage])
    # Admin with no other relation should not be granted anything else than listing and viewing, as specified
    {:admin_allowed, lst} = DirDocument.user_document_privileges(doc_bp_python2.id, admin_id, [:list, :weak_view, :strong_view, :edit, :manage])
    assert Enum.all?([:strong_view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 3

    doc_ruby = Repo.one(from d in Koodeksi.DB.Document, where: d.title == "Best practices Ruby")
    # User 1 has no relationship; solely the document being marked as visible does not entitle the user to see it
    {:denied, lst} = DirDocument.user_document_privileges(doc_ruby.id, user1_standard_id, [:list, :weak_view, :strong_view, :edit, :manage])
    assert Enum.all?([:list, :weak_view, :strong_view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 5
    # User 2 has all privileges as the owner
    {:allowed} = DirDocument.user_document_privileges(doc_ruby.id, user2_editor_id, [:list, :weak_view, :strong_view, :edit, :manage])
    # Admin has no relationship, and therefore would otherwise also be refused all permissions
    {:admin_allowed, lst} = DirDocument.user_document_privileges(doc_ruby.id, admin_id, [:list, :weak_view, :strong_view, :edit, :manage])
    assert Enum.all?([:list, :weak_view, :strong_view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 5

    doc_sliders = Repo.one(from d in Koodeksi.DB.Document, where: d.title == "Volume sliders")
    # User 2 has no relationship - nor is public access or viewing allowed
    {:denied, lst} = DirDocument.user_document_privileges(doc_sliders.id, user2_editor_id, [:list, :weak_view, :strong_view, :edit, :manage])
    assert Enum.all?([:list, :weak_view, :strong_view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 5
    # User 1 has all privileges as the owner
    {:allowed} = DirDocument.user_document_privileges(doc_sliders.id, user1_standard_id, [:list, :weak_view, :strong_view, :edit, :manage])
    # Admin has no relationship, and therefore would otherwise also be refused all permissions
    {:admin_allowed, lst} = DirDocument.user_document_privileges(doc_sliders.id, admin_id, [:list, :weak_view, :strong_view, :edit, :manage])
    assert Enum.all?([:list, :weak_view, :strong_view, :edit, :manage], fn missing -> Enum.member?(lst, missing) end)
    assert length(lst) == 5
  end

  test "interests are properly determined" do
    root_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: is_nil(d.parent_directory_id))
    user1_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Test")
    user2_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Editor")

    # User 1 has ownership of the root directory, and therefore has such interest
    {:dir_owner} = DirDocument.user_interest_in_directory(user1_id, root_dir_id)
    # User 2 has the direct ownership of a subdirectory
    {:content_owner} = DirDocument.user_interest_in_directory(user2_id, root_dir_id)

    # Retrieve the ID for the python dir
    python_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: d.path_slug == "python" and d.parent_directory_id == ^root_dir_id)

    # User 1 is the owner of the Python directory
    {:dir_owner} = DirDocument.user_interest_in_directory(user1_id, python_dir_id)
    # User 2 has an editing interest
    {:contents_interest} = DirDocument.user_interest_in_directory(user2_id, python_dir_id)

    # Retrieve the ID for the ruby dir
    ruby_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: d.path_slug == "ruby" and d.parent_directory_id == ^root_dir_id)

    # User 1 has no interest in any way
    {:none} = DirDocument.user_interest_in_directory(user1_id, ruby_dir_id)
    # User 2 owns the directory and document
    {:dir_owner} = DirDocument.user_interest_in_directory(user2_id, ruby_dir_id)

    # Retrieve the ID for the meme dir
    meme_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: d.path_slug == "memes" and d.parent_directory_id == ^root_dir_id)

    # User 1 owns a document immediately below
    {:content_owner} = DirDocument.user_interest_in_directory(user1_id, meme_dir_id)
    # User 2 owns the directory
    {:dir_owner} = DirDocument.user_interest_in_directory(user2_id, meme_dir_id)
  end

  test "baseline operations work properly" do
    root_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: is_nil(d.parent_directory_id))
    {:invalid} = DirDocument.resolve_path("älaösÄÄ", nil) # This should be invalid
    {:invalid} = DirDocument.resolve_path("nonexistent", root_dir_id) # Nonexistent
    {:directory, found_dir_id, "manage", []} = DirDocument.resolve_path("$manage", root_dir_id) # Management in root directory, no prior slugs
    assert root_dir_id == found_dir_id
    # Retrieve the Python directory
    {:directory, python_dir_id, "view", [%{"title" => "Python", "slug" => "python", "is_directory" => true, "command" => "view"}]} = DirDocument.resolve_path("python/$view", root_dir_id)
    {:document, found_doc_id, found_dir_id, "permissions",  [%{"is_directory" => true, "slug" => "python", "title" => "Python"}, %{"command" => "permissions", "is_directory" => false, "slug" => "best_practices", "title" => "Best practices Python"}]} = DirDocument.resolve_path( "python/best_practices$permissions", root_dir_id)
    assert python_dir_id == found_dir_id # This document was placed in the root directory
    doc1 = Repo.get!(Koodeksi.DB.Document, found_doc_id)
    assert doc1.parent_directory_id == python_dir_id
    assert doc1.path_slug == "best_practices"
  end

  test "trampoline functionality seems to work" do
    root_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: is_nil(d.parent_directory_id))
    user1_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Test")
    # Let's test the Python directory with user 1
    %{"result" => "dir_list", "slugs" => [%{"slug" => "python", "title" => "Python"}], "to_preload" => preload} = DirDocument.generate_trampoline_preload("python/$list", user1_id, root_dir_id)
    # Ensure that the preload contains something that factually resembles a directory structure
    assert is_bitstring(preload["title"]) # Check that the title is provided
    assert Enum.all?(preload["this_dir_privileges"], fn permission ->
      assert Enum.all?(["type", "admin_override"], fn x -> Enum.member?(Map.keys(permission), x) && permission[x] != nil end)
    end)
    assert Enum.all?(preload["list"], fn item -> # Check for list contents
      keylist = Map.keys(item)
      assert Enum.all?(["title", "id", "type", "path_slug", "invisible", "permissions", "owner_id", "owner_name"], fn x -> Enum.member?(keylist, x) && item[x] != nil end)
      # Test permissions
      assert Enum.all?(item["permissions"], fn permission ->
        assert Enum.all?(["type", "admin_override"], fn x -> Enum.member?(Map.keys(permission), x) && permission[x] != nil end)
      end)

    end)
  end
end