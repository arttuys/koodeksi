defmodule Koodeksi.Operations.DirectoryTest do
  @moduledoc """
      This module tests various directory-specific operations, like new_directory_json/4 for web UI actions
  """

  use Koodeksi.DataCase
  alias Koodeksi.Operations.User # Pull in User
  alias Koodeksi.Operations.Directory

  setup do
    {:ok, standard_user} = Repo.insert(Koodeksi.DB.User.signup(%{:username => "Test", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}))
    {:ok, unprivileged_user} = Repo.insert(Koodeksi.DB.User.signup(%{:username => "Test2", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}))

    {:ok, root_dir} = Repo.insert(Koodeksi.DB.Directory.new_directory(%{:title => "Root", :path_slug => nil, :owner_id => standard_user.id, :parent_directory_id => nil, :anyone_can_access => true, :visible_to_all => true, :anyone_can_edit => false})) # Set up a directory, with edit permissions only to the owner

    :ok
  end

  test "new directory-operation works properly" do
    user1_standard_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Test")
    user2_unpriv_id = Repo.one(from u in Koodeksi.DB.User, select: u.id, where: u.username == "Test2")
    root_dir_id = Repo.one(from d in Koodeksi.DB.Directory, select: d.id, where: is_nil(d.parent_directory_id))

    # Attempt certainly invalid calls
    # Syntax: new_directory_json(root_id, user_id, title, slug)

    # No valid root directory
    %{"error" => _} = Directory.new_directory_json(nil, user1_standard_id, "Pseudo-Valid", "root_or_is_it")

    # Otherwise valid but no valid slugs
    %{"error" => _} = Directory.new_directory_json(root_dir_id, user1_standard_id, "Fold1", "")
    %{"error" => _} = Directory.new_directory_json(root_dir_id, user1_standard_id, "Fold2", nil)

    # Unprivileged user may not edit
    %{"error" => _} = Directory.new_directory_json(root_dir_id, user2_unpriv_id, "Otherwise Valid", "but_no_perms")

    # Valid
    %{"ok" => true} = Directory.new_directory_json(root_dir_id, user1_standard_id, "Valid", "all_ok")
    # .. but not on the second try!
    %{"error" => _} = Directory.new_directory_json(root_dir_id, user1_standard_id, "Valid", "all_ok")
  end
end