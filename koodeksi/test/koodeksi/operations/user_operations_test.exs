defmodule Koodeksi.Operations.UserTest do
  @moduledoc """
    This module tests if baseline user operations seem to work as expected in the base cases
"""
  use Koodeksi.DataCase
  alias Koodeksi.Operations.User # Pull in User

  @valid_user %{:username => "Test", :password => "ThisIZ2AStrongPassword!!!", :email => "test@user.invalid", :inactive => false, :admin => false}
  @valid_user2 %{:username => "Test2", :password => "ThisIZ2AStrongPassword!!!", :email => "test2@user.invalid", :inactive => false, :admin => false}
  @valid_admin %{:username => "Admin", :password => "KeepAdminsPasswordSafe1!!!", :email => "admin@user.invalid", :inactive => true, :admin => true}
  @valid_admin2 %{:username => "Admin2", :password => "KeepAdminsPasswordSafe1!!!", :email => "admin2@user.invalid", :inactive => true, :admin => true}
  @valid_active_admin %{:username => "AAdmin", :password => "KeepAdminsPasswordSafe1!!!", :email => "admin3@user.invalid", :inactive => false, :admin => true}
  @valid_active_admin2 %{:username => "AAdmin2", :password => "KeepAdminsPasswordSafe1!!!", :email => "admin3@user.invalid", :inactive => false, :admin => true}
  test "signup seems to work and fail with basic cases; also basic admin comparisions work" do
    {:ok, user} = User.signup_full(@valid_admin)
    assert user.username == "Admin"
    date_cmp = NaiveDateTime.compare(user.administrative_user_date, NaiveDateTime.utc_now())
    assert date_cmp == :lt

    assert user.inactive == true
    admin_id = user.id
    Process.sleep(500)
    {:ok, user} = User.signup_full(@valid_user)
    assert user.inactive == false
    assert user.administrative_user_date == nil
    user_id = user.id
    {:error, _} = User.signup_full(%{})
    assert User.compare_admin_dates(admin_id, user_id) == false # Admins cannot be targeted by users, obviously
    assert User.compare_admin_dates(user_id, admin_id) == false # Inactivity prevents privileges from being exerted
  end

  test "basic admin date comparision cases work" do
    {:ok, inactive_admin} = User.signup_full(@valid_admin)
    Process.sleep(500)
    {:ok, user} = User.signup_full(@valid_active_admin)
    admin_id = user.id
    Process.sleep(500)
    {:ok, user2} = User.signup_full(@valid_active_admin2)
    admin_id2 = user2.id
    Process.sleep(500)
    # Particularly observe that the second admin is newer than the first one, and therefore inferior in a way
    {:ok, user3} = User.signup_full(@valid_user)
    user_id = user3.id
    assert User.compare_admin_dates(nil, nil) == false
    assert User.compare_admin_dates(admin_id, inactive_admin.id) == false # If the senior admin is inactive, cannot utilize their privileges
    assert User.compare_admin_dates(admin_id, admin_id2) == false # Senior admin cannot be overrode by a junior one
    assert User.compare_admin_dates(admin_id2, admin_id) == true # But a junior admin can be overrode by a senior one
  end

  test "safe signup does not create admins unless in superuser mode" do
    {:error, _} = User.signup_safe(@valid_admin, false) # No disclaimer agreed to, no pass
    {:ok, user} = User.signup_safe(@valid_admin |> Map.put(:disclaimer_agree, true), false) # Agree to a disclaimer and it should work..
    assert user.inactive == false # .. but not issue inactivity despite being specified above
    assert user.administrative_user_date == nil # .. and certainly not admin privileges
    # Let's delete that user
    Repo.delete(user)
    # And try again with superuser privileges
    {:ok, user} = User.signup_safe(@valid_admin, true)
    datecmp = NaiveDateTime.compare(user.administrative_user_date, NaiveDateTime.utc_now())
    assert datecmp == :lt || datecmp == :eq
    assert user.inactive == true # Is inactive
  end

  test "credential check works predictably" do
    {:ok, _} = User.signup_full(@valid_admin)
    {:ok, active_user} = User.signup_full(@valid_user)
    # This should trigger an error
    {:error, _} = User.check_credentials(nil, nil, nil)
    # And an obviously bad username should as well
    {:error, _} = User.check_credentials("doesnotexist", "root", false)
    # Check that active users work on both settings
    {:ok, uid} = User.check_credentials("Test", "ThisIZ2AStrongPassword!!!", false)
    {:ok, uid2} = User.check_credentials("Test", "ThisIZ2AStrongPassword!!!", true)
    assert uid == uid2
    assert active_user.id == uid
    # Check that inactive ones fail when not permitted
    {:ok, _} = User.check_credentials("Admin", "KeepAdminsPasswordSafe1!!!", true)
    {:error, _} = User.check_credentials("Admin", "KeepAdminsPasswordSafe1!!!", false)
    # Should fail without a valid password
    {:error, _} = User.check_credentials("Test", "letmein", false)
  end

  test "user page preloads contain appropriate information" do
    start_date = NaiveDateTime.utc_now() # Compare other dates to
    {:ok, inactive_seniorest_admin} = User.signup_full(@valid_admin) # Order matters, the inactive admin is registered first, and therefore is more senior
    Process.sleep(500) # These sleeps ensure that admins do have truly distinct dates; it is possible otherwise that admins are created in such close proximity that their effective dates are the same
    {:ok, active_senior_admin} = User.signup_full(@valid_active_admin)
    Process.sleep(500)
    {:ok, active_junior_admin} = User.signup_full(@valid_active_admin2)
    Process.sleep(500)
    {:ok, user} = User.signup_full(@valid_user)
    end_date = NaiveDateTime.utc_now()

    # Test a base case first, user viewing their own data
    user_json = User.generate_user_page_json_preload(user.id, user.id)
    assert user_json["result"] == "success"
    assert user_json["admin_status"] == false
    assert user_json["can_edit"] == true
    assert user_json["email"] == "test@user.invalid"
    assert user_json["username"] == "Test"
    assert !Map.has_key?(user_json, "administrative_date")
    assert !Map.has_key?(user_json, "inactive")

    # Another base case, guest user
    guest_user_json = User.generate_user_page_json_preload(user.id, nil)
    assert guest_user_json["result"] == "success"
    assert guest_user_json["admin_status"] == false
    assert guest_user_json["can_edit"] == false
    assert guest_user_json["email"] == "test@user.invalid"
    assert guest_user_json["username"] == "Test"
    assert !Map.has_key?(user_json, "administrative_date")
    assert !Map.has_key?(user_json, "inactive")

    # Inactive admins can view but gain no privileges
    inactive_json = User.generate_user_page_json_preload(active_junior_admin.id, inactive_seniorest_admin.id)
    assert inactive_json["admin_status"] == false
    assert inactive_json["can_edit"] == false
    assert !Map.has_key?(inactive_json, "administrative_date")
    assert !Map.has_key?(inactive_json, "inactive")

    # Same applies for active admins but wrongly ordered
    unpriv_admin_json = User.generate_user_page_json_preload(active_senior_admin.id, active_junior_admin.id)
    assert unpriv_admin_json["admin_status"] == false
    assert unpriv_admin_json["can_edit"] == false
    assert !Map.has_key?(unpriv_admin_json, "administrative_date")
    assert !Map.has_key?(unpriv_admin_json, "inactive")

    # .. and for perfectly benign, normal users
    # Same applies for active admins but wrongly ordered
    user_json = User.generate_user_page_json_preload(active_senior_admin.id, user.id)
    assert user_json["admin_status"] == false
    assert user_json["can_edit"] == false
    assert !Map.has_key?(user_json, "administrative_date")
    assert !Map.has_key?(user_json, "inactive")

    # Now, if we are a senior admin, we can exert changes..
    admin_ovr_json = User.generate_user_page_json_preload(active_junior_admin.id, active_senior_admin.id)
    assert admin_ovr_json["admin_status"] == true
    assert admin_ovr_json["admin_status"] == true
    # Now that this has an administrative date, check if it falls into a proper range
    assert Map.has_key?(admin_ovr_json, "administrative_date")
    admin_date = admin_ovr_json["administrative_date"]
    assert NaiveDateTime.compare(start_date, admin_date) == :lt # Should be distinctly lesser
    assert NaiveDateTime.compare(end_date, admin_date) != :lt # Should be at least equal, or over
    assert admin_ovr_json["inactive"] == false # This should not be nil either, and in this case, equal false
  end
end