defmodule KoodeksiWeb.ErrorViewTest do
  use KoodeksiWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.html" do
    assert render_to_string(KoodeksiWeb.ErrorView, "404.html", []) =~
           "errors\/notfound"
  end

  test "render 500.html" do
    assert render_to_string(KoodeksiWeb.ErrorView, "500.html", []) =~
           "errors\/internalerr"
  end

  test "render any other" do
    assert render_to_string(KoodeksiWeb.ErrorView, "505.html", []) =~
             "errors\/internalerr"
  end
end
