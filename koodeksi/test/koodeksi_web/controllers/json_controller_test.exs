defmodule KoodeksiWeb.JSONControllerTest do
  use KoodeksiWeb.ConnCase

  test "No login means empty UID (GET /json/auth/user_blob", %{conn: conn} do
    conn = get conn, "/json/auth/user_blob"
    json = json_response(conn, 200)

    assert json["user"] == nil
  end

  test "Login and logout seem to work properly", %{conn: conn} do
    # Sign up a new user
    {:ok, user_struct} = Koodeksi.Operations.User.signup_full(%{username: "SignupTest", password: "StrongPassword1234", email: "testemail@test", inactive: false, admin: false})
    {:ok, _} = Koodeksi.Operations.User.signup_full(%{username: "SignupTest2", password: "StrongPassword1234", email: "testemail@test", inactive: true, admin: false})

    # One must observe that CSRF protection is disabled in tests! Therefore, we need not care about the CSRF tokens

    # Try an empty login; this should be incomplete
    conn = post conn, "/json/auth/login"
    rsp = json_response(conn, 200)
    assert rsp["error"] =~ "Incomplete" # Check for proper response

    # Attempt to log in using an invalid user
    conn = post conn, "/json/auth/login", %{"username" => "invalid", "password" => "invalid"}
    rsp = json_response(conn, 200)

    assert rsp["error"] =~ "Invalid" # Check for invalidness

    # Try to log on using an inactive user
    conn = post conn, "/json/auth/login", %{"username" => "SignupTest2", "password" => "StrongPassword1234"}
    rsp = json_response(conn, 200)

    assert rsp["error"] =~ "not active" # Check for invalidness

    # Try to log in with a valid user
    conn = post conn, "/json/auth/login", %{"username" => "SignupTest", "password" => "StrongPassword1234"}
    rsp = json_response(conn, 200)

    # Assert that there is a response
    login_blob = rsp["ok"]
    assert login_blob["uid"] == user_struct.id
    assert login_blob["username"] == "SignupTest"
    assert login_blob["admin"] == false

    ## Check the cookie as well, by checking the returned user blob
    conn = get conn, "/json/auth/user_blob"
    json = json_response(conn, 200)

    assert json["user"] != nil
    user_blob = json["user"]
    assert user_blob["username"] == "SignupTest"
    assert user_blob["uid"] == user_struct.id
    assert user_blob["admin"] == false

    # Try logging out
    conn = post conn, "/json/auth/logout"
    _ = json_response(conn, 200) # Do not care about the specific output

    # Check again, there should be nothing as we erased the cookie on logout
    conn = get conn, "/json/auth/user_blob"
    json = json_response(conn, 200)

    assert json["user"] == nil
  end
end
