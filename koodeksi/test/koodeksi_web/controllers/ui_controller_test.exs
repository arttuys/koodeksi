defmodule KoodeksiWeb.UIControllerTest do
  use KoodeksiWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "koodeksi-app.js"
  end
end
