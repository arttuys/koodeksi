date(26,2,2018)
duration(2,29)

synopsis("Work towards content view scaffolding, path trampoline design")

describe("It has been decided that the dynamic content paths are resolved serverside; the client's job is to request a specific preload structure from the server, and then appropriately navigate to the correct subpage from a so-called 'path trampoline', where all queries will first land to. The API endpoint for such inquiries has been defined; currently under process is the directory listing command.")
