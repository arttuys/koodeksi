date(23,11,2017)
duration(1,0)

synopsis("Developing UI scaffolding further")

describe("Work has started on developing the UI scaffold. Currently being worked on is the kd-root component, which is designated to be the root of the page. It also has been decided that the UI color scheme shall be white and blue; blue for primary elements, and white background to make it perhaps slightly more approachable in its look. This has also been reflected in the loading screen.")
