date(17,4,2018)
duration(3,6)

synopsis("Document page planning II, more precisely defined data models, PluginGateway scaffolding")

describe("The data structures for document pages are now more rigorously and unambiguously defined, making development work easier in the nearby future. There is also now a new GenServer module, which shall be used as a plugin gateway; plugins, as per specifications, are not run under the scope of web services, and therefore should have their own subprocess (even though plugins, as per specification, do not retain state across calls - but their supervisor may do so!). There's also a few documentary improvements in the JSON routing module")
