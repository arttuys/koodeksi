date(21,11,2017)
duration(2,15)

synopsis("Time tracker - preliminary version")

describe("As manually calculating sums and ordering time tracking records is painful, this commit introduces the Tick-Tock Time Tracker. It is a simple Ruby script, which generates pretty \\LaTeX printouts of steps. This allows the time tracking to be off-sync from actual commits, and easy calculation of relevant statistics.")