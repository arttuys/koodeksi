date(9,5,2018)
duration(3,22)

synopsis("Simplification over flexibility, HTML plugin scaffolding")

describe("It turned out to be a poor idea to allow overt flexibility in scaffolding, so the specification was made more rigid which should ease implementation. There is now also some primitive functionality for the HTML plugin and plugin gateway, although nowhere near functional yet.")
