date(12,1,2018)
duration(3,19)

synopsis("Substantial style refactoring and forms scaffolding")

describe("SCSS styles have now been restructured to better take advantage of the ability to have cross-file references. There is also now some more form scaffolding, mostly in terms of grid styles to automatically layout the form to a suitable shape, and specific CSS classes to style buttons appropriately.")
