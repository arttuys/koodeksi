date(16,1,2018)
duration(3,5)

synopsis("More form scaffolding, preciser definitions")

describe("A more substantial part of this session was taken by thinking process, and as a result, it has been decided that forms should be mostly Vuex- and formbox-based; this way, it is easy to genericize form functionality to work with CSRF, dynamic user auth updates, possible live update interactions and so forth. As of more particular changes, signin now builds buttons from a Vuex state structure, and the construction of the form view structures is now more precisely defined, although implementation is significantly incomplete as of yet.")
