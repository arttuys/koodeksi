date(18,12,2017)
duration(3,9)

synopsis("Work towards a working page scaffold, assorted UI changes")

describe("For a change, some focus has been shifted towards the UI, and a variety of changes have been made. The menu bar now supports a basic breadcrumb navigator, which shows links from the centralized Vuex state repository. Ideally, most of the UI state should be stored in Vuex, and components are to observe the parts relevant for their visual appearance. Later on, VueRouter will be used in conjunction, altering the state repository in tandem with navigation.")
