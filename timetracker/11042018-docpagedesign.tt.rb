date(11,4,2018)
duration(3,00)

synopsis("Document page design detailing, improved documentation on content paths and their function")

describe("Document view root contains now approximate plans on how document pages ought to operate. Now, there is a much more clearer idea on what components and what functionality is required on the clientside; approximate serverside functionality is also described, although at the level of providing proper endpoints for calls. Documentation has also been improved on the topic of content paths, which have slightly deviant behavior from most routes used by the UI.")
