date(6,1,2018)
duration(2,12)

synopsis("Work towards session management")

describe("This commit works towards establishing a functional scaffold for user session management. The backend naturally needs to know if the requester is signed in, and as whom. Security is also being taken to account by implementing a specific server-side security key - a part of the session cookie that can be revoked at will from the serverside, to invalidate all existing copies of the session with that specific security key.")
