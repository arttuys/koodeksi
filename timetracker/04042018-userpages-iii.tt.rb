date(4,4,2018)
duration(4,11)

synopsis("User pages III")

describe("The now very-longwinding user page saga is at a point where we have a partially working page. No editing or full display capability yet, but the scaffolding is there for the viewing. Also, more tests for the user scaffolding, and a minor fix to the breadcrumb links, which did not before nicely handle empty links (no address).. this still results in a slightly uneven position, but this is something that needs a more careful look at later on")
