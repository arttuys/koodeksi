date(19,12,2017)
duration(3,33)

synopsis("Substantial Vue framework, working 404 and 403 pages")

describe("There is now a vue-router scaffold which works; it is possible to navigate between a very small set of pages (index/document view test, 404 and 403) using vue-router links, all without requiring a page refresh (and which would not work, as Phoenix routes are still to be updated!). Additional modifications have been made to the Vuex state, and code documentation has been improved to be up to the notch.")
