date(9,1,2018)
duration(3,27)

synopsis("Preloading and navigation scaffolding, style changes, progress to form component")

describe("There is now more scaffolding done for navigation and its relationship to preloading; navigation will now automatically reset view-specific data storage and destroy preloads if the loaded component is not the first one being loaded (as expected in a preload). There is now also work done towards creating a form component, which shall also utilize Vuex state storage to allow efficient component reuse. Several style changes have been done as well, e.g generalizing the error page's content box, which is now a generic centered content box.")
