date(27,2,2018)
duration(3,21)

synopsis("Directory listing preloads partially functional, hierachy resolver.")

describe("Essentially, directory listings can now be seen at the clientside, with appropriate permissions being used on retrieval. The functionality is now rather incomplete though, as there is no UI view yet - only a preload. In addition, a hierachy of slugs is now included, allowing the client to compose a breadcrumb list to the top of the screen. Rudimentary testing for listings has been implemented as well.\n\nIt must be noted that some time was spent with unexpected technical difficulties with the development environment, triaged to be related to a recent Linux kernel upgrade not playing well with nVidia GPU drivers.")
