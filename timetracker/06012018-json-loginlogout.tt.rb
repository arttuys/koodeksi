date(6,1,2018)
duration(4,6)

synopsis("Basic JSON login/logout, session management")

describe("There is now a JSON API for logging in and out, and for querying the apparently active user identifier. The session data is stored in a cookie, as we do not expect to interact with the website using anything else but a browser - and therefore, explicit tokens are unnecessary. It also adds some security as the user identity is not as easily exposed to scripts with the HTTPOnly flag on the cookie.\nBasic testing for the JSON API has been done, and it should behave as expected.")
