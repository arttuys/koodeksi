date(14,3,2018)
duration(1,54)

synopsis("Partially working directory addition functionality, improved database constraints")

describe("Due to special circumstances, this session is shorter than usual, and therefore the functionality is not quite complete; however, it is intended to implement the add folder-action for the directory listings. It is not yet bound to anything. In addition, database constraints have been improved to return better error messages in case of missing associations.")
