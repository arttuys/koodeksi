date(28,4,2018)
duration(4,56) # 1.5h accounts for the final report itself, written separately from the rest of this session on earlier dates.

synopsis("Various smaller patches, testing, scaffolding of final report")

describe("This commit mostly fixes several bugs; improperly applied soft reload in the directory listing, for one. Tests for new directories and documents have been strengthened. Also, as the project is nearing is conclusion, a scaffold for a final report has been written. It is an initial scaffold and will be eventually transformed into a LaTeX document.")
