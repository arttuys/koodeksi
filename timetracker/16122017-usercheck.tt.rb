date(16,12,2017)
duration(1,8)

synopsis("Primitive user account check, operation structure")

describe("Added a function that checks a given username-password combo for validity, and returns an ID if valid, or an error message (which is shown verbatim to the user) if it is not. All actions are also logged to the database. No automated checks yet. This commit also adds a new folder, 'operations', which shall contain an abstraction layer above the raw database access to make executing different operations (e.g add new user, save submission, create new document) smooth and easy.")
