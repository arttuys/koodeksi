date(21,5,2018)
duration(5,22) # Includes 45min from a nightly session some days back

synopsis("Serverside blocks can be load but not generated")

describe("A massive progress, there is now a functional serverside API for querying block loads; upon a suitable command, block data will be loaded from the database, and provided as an appropriately formed overlay. Next in line is basic client support - and IF there's enough time, more complex editing functionality. This session probably nearly broke records in terms of its length, and 8 ECTS credits are drawing to a close very soon.")
