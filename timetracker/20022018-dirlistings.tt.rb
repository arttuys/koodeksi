date(20,2,2018)
duration(3,13)

synopsis("Small regression bugfix, directory listings implemented")

describe("A careless pattern match would have led to a situation where a no-match would have led to an exception, in relation to security keys. This has now been fixed, and is now handled gracefully by denying improperly formed cookies. There is also now a working directory listing JSON structure generator, although the tests so far are very cursory and will be made more rigorous next.")
