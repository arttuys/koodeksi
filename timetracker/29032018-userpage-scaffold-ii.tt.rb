date(29,3,2018)
duration(3,11)

synopsis("Scaffolding towards user pages II, date fixup, minor documentation changes")

describe("By a chance, we didn't still get to working user pages today; this was caused by a subtle accident with NaiveDateTime, as I only now realized that one needs to use NaiveDateTime.compare. Basic equality comparisions seem that they may work, but they actually compare the structures, which in certain cases results in a scenario where the comparison returns a wrong result. This has now been fixed. By that matter, there's more tests on the user page preloads, although those are still quite incomplete and would not satisfy me just yet. In addition, documentation has been slightly improved to concisely describe OTP and its relation to Elixir and this program.")
