date(30,11,2017)
duration(1,47)

synopsis("User schema essentially complete")

describe("User schema should now be essentially complete, with changesets one would imagine needing having been implemented and automatically unit-tested to assure correct behavior")
