date(10,12,2017)
duration(0,46)

synopsis("Log schema implemented and tested, schemas assumed complete")

describe("There now exists a Log schema that can store references to various database elements, and (so far not defined) additional auxillary data in a JSON map. This commit also concludes schema implementation for now, as all tables defined in the migration are now implemented in Ecto, and unit-tested to assure that their defined properties hold. Some changes to the migration have also been made, as fields were mistakenly named out of convention.")
