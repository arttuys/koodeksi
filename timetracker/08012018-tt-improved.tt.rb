date(8,1,2018)
duration(0,6)

synopsis("Time tracker style improvement; typo correction on 'Preliminary DB schema'.")

describe("A minor readability change to the time tracking script, to ensure the important points stand out with their bold font. It also now shows the originating file names for records. There was also a typographical error on a single log record, which has now been corrected")
