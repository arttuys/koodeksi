date(8,1,2018) # 10 minutes on late 7.1, related to path slugs
duration(0,31)

synopsis("Log originator and sensitivity records, simplification of pathnames")

describe("Two new fields have been added: log origin and sensitivity. It has been decided that with certain constraints (as opposed to earlier plans), several Koodeksi servers can share a single database. It has not yet been firmly defined what kind of interplay will be allowed (at least user databases can be shared), but log records will certainly be keyed by their origin as to avoid showing spurious records for multiple separate servers. Sensitivity has also been added to log records, which allows certain records to be invisible even to those related to the records - only administrators can see sensitive records.\nIn addition, in interest of simplifying the logic required, it has been decided that now path slugs for directories and documents can be equal under the same subdirectory, as documents will be prefixed by some special character not usable in any path slugs. This has been reflected in the migration for the database.")
