date(20,11,2017)
duration(0,20)

synopsis("Preliminary DB schema")

describe("Drafted up a preliminary database schema. For now, the DB shall have users, folders, documents and blocks. Folders may contain subfolders and documents, and documents contain blocks. Users, folders and documents have access control properties. Blocks will contain plugin-specific data, to be provisioned to the plugin at the rendering step.")
