date(27,11,2017)
duration(4,23)

synopsis("First migration")

describe("First migration file is now complete, and can generate a reasonable database. This took quite long, as there were some unthought-of kinks to solve as Ecto had a slightly different data model I thought it had - which required creating a few new tables. Still missing are schemas (the Elixir components), and a seed file which will initialize the DB with reasonable defaults.")
