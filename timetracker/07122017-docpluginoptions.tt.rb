date(7,12,2017) # Between 6.12.2017 and 7.12.2017
duration(1,6)

synopsis("Document plugin option schema implemented and tested")

describe("There is now a DocumentPluginOptions schema, which is intended to store document-specific, per-plugin data. This will be later on be allowed to be bundled as part of plugin transactions, for, say, page-specific highscore listings or preview data for Markdown text headers. \n\nIn additional minor changes, small blurbs have been added to WittyQuotes (not counted on timekeeping logs due to the short time taken and relatively inconsequential nature), and some comment inaccuracies, test warnings about unused variables have been remedied")
