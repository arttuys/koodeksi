date(6,2,2018)
duration(6,0)

synopsis("Documentation refactoring")

describe("Due to the exceptional circumstances in relation of moving from an one apartment to another, this commit has not had any substantial code modifications. Instead, older disjoint documentation files have been phased out and a new combined LaTeX documentation file has been created instead. It also now includes substantial new content about the requirements and workings of the program.")
