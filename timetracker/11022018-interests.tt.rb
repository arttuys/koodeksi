date(11,2,2018)
duration(2,3)

synopsis("Permission structures continued")

describe("More work towards the permission system; there is now an ability to check if an user has interest in some directory, which is crucial in verifying that the user has or has not a right to list a directory.")
