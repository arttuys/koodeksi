date(9,4,2018)
duration(1,27)

synopsis("Regression and validation bugfix, user page viewing functional, credit mark check")

describe("At some point, changed data model caused the table list sorting to become broken on the directory listing pages. Also, it was found out that due to the corner cases of the data model and incomplete validation, it was possible to generate directories with null slugs from the listing pages. That has now been fixed. User page views are also functional.\n\nIt is now also the time when we have passed the 6op mark, which actually requires us now to deprioritize further refinements; at this point, we must move on to the document pages, so that a functional prototype of some nature exists by the time the 8op mark is hit. At least I don't find scaffolding only to be particularly useful.. and I'd like to try the Elixir GenServer structures someday")
