date(22,11,2017)
duration(0,10)

synopsis("Preliminary document UI sketch")

describe("There is now an approximate idea what the document view should look like. A menu bar on top with a breadcrumb leading to the current page, a settings menu, and a user menu. On the left, an index, and in the center, content. Any auxillary stuff like settings shall open in modal dialogs, instead of taking valuable screen estate.")
