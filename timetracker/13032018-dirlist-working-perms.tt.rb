date(13,3,2018)
duration(3,17)

synopsis("Permission display for listings, scaffolding, self privileges for directory listings, tooltips, observation about credits")

describe("Directory list now displays baseline permission entries, and there is also a base form for creating new documents and directories (implementation still pending). It was also realized that the directory list doesn't return applying permissions of the directory itself (doh!), so that was fixed. Basic tooltips have also been implemented, as to advise the user of the more precise definition for a given icon.\n\nIn other observations, we've already went over the 5 credit mark, which is 62.5 percent of the allotted work budget of 8 study credits. It seems that we may have to cut short the idea of executable code and instead focus on getting the scaffolding in working order - which is, reasonable user interfaces for all features, in particular the document and log interfaces, which are of high priority once directory listings and management are working right.")
