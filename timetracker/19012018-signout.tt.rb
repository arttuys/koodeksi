date(19,1,2018)
duration(2,57)

synopsis("Signout working, API simplification, work towards signup, assorted style tweaks")

describe("Signing out works now, completing the essentials of user state transition. Some formbox API elements have been simplified slightly due to the very small benefits gained by excess complexity. There is now a visual scaffold for a signup page; there's no functionality yet, although at the serverside there does exist a Operations function call which creates a new user. To support this, some SASS style tweaks have also been completed.")
