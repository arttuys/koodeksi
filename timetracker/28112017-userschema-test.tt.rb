date(28,11,2017)
duration(2,57)

synopsis("Refactoring of migration, user schema partially built and tested, licensing diary")

describe("User schema has now been defined, and there exists some tests for the validations. It is not quite complete yet though; only signup exists as a premade changeset now. Same applies for tests, as it only tests signup as of now, and even those tests do not yet entirely cover the numerous validations enforced by the model (e.g password strength, correct deposit of admin privileges, et al). ComeonIn has been added as a new dependency along with Bcrypt, used for password hashing - it concatenates the hash and the salt into a single string, necessitating a change in the migration.\n\nIf this were not sufficient already, there is also now an informal licensing diary, to keep track of dependencies and their licensing. This can be important later on, as the license I intend to license under (Affero GNU GPLv3 or later) has some special terms that can render compatibility problematic.")
