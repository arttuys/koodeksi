date(29,11,2017)
duration(0,20)

synopsis("Improved permissions to avoid excess DB load, document plugin options.")

describe("A sudden realization arose that the current permissions model could lead to undue database loads, as many types of permissions checks would require checking deep tree structures to assure permissions. This modification limits the depth of the checks, allowing scenarios like showing of inaccessible folders in listings and prohibiting on listing parent folders of folders you can access as a tradeoff for decreased DB load.\n\nThere was also a realization that plugins may need to store document-specific options - e.g for text blocks, the Markdown plugin may want to store a list of headings, to speed up building a chapter list. A new table has been created for that.")
