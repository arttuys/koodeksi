date(17,1,2018)
duration(4,32)

synopsis("Login functional, formbox essentially complete, scaffolding simplification and redesign")

describe("Users can now log on via the signin form, and it will be reflected in the UI. Signout is not yet functional, but can be assumed to be significantly less complicated than signin. Sign-in functionality operates via the formbox, which is designed to be utilizable in many other places, e.g signup, user profile editing, et cetera. There have also been numerous smaller changes varying from mutation design (for better convenience) to style (better looks), which are not listed explicitly here.")
