date(25,4,2018)
duration(3,9)

synopsis("Document pages component scaffolding, assorted minor bugfixes")

describe("Two main themes in this commit; one being that the document page is taking shape; at this point, we get as far as properly saving view paths to Vuex for eventual display. There's also a separate TOC component for which the appropriate data is extracted (but not yet shown). The second one is a variety of smaller bugfixes, varying from improperly applied Babel runtime, to directory listing permission icons not showing proper overrides, to anonymous components not loading properly.")
