date(23,1,2018)
duration(3,28)

synopsis("Signup functional")

describe("Signup is now essentially functional, allowing self-service account creation. So far, there is not much validation beyond basics (e.g email format is checked, but its ability to receive messages is not validated). For the current plans though, it is essentially assumed that the site will work on such a small-scale basis that such features are not the most urgent, and that manual administrative intervention can manage such cases.")
