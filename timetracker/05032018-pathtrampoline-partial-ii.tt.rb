date(5,3,2018)
duration(3,9 + 15) # Add 15min for last-minute fix

synopsis("Path trampoline partially functional, associated alterations, unnecessary permission check removed, improved network ban functionality")

describe("Path trampoline is now partially functional, and can retrieve data from a server preload; it cannot yet request a preload from a navigation, although there does exist an JSON API endpoint for that - so adding it should be easy. Many small changes were effected to allow proper functionality, e.g adding a special parameter to the standard error box to indicate it is loaded as a subcomponent, and therefore should behave as such. There was also an unnecessary permission check in the trampoline, as the underlying directory listing functionality does a similar check. \n\nAs of permissions, sessions now also enforce network bans by not recognizing any sessions (even if valid) if the connected user is network banned. This can save time, as this effectively forces the UI to treat the user as a guest, and not do failing requests.")
