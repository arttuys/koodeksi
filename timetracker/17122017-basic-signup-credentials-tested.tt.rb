date(17,12,2017)
duration(2,38)

synopsis("Basic signup and credential check operations with tests, changeset-to-human-readable-list function")

describe("There are now signup and check-credentials operations available; both either return an identifier or a structure in case of a successful operation, or a human-readable, safe (no exposed private information) error message upon failure; a separate transforming function has been implemented in the Common module. In truly exceptional cases, the operations may also throw an exception, which is intended to cause an internal error page to be shown. All of these functions have now also been unit-checked to assure proper operation.")
