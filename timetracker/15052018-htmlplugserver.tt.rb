date(15,5,2018)
duration(3,22)

synopsis("Baseline serverside HTML plugin w/ Markdown, resolved Brunch and Node issues, restructuring")

describe("There is now a (at least theoretically, testing pending) functional HTML plugin on the serverside, which ingests Markdown and returns HTML code. It also occured that the recent NPM update (due to it breaking apart on its own, which is an entirely different story), apparently also several Brunch-related dependencies broke, requiring substantial path refactoring and rebuilding of SASS for Node. The planned fixed position menu also turned out to be a trickier proposition than thought, so it has been for now switched to a CSS3 grid")
