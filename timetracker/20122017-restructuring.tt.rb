date(20,12,2017)
duration(0,55)

synopsis("Miscellanous restructuring work")

describe("Some restructuring has been done, to more clearly delineate the responsibilities for each file and the relations between the parts of the program. This also extends to the Phoenix part, where the main UI controller has been named to UIController.")
