# Tick-Tock Time Tracker
# An simple and efficient time tracker for devs

# Word of warning: ALL DATA is intepreted first as Ruby code, and string inputs as raw LaTeX document data. Be aware of these restrictions.
require 'date'
module Timetracker

  class RecordScope
    def initialize(filename)
      @filename = filename
    end

    def attempt_eval
      code = File.read(@filename)
      eval(code, binding)
    end

    ##

    def duration(hours, minutes=0, seconds=0)
      @hours = hours
      @minutes = minutes
    end

    def date(day, month, year)
      @day = day
      @month = month
      @year = year
    end

    def synopsis(description)
      @synopsis = description
    end

    def describe(description)
      @full_description = description
    end

    ##

    # Generate a simple hash filled with strings that describe the data
    def generate_hash
      raise "Please set a date for a record!" if @day == nil
      raise "Please set a duration for a record" if @hours == nil
      raise "Please set a synopsis for a record!" if @synopsis == nil

      date_object = Date.new(@year, @month, @day)
      time = "#{"%02d" % @hours}:#{"%02d" % @minutes}"

      hash = {}
      hash[:date] = "#{date_object.iso8601}"
      hash[:time] = time
      hash[:synopsis] = @synopsis
      hash[:raw_date] = date_object
      hash[:time_duration] = (@hours * 60) + @minutes
      hash[:description] = "\\textbf{On} \\underline{#{date_object.iso8601}}, \\textbf{change} '#{@synopsis}', \\textbf{duration} \\underline{#{Timetracker.format_duration(60*hash[:time_duration])}}, \\textbf{originating from file} \\underline{#{File.basename(@filename)}}\n\n#{@full_description}"
      return hash
    end
  end

  # Lifted from my old CodeWars answer to an exercise, and altered slightly
  def self.format_duration(seconds)
    orig_seconds = seconds
    return "none" if seconds == 0 #Special case.

    durations = {60 => "minute", 3600 => "hour", 86400 => "day", 31536000 => "year"}
    components = []

    durations.keys.sort {|x,y| y <=> x}.each do |unit|
      even_unit_durations = (seconds - (seconds % unit))
      next if even_unit_durations == 0

      seconds -= even_unit_durations

      units = even_unit_durations / unit
      components << "#{units} #{durations[unit]}#{units > 1 ? "s" : ""}"
    end

    ret_str = components[0]

    components[1..-1].each_with_index do |comp, ind|
      ret_str << (((ind+1) != (components.size-1)) ? ", #{comp}" : " and #{comp}")
    end

    return ret_str + " (#{((orig_seconds.to_f)/(60.to_f)/(60.to_f)/(27.to_f)).round(3)} credits)"
  end

  def self.file_to_record(filename)
    begin
      record = RecordScope.new(filename)
      record.attempt_eval
      return record.generate_hash
    rescue StandardError => e
      puts "Exception - invalid record #{filename}!"
      puts e.inspect
      return nil
    end
  end

  def self.generate_table(table)
    %Q(
\\begin{tabulary}{\\textwidth}{|C|C|C|}
\\hline
Date & Duration & Synopsis \\\\
\\hline
#{
table.map {|x| x[1]}.map {|data| "#{data[:date]} & #{data[:time]} & #{data[:synopsis]} \\\\\n\\hline"}.reduce(:+)
}
\\end{tabulary}
\\newpage
)
  end

  def self.generate_latex_output(list)
%Q(
\\documentclass[utf8,a4paper,12pt]{article}
\\usepackage{tgadventor}
\\usepackage[utf8]{inputenc}
\\usepackage[finnish]{babel}
\\usepackage{titlesec}
\\usepackage{csquotes}
\\usepackage{hyperref}
\\usepackage{listings}
\\usepackage{tabulary}
\\newenvironment{ttenv}{\\fontfamily{pcr}\\selectfont}{\\par}
\\titleformat{\\chapter}[display]{\\normalfont\\bfseries}{}{0pt}{\Large}

\\setlength{\\parskip}{0.5em}
\\begin{document}

\\title{Time Tracking Documentation}
\\author{Arttu Ylä-Sahra}
\\date{\\today}

\\maketitle
\\section{What's this?}

This document is an automatedly generated synopsis of time used for the Koodeksi project.

It is constructed using the \\emph{.tt.rb} files located in the same folder as in the program generating this file. For this file specifically, a total of #{list.length} records were read.

\\section{Tabulated records}
\\begin{center}
#{list.each_slice(12).map {|slice| "#{generate_table(slice)}\n"}.reduce(:+)}

Total time used was \\texttt{#{format_duration(60*list.map{|x| x[1][:time_duration]}.reduce(:+))}}. Average consumption of time for a commit is \\texttt{#{format_duration(60*list.map{|x| x[1][:time_duration]}.reduce(:+) / list.length)}}

\\section{Precise action records and descriptions}
\\begin{ttenv}
#{list.map {|x| "\n#{x[1][:description]}\n\\noindent\\rule{\\textwidth}{1pt}"}.reduce(:+)}
\\end{ttenv}
\\end{center}

\\end{document}

)
  end

  def self.main
    begin
      data = []

      Dir.foreach(".") do |path|
        next unless path.downcase.end_with?(".tt.rb")
        record = file_to_record(File.join(".", path))
        raise "Record may not be nil" if record == nil
        data << [path, record]
        # Sort records by date first, and then longest duration first.
      end

      data.sort! do |a,b|
        datecomp = a[1][:raw_date] <=> b[1][:raw_date]
        puts "#{a[1][:raw_date]} <=> #{b[1][:raw_date]} = #{datecomp}"
        if (datecomp == 0) then
          timecomp = b[1][:time_duration] <=> a[1][:time_duration]
          puts "#{b[1][:time_duration]} <=> #{a[1][:time_duration]} = #{timecomp}"
          timecomp
        else
          datecomp
        end
      end

      puts "Found #{data.length} valid records"
      puts "Writing LaTeX file to timekeeping.tex.."

      File.open("./timekeeping.tex", 'w') {|x| x.write(generate_latex_output(data))}
    rescue StandardError => e
      puts "Unable to build records - exiting!"
      puts e.inspect
      exit(1)
    end
  end

end

Timetracker.main
