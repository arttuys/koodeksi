date(22,11,2017)
duration(1,30)

synopsis("Loading screen, code reorganization and architectural planning, Witty Quotes")

describe("Some code reorganization has been done to set up a desirable model. The main layout will now supply all prerequisite JS modules, and a non-standard page will be an exception. It is also now decided that there will be Vue-router, which shall leverage Vuex to allow page transitions without full reloads. The scaffolding for that is not yet done, but there is now a loading screen which shows random quotes to keep the user entertained for the short while the user interface takes to load.")
