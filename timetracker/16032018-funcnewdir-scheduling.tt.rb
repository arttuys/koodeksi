date(16,3,2018)
duration(1,12)

synopsis("Functional newdir, schedule reconsiderations")

describe("This commit doesn't bring much more than a functional new directory-operation. In addition, a minor typo has been fixed in the documentation.\n\nAs of other considerations, it has been decided that since so much overtime has been done, it is safe to drop one session from regular weekly rotation as to grant more flexibility and evenly stagger work till the start of the summer. This change will take place effective immediately.")
