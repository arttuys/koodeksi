date(26,1,2018)
duration(6,13)

synopsis("Path resolver and permission scaffolding")

describe("Work on necessary scaffolding for the directory view; there is now a way to resolve an URL to point to a specific directory or document, and work in progress towards an easy-to-use function interface for verifying permissions needed for various actions. These are still incomplete in various ways; none of them have any substantial automated testing set up yet, and particularly the permissions checking is still not functional. There also have been various associated, smaller changes")
