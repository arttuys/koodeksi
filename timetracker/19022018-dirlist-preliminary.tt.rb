date(19,2,2018)
duration(2,18)

synopsis("Work towards the directory listing operation, bugfix on inactivity")

describe("There is now a preliminary plan on how to implement directory listings; quite like with the user blobs, directory listings will be preloaded where available, and a special JSON helper function will be created to help with this task. Format for those is also now defined. I also noticed an omission in the session validation, and changed it so that it now enforces that the users are active if a session is to be considered valid. Effectively, when an user gets inactivated, all sessions render themselves invalid.")
