date(4,12,2017) # Between 3.12.2017 and 4.12.2017
duration(0,49)

synopsis("Document edit perms implemented and tested, assorted small changes")

describe("Document edit permissions have now been added, both as their own separate schema and as references via User and Document schemas, for convenience.\n\nThis commit also includes a few other smaller changes; some Vue file changes for bugfixing and styling purposes (a demo effect), time tracker bugfix for better accuracy, and more witty quotes. None of these smaller changes are timed due to their relatively insignificant and spontaneous nature, and the very short period of time it took to do them.")
