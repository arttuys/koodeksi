date(10,4,2018)
duration(1,50)

synopsis("Code cleanup for structuring, validation improvements")

describe("Starting work towards document pages; the directory structure has been improved so that the pathnames make more sense for their contents. Plan is to first implement an UI scaffold (harder part) and then a serverside scaffold (easier). As of other changes, a new concept of reserved names has been added, probihiting use of potentially confusing names (e.g null, undefined, block, ..) in various contexts. This should perhaps slightly improve security as well, in the face of easily confusing implicit JS type coercion.")
