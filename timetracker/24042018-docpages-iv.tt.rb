date(24,4,2018)
duration(2,56)

synopsis("Document pages IV - primitive preload generator")

describe("There is now a function which can generate an essentially valid preload according to the form described in 'view\\_root.vue', although as no plugin scaffolding yet exists in any form but name, there is not much functionality yet. The next step would be implementing more rigorous automated tests, and basic functionality for the clientside which would receive the preload structure.")
