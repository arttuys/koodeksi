date(1,12,2017)
duration(4,16)

synopsis("Directory schema mostly implemented, mostly tested, assorted changes")

describe("There is now a directory schema available, containing appropriate relations to the schemas so far implemented. User schema has also been implemented with a shorthand to retrieve all directories an user may own. Directory schema has also been mostly tested to assure proper functionality.\n\nIn addition to these changes, some general refactoring and fixing work has been done: regexes have been corrected to correctly work with newlines (or not approve them), and signup/new directory have been changed to a form in which you can not inject your own structure (encouraging to use the single functions to elect changes)")
