date(9,12,2017)
duration(1,24)

synopsis("Submission schema implemented and tested")

describe("There is now a schema implementation for submissions; submissions being optionally storable, user-provided data blobs that are bound to a specific block. The implementation is fairly simple as the migration does not place many constraints on submissions - and this also led to relatively easy testing process.")
