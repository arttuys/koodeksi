date(16,2,2018)
duration(2,12)

synopsis("Document and directory permission checks mostly complete")

describe("There are now easy-to-use functions for determining what permissions an user does have; it is still under consideration how actual permission changes will be enacted. Most likely they are done transparently via property changes on directories and documents, with the UI advising on the consequences of actions - this would be quite logical, as permissions are now mostly calculated from document properties instead of explicit definitions")
