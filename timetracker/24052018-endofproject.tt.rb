date(24,5,2018)
duration(3,25) 

synopsis("Functional view-only documents, end of project")

describe("Documents can now have blocks that can be dynamically loaded and viewed. And unfortunately, that is the end of the credit limit - no more credits can be assigned from this project, so therefore work stops here for now. After this commit, some more work will be done for setting up a VM")
