date(8,12,2017)
duration(2,10)

synopsis("Block schema implemented and tested, bugfix for timetracker")

describe("The database schema for Block has been implemented and tested; a single block is a part of a document, and is ordered so that for all blocks assigned for a document, it is always assured that there is an unique, nonambiguous order for the blocks to be displayed. It also had occurred that the time tracker does not handle long tables nicely, and therefore a patch has been implemented to split tables into reasonably sized chunks. In addition, as a careful reading of Ecto documentation revealed, declaring database constraints on schema level (delete when foreign key is deleted, et al) should be preferably done via migrations if possible. As it has been done with migrations, a part of the schema was stricken.")
