date(3,12,2017) # Between 2.12.2017 and 3.12.2017
duration(2,42)

synopsis("Document schema implemented and mostly tested, StdRegexes module for shared functionality, assorted refactoring")

describe("There is now a Document schema available, and tests for it also have been implemented. Relevant foreign key references have also been added to the Directory and User schemas. Several tests now share regexes, which have been lifted to a separate StdRegexes module, to allow many modules to share the same logic.\n\nOther changes include a slight change in debug logging configuration, a change in the migration (again) to fix an issue with log tables (for some reason, one field name would not allow for a partial index, but another one allows).")
