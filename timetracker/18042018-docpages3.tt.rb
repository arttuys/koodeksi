date(18,4,2018)
duration(3,13)

synopsis("Document page preload scaffolding, style design")

describe("It has been decided that the best page to start working for document views is the serverside after all - and that decision is implemented by working on a preload scaffolding, to allow at least something to be returned to the path trampoline, which can then load the view page with it. Later on, it will be extended to add actual schema generation functionality. Styles have also been worked with, as the document views have their own two-part design instead of the common one-box style.")
