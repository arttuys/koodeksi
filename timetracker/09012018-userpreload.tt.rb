date(9,1,2018)
duration(3,18)

synopsis("Preloading user data functional, corner-case reduction, variety of small fixes")

describe("Server can now preload user login data to the client, allowing immediate showing of the pertinent status. Security key corner-case has been removed, and serverside code can now assume that an UID assign in a request means that the user has a valid session cookie. There's also a variety of other fixes, ranging from NaiveDateTime test corner-cases (tests fail if actions take place too fast, even if they are perfectly correct) to style changes in the main layout and improved comments")
