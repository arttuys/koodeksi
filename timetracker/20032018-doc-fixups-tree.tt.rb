date(20,3,2018)
duration(3,22)

synopsis("Documentation hierachy trees")

describe("Due to personal circumstances, there wasn't much mood for a complex code design. Instead, scheduled time was spent on a less complicated, but nevertheless time-consuming and still very useful documentation work. There now exist several annotated hierachy trees of various important files and directories, intended to be an useful at-glance insight tool for the structure of the program.")
