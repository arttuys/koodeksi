date(7,3,2018)
duration(3,9)

synopsis("Breadcrumbs for content items now functional, various related and unrelated fixes and changes")

describe("It took surprisingly long to get the breadcrumb menus for the path trampoline-handled pages to work properly; it entailed a variety of both server- and clientside design changes all the way up to the DirDocument module. This naturally also entailed documentation rewriting to reflect these changes properly. Now, however, the main path trampoline component will correctly calculate and assign the proper breadcrumbs according to the data returned by the server.\n\nThe previous section encompassed the first session. The second session reflected by this commit is mostly smaller changes; badly worded descriptions and ambiguous functions being documented better, and a security logging function for directory listings (all rejected requests are logged into the 'logs' table; they are not considered sensitive per se, as they simply indicate that some user X was refused listing access to some directory)")
