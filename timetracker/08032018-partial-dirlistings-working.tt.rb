date(8,3,2018)
duration(2,0)

synopsis("Partial directory listings")

describe("Although a bit crude and unfinished (it doesn't look terribly pretty..), directory listings are now functional in a sense that you can browse directories and see their owners. Action links have not yet been completed, but should be easy to add, with the help of FontAwesome. I also noticed the silly omission of the listing's title (doh!), so that was added to the JSON index function, along with an admin override indicator (if this listing is a special concession).")
