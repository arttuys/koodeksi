date(23,2,2018)
duration(2,51)

synopsis("Directory listings more rigorously tested, strengthened validation")

describe("Directory listings have been more rigorously tested now; also, upon noticing a validation omission leading to a potential Elixir thread crash (which would, however, be automatically restarted, with fairly little ill effect), many functions have been went through and their validation routines strengthened to assure a definite response instead of a cut connection (when the thread goes down due to an exception) in case of bad input.")
