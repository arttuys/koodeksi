date(1,3,2018)
duration(3,24)

synopsis("Tables can sort now, improved error messages and soft IP ban support")

describe("The planned sorting functionality for table lists work now; it is possible to specify a means to sort some given column. In addition, due to part boredom and part realization of possible future use cases, a 'soft' IP ban functionality has been implemented. \n\nEssentially, Koodeksi will respond to the user, but will not allow any complicated operations to take place or access to most of the user interface. This is implemented as a special error page, plus a Phoenix plug forcefully terminating and redirecting requests if the user is determined to be banned. There is also a separate Service Unavailable message, that is planned to be used for service breaks or other predictable events.")
