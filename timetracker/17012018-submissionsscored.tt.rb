date(17,1,2018)
duration(0,14)

synopsis("Score field added to submissions")

describe("Originally, it was thought that scores could be stored in DocumentPluginOptions. However, in more detailed consideration, I found that it would significant extra work to maintain, as the information would be splattered all over the options information. In contrast, with this change of submissions now having an optional score field, it is highly trivial for interested parties to inquire the SQL database for an accurate score tally, and easily pinpoint the origin of the score. Appropriate tests have also been added.")
